﻿
using System;

namespace FH.Admin.Entities
{
    public class LeadPaymentDetail
    {
        public int LeadId { get; set; }        
        public bool IsPaymentReceived { get; set; }
        public int TotalBrokerage { get; set; }
        public int ReceivedAmount { get; set; }
        public int OurCharges { get; set; }
        public string PaymentExpectedDate { get; set; }
        public string PaymentReceivedDate { get; set; }
        public string Comment { get; set; }
        public int CreatedBy { get; set; }
    }
}
