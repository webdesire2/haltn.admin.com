﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FH.Admin.Entities
{
    public class LeadDE
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PropertyIds { get; set; }
        public string PropertyType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }  
        public int LeadId { get; set; }
        public string Gender { get; set; }
        public string Tenant { get; set; }
        public string RoomType { get; set; }       
        public string Bhk { get; set; }
        public string Apartment { get; set; }
        public string LeadSource { get; set; }
    }
    
    public class SiteVisitModel
    {        
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string VisitDateTime { get; set; }
    }

    public class WebsiteLead
    {
        public int EnquiryId { get; set; }
        public string UserName { get; set; }
        public string UserMobile { get; set; }
        public string UserEmail { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class PropertyForLead
    {
        public int PropertyId { get; set; }
        public string PropertyType { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public double Rent { get; set; }
        public double Deposit { get; set; }
        public int PlanID { get; set; }
        public string PlanName { get; set; }
        public string Color { get; set; }
        public int NumberOfLeads { get; set; }
    }

    public class AllLeadList
    {
        public int LeadId { get; set; }
        public int PropertyId { get; set; }
        public string LeadName { get; set; }
        public string LeadMobile { get; set; }
        public string LeadSource { get; set; }
        public string Locality { get; set; }
        public string PropertyType { get; set; }
        public string TenantGender { get; set; }
        public DateTime LeadDate { get; set; }

    }


}
