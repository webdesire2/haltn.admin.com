﻿using System;
using System.Collections.Generic;
namespace FH.Admin.Entities
{
    public class AccountList
    {
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public string AccountName { get; set; }
        public string UserName { get; set; }
        public string Mobile { get; set; }
        public string AccountNo { get; set; }
        public string Ifsccode { get; set; }
        public string BankName { get; set; }
        public string AccountHolderName { get; set; }
        public string PaytmMID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int IsActive { get; set; }
        public string AccountType {  get; set; }
        public string AccountPan { get; set; }
        public string FilePath { get; set; }

    }
    public class AgentList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string City { get; set; }
        public bool IsActive { get; set; }
        public string AgreementStatus { get; set; }
        public DateTime CreatedOn { get; set; }
        
        public virtual IEnumerable<ZoneMaster> Zone { get; set; }
        public virtual IEnumerable<AreaMaster> Area { get; set; }
    }
   
    public class Agent
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string CompanyName { get; set; }
        public string OwnerName { get; set; }
        public string OfficialEmail { get; set; }
        public string OfficeAddress { get; set; }
        public string GSTNo { get; set; }
        public string Relationship { get; set; }
        public int CityId { get; set; }
        public string AreaId { get; set; }
        public string PANNo { get; set; }
       
        public IEnumerable<AreaMaster> Area { get; set; }
        public IEnumerable<CityMaster> City { get; set; }
        public IEnumerable<AreaMaster> AreaMaster { get; set; }
    }
}
