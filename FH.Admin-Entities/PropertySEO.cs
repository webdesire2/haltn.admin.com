﻿using System.Collections.Generic;

namespace FH.Admin.Entities
{
    public class PropertySEO
    {        
        public int PropertyId { get; set; }        
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDesc { get; set; }
        public string JsonDesc { get; set; }
        public IEnumerable<PropertySEOCategory> PropertySEOCategories { get; set; }
    }

    public class PropertySEOCategory
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
    }
}
