﻿using System.Collections.Generic;
namespace FH.Admin.Entities
{
    public class Property
    {
        public PropertyFlatRoom PropertyFlatRoomFlatmate { get; set; }
        public PropertyPG PropertyPG { get; set; }
        public IEnumerable<AmenityMaster> AmenityMaster { get; set; }
        public IEnumerable<FurnishingMaster> FurnishMaster { get; set; }
        public UserDetails PropertyPostedBy { get; set; }
        public IEnumerable<AreaMaster> AreaMaster { get; set; }
        public PropertySEO PropertySEO { get; set; }    
        public IEnumerable<PGRoomData> PGRoomData { get; set; }
    }

   
}
