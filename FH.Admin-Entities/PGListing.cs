﻿
using System;

namespace FH.Admin.Entities
{
    public class PGListing
    {
        public int Id { get; set; }
        public string Locality { get; set; }
        public string AvailableFor { get; set; }
        public string RoomType { get; set; }
        public int Rent { get; set; }
        public int TotalSeat { get; set; }
        public int AvailableSeat { get; set; }
        public string Status { get; set; }
        public DateTime CreatedOn { get; set; }        
    }
}
