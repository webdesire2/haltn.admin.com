﻿
namespace FH.Admin.Entities
{
    public class VendorAgreementParam
    {
        public int CallValue { get; set; }
        public string EmailId { get; set; }
        public string Status { get; set; }
        public int PropertyId { get; set; }
    }

    public class VendorEmail
    {
        public int PropertyId { get; set; }
        public string Email { get; set; }
    }
}
