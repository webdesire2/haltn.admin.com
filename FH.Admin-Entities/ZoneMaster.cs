﻿namespace FH.Admin.Entities
{
    public class ZoneMaster
    {
        public int ZoneId { get; set; }
        public string ZoneName { get; set; }
        public int CityId { get; set; }
    }
}
