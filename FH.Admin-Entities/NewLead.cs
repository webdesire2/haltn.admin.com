﻿namespace FH.Admin.Entities
{
    public class NewLead
    {        
        public string Mobile { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string VisitDate { get; set; }
        public string VisitTime { get; set; }
        public int PropertyId { get; set; }
        public int LeadId { get; set; }
        public int LeadAssignTo { get; set; }
        public int CreatedBy { get; set; }
    }
}
