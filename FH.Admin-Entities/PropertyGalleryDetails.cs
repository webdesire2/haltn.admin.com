﻿
namespace FH.Admin.Entities
{
    public class PropertyGalleryDetails
    {
        public int PropertyGalleryId { get; set; }      
        public string FilePath { get; set; }        
    }
}
