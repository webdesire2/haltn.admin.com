﻿using System;

namespace FH.Admin.Entities
{
    public class UseCodeMaster
    {
        public int UseCodeId { get; set; }
        public string Code { get; set; }
        public string UseCodeType { get; set; }        
        public int UseCodeTypePercentage { get; set; } 
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
    }

    public enum UseCodeType
    {
       CASHBACK,
       DISCOUNT
    }    
}
