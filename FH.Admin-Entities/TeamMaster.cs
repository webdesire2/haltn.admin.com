﻿namespace FH.Admin.Entities
{
   public class TeamMaster
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public int DepartmentId { get; set; }
    } 
}
 