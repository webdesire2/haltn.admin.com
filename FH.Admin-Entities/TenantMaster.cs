﻿using System;
using System.Collections.Generic;


namespace FH.Admin.Entities
{
    public class TenantDE
    { 
        public int TenantId { get; set; }
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public double SecurityAmount { get; set; }
        public double SecurityDeposit { get; set; }
        public double MonthlyRent { get; set; }        
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public bool IsActive { get; set; }
        public int IsActive1 { get; set; }
        public int UserId { get; set; }
        public int RoomId { get; set; }
        public float Occupancy { get; set; }
        public int AdminId { get; set; }
        public int RoomNo { get; set; }
        public int PageNo { get; set; }
        public double Payment { get; set; }
        public int CreatedBy { get; set; }
        public string PaymentType { get; set; }
        public string StatusMessage { get; set; }
        public DateTime? RentForMonthStartDate { get; set; }
        public DateTime? RentForMonthEndDate { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }

    public class TenantListVM
    {
        public IEnumerable<TenantList> TenantList { get; set; }
        public int PageNo { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
    }
    public class TenantList
    {
        public int TenantId { get; set; }
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public double SecurityAmount { get; set; }
        public double SecurityDeposit { get; set; }
        public bool IsActive { get; set; }
        public double MonthlyRent { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; } 
        public DateTime? UpdatedOn { get; set; }
        public int RoomNo { get; set; }
        public int RoomId { get; set; }
        public float Occupancy { get; set; }
    }

    public class NotPaidT
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int TenantId { get; set; }
        public int RentMonth { get; set; }
        public int RentYear { get; set; }
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public double SecurityAmount { get; set; }
        public double SecurityDeposit { get; set; }
        public double MonthlyRent { get; set; }
        public double RentAmount { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int RoomNo { get; set; }
        public int RoomId { get; set; }
        public float Occupancy { get; set; }
        public float SecurityAmountDue { get; set; }
    }

    public class TenantPaymentHistory
    {
        public int PaymentId { get; set; }
        public double DepositAmount { get; set; }
        public string PaymentType { get; set; }
        public DateTime? RentForMonthStartDate { get; set; }
        public DateTime? RentForMonthEndDate { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentMode { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class GetTenant
    {
        public int TenantId { get; set; }
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public double SecurityAmount { get; set; }
        public double MonthlyRent { get; set; }
        public double SecurityDepossit { get; set; }
        public int RoomNo { get; set; }
        public int RoomId { get; set; }
        public float Occupancy { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public bool IsActive { get; set; }        
    }

    public class TanentStayDetails
    {
        public int TenantId { get; set; }
        public double MonthlyRent { get; set; }
        public double SecurityAmount { get; set; }
        public double SecurityDeposit { get; set; }
        public double SecurityAmountDue { get; set; }
        public int PropertyId { get; set; }
        public string OwnerName { get; set; }
        public string OwnerMobile { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RoomNo { get; set; }
 
    }
}
