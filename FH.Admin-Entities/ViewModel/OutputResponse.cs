﻿
namespace FH.Admin.Entities.ViewModel
{
    public class QueryResponse
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}
