﻿
namespace FH.Admin.Entities
{
    public class PropertyLeadLog
    {
        public int PropertyId { get; set; }
        public string AssigneeName { get; set; }        
        public string LeadStatus { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
    }
}
