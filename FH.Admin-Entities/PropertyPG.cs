﻿using System;
using System.Collections.Generic;

namespace FH.Admin.Entities
{
    public class PropertyPG
    {
        public List<PGRoomRentalDetail> RoomRentalDetail { get; set; }
        public IEnumerable<PGRoomAmenity> RoomAmenities { get; set; }
        public PropertyMaster PropertyMaster { get; set; }
        public PGDetail PgDetail { get; set; }
        public IEnumerable<PGRule> pgRules { get; set; }
        public IEnumerable<PropertyGalleryDetails> GalleryDetails { get; set; }
        public IEnumerable<PropertyAmenitiesDetails> AmenityDetails { get; set; }
    }

    public class PGRoomData
    {
        public int PGRoomInventoryId { get; set; }
        public int PropertyId { get; set; }
        public int RoomNo { get; set; }
        public string RoomType { get; set; }
        public string FloorNo { get; set; }
        public float Occupancy { get; set; }
        public int Person { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

}
