﻿using System;
using System.Collections.Generic;

namespace FH.Admin.Entities
{
    public class PropertyMaster
    {
        public int PropertyId { get; set; }
        public string PropertyType { get; set; }
        public string PropertyName { get; set; }

        public string City { get; set; }        
        public string Locality { get; set; }
        public string Street { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PropertyStatusByAgent { get; set; }
        public string PropertyStatusByFH { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string PostedBy { get; set; }
        public int Area { get; set; }       
        
        public string ContactPersonName { get; set; }
        public string ContactPersonMobile { get; set; }
        public string LocationMapLink { get; set; }
        public string AgreementStatus { get; set; }
    }
}
