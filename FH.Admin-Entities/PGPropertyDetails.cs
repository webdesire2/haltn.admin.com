﻿
using System.Collections.Generic;

namespace FH.Admin.Entities
{
    public class PGPropertyDetails
    {
        public int PropertyId { get; set; }
        public string RoomType { get; set; }
        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public string TenantGender { get; set; }
        public string PreferredGuest { get; set; }
        public string AvailableFrom { get; set; }
        public bool Fooding { get; set; }
        public string GateClosingTime { get; set; }
        public bool Laundry { get; set; }
        public bool RoomCleaning { get; set; }
        public bool Warden { get; set; }
        public string Description { get; set; }
        public IEnumerable<PGRule> PGRules { get; set; }

    }
}
