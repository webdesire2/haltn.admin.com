﻿
namespace FH.Admin.Entities
{
    public class PropertyNearbyPlace
    {
        public int PropertyId { get; set; }
        public string PlaceType { get; set; }
        public string PlaceName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Distance { get; set; }
        public string TravelTime { get; set; }
    }
}
