﻿
namespace FH.Admin.Entities
{
    public class PropertyFurnishingDetails
    {
        public int PropertyId { get; set; }
        public int FurnishingMasterId { get; set; }        
        public string Value { get; set; }

    }
}
