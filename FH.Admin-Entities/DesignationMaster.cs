﻿
namespace FH.Admin.Entities
{
   public class DesignationMaster
    {
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int DepartmentId { get; set; }
    }
}
