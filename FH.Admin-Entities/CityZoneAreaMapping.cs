﻿
using System.Collections.Generic;

namespace FH.Admin.Entities
{
    public class CityZoneAreaMapping
    {
        public IEnumerable<CityMaster> CityMaster { get; set; }
        public IEnumerable<ZoneMaster> ZoneMaster { get; set; }
        public IEnumerable<AreaMaster> AreaMaster { get; set; }
    }
}
