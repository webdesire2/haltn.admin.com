﻿using System;
using System.Collections.Generic;

namespace FH.Admin.Entities
{
   public class UserDetails
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public int AccountId { get; set; }
        public string Password { get; set; }       
    }
    
    public class UserRegistration
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int CityId { get; set; }
        public int DepartmentId { get; set; }
        public int TeamId { get; set; }
        public int DesignationId { get; set; }        
        public int ZoneId { get; set; }        
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }        
        public virtual IEnumerable<DepartmentMaster> Department { get; set; }
        public virtual IEnumerable<DesignationMaster> Designation { get; set; }
        public virtual IEnumerable<CityMaster> City { get; set; }
        public virtual IEnumerable<TeamMaster> Team { get; set; }
        public virtual IEnumerable<ZoneMaster> Zone { get; set; }
    }          

    public class UserList
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string TeamName { get; set; }
        public bool IsActive { get; set; }


    }
}
