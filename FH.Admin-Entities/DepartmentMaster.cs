﻿
namespace FH.Admin.Entities
{
    public class DepartmentMaster
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}
