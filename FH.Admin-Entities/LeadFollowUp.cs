﻿namespace FH.Admin.Entities
{
    public class UserLeadFollowUp
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int LeadCount { get; set; }
    }
}
