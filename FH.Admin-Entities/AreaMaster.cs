﻿using System.Collections.Generic;

namespace FH.Admin.Entities
{
    public class AreaMaster
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public int ZoneId { get; set; }
        public int CityId { get; set; }

        public virtual ZoneMaster Zone{get;set; }
        public virtual CityMaster City { get; set; }
    }
}
