﻿
namespace FH.Admin.Entities
{
    public class CityMaster
    {
        public int CityId { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string SWLatitude { get; set; }
        public string SWLongitude { get; set; }
        public string NELatitude { get; set; }
        public string NELongitude { get; set; }

    }
}
