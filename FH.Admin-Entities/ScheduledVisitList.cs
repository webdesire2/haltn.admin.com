﻿using System;

namespace FH.Admin.Entities
{
    public class PropertyLeadList
    {        
        public int LeadId { get; set; }
        public string PropertyId { get; set; }
        public string PropertyType { get; set; }
        public string City { get; set; }
        public string Locality { get; set; }
        public DateTime PropertyPostedOn { get; set; }
        public string PropertyPostedByName { get; set; }
        public string PropertyPostedByMobile { get; set; }
        public string VisitorName { get; set; }
        public string VisitorMobile { get; set; }
        public string VisitDate { get; set; }
        public string VisitTime { get; set; }
        public string LeadStatus { get; set; }
        public DateTime? LeadCreatedOn { get; set; }
        public bool IsReferredByAgent { get; set; }
        public string PropertyStatus { get; set; }        
        public string LeadAssignTo { get; set; }        
        public string Remarks { get; set; }
        public int? TotalAmount { get; set; }
        public int? ReceivedAmount { get; set; }
        public DateTime? PaymentExpectedDate { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public int TotalRecord { get; set; }
    }
}
