﻿using System.Collections.Generic;

namespace FH.Admin.Entities
{
    public class PropertyFlatRoom
    {
        public PropertyMaster PropertyMaster { get; set; }
        public PropertyDetailFlatRoom PropertyDetail { get; set; }
        public IEnumerable<PropertyGalleryDetails> GalleryDetails { get; set; }
        public IEnumerable<PropertyAmenitiesDetails> AmenityDetails { get; set; }
    }
}
