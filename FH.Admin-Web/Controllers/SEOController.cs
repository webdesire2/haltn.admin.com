﻿using FH.Web.Security;
using System.Web.Mvc;
using FH.Admin.Entities;
using FH.Admin.Service;
using FH.Admin_Web.ViewModel;
using FH.Util;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class SEOController : BaseController
    {
        private readonly SEOBL _seoBL;

        public SEOController()
        {
            _seoBL = new SEOBL();
        }

        [HttpGet]
        public ActionResult Category()
        {
            return View();
        }

        [HttpGet]
        public PartialViewResult ListCategory(int pageSize, string category ="",int pageNo = 1)
        {
            SEODE de = new SEODE()
            {
                PageSize= pageSize,
                PageNo=pageNo,
                Category=category
            };

            var data= _seoBL.GetAllCategory(de);
            data.Category = category;
            return PartialView("_ListCategory", data);
        }

        [HttpGet]
        public ActionResult AddCategory()
        {
            return View(new AddSEOCategory());
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult AddCategory(AddSEOCategory model)
        {
            if (string.IsNullOrEmpty(model.Lat))
                return JsonResultHelper.Error("Lat required");
            if (string.IsNullOrEmpty(model.Lng))
                return JsonResultHelper.Error("Lng required");
            if (string.IsNullOrEmpty(model.City))
                return JsonResultHelper.Error("city required");
            if(string.IsNullOrEmpty(model.PropertyType))
                return JsonResultHelper.Error("property type required");
            if (string.IsNullOrEmpty(model.Prefix))
                return JsonResultHelper.Error("prefix required");

            if (model.Radius <= 0)
                return JsonResultHelper.Error("Radius required Interger only.");

            List<SEOCategory> lstCategory = new List<SEOCategory>();

            var lstLocality=_seoBL.GetLocalityByLatLng(model.Lat, model.Lng,model.Radius);

            foreach(var l in lstLocality)
            {
                var objseoCategory = _seoBL.GenerateCategory(l.Locality, model.PropertyType, model.Prefix,l.Lat,l.Lng);
                lstCategory.Add(objseoCategory);
            }

            // string googleResponse = string.Empty;
            //    string[] types = { "restaurant", "transit_station" };
            //var googleApiKey = System.Configuration.ConfigurationManager.AppSettings["GooglePlaceApiKey"].ToString();

            //foreach (var type in types)
            //    {
            //        var url = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={model.Lat},{model.Lng}&radius=1500&type={type}&key={googleApiKey}";
            //        using (WebClient wc = new WebClient())
            //        {
            //            googleResponse = wc.DownloadString(url);
            //        }

            //        dynamic googleData = Newtonsoft.Json.JsonConvert.DeserializeObject(googleResponse);

            //        dynamic results = googleData.results;
            //        foreach (var item in results)
            //        {

            //            string categoryLocality =item.name +" "+item.vicinity+" "+ model.City;
            //        string lat = item.geometry.location.lat;
            //        string lng = item.geometry.location.lng;
            //            var objseoCategory=_seoBL.GenerateCategory(categoryLocality, model.PropertyType, model.Prefix,lat,lng);

            //            lstCategory.Add(objseoCategory);
            //        }
            //    }

            foreach (var item in lstCategory)
                {
                    SEODE de = new SEODE
                    {
                        Category = item.Category,
                        Url = item.Url,
                        MetaTitle=item.MetaTitle,
                        MetaKeyword=item.MetaKeyword,
                        MetaDesc=item.MetaDesc,
                        MetaJson="",
                        Heading=item.Heading,
                        HeadingContent=item.HeadingContent,
                        PropertyType=item.PropertyType,
                        Prefix=item.Prefix,
                        Lat=item.Lat,
                        Lng=item.Lng,
                        City=model.City,
                        UserId=Validator.Validation.DecryptToInt(CurrentUser.User.UserId)
                    };

                 _seoBL.AddCategory(de);
                }

            return JsonResultHelper.Success("Category added successfully.");            
        }

        [HttpGet]
        public ActionResult EditCategory(int id)
        {
            EditSEOCategory model = new EditSEOCategory();
            if (id>0)
            {
                SEODE de = new SEODE()
                {                   
                    Id=id
                };
                var data=_seoBL.GetCategory(de);
                
                if(data!=null)
                {
                    model.Id = data.Id;
                    model.Category = data.Category;
                    model.Url = data.Url;
                    model.MetaTitle = data.MetaTitle;
                    model.MetaKeyword = data.MetaKeyword;
                    model.MetaDesc = data.MetaDesc;
                    model.MetaJson = data.MetaJson;
                    model.Heading = data.Heading;
                    model.HeadingContent = data.HeadingContent;
                }
            }

            return View("EditCategory",model);
        }
        
        [HttpPost]
        public ActionResult EditCategory(EditSEOCategory req)
        {
            if(ModelState.IsValid)
            {
                SEODE de = new SEODE
                {
                    Id=req.Id,
                    Category=req.Category,
                    Url=req.Category.Replace(" ","-"),
                    MetaTitle=req.MetaTitle,
                    MetaDesc=req.MetaDesc,
                    MetaKeyword=req.MetaKeyword,
                    MetaJson=req.MetaJson,
                    Heading=req.Heading,
                    HeadingContent=req.HeadingContent
                };

                string result=_seoBL.UdpateCategory(de);
                req.Result = result;
                req.IsPostBack =true;
            }

            return View(req);
        }

        [HttpPost]
        public ActionResult DeleteCategory(int id)
        {
            try
            {
                if (id <= 0)
                    return JsonResultHelper.Error("id required.");

                SEODE de = new SEODE()
                {
                    Id = id
                };

                _seoBL.DeleteCategory(de);
                return JsonResultHelper.Success("Category deleted.");
            }
            catch(Exception e)
            {
                return JsonResultHelper.Error(e.Message);
            }
        }

        [HttpPost]
        public JsonResult GetCategoryByName(string name)
        {
            SEODE de = new SEODE()
            {
                Category=name
            };

            try
            {
                var data = _seoBL.GetCategoryByAutoSearch(de);
                return JsonResultHelper.Success("", data.Count() > 0 ? data.Select(x => new { Id = x.Id, Category = x.Category }) : null);
            }
            catch(Exception e)
            {
                return JsonResultHelper.Error(e.Message);
            }           
        }        

        [HttpPost]
        public JsonResult ReplaceCategoryText(ReplaceCategoryText req)
        {
            if (req.Id <= 0)
                return JsonResultHelper.Error("Id required");

            var category=_seoBL.GetCategory(new SEODE { Id = req.Id });

            if (category == null)
                return JsonResultHelper.Error("No data found for category.");
            
                category.Category = category.Category.Replace(req.OldText, req.NewText);
                category.Url = category.Category.Replace(req.OldText, req.NewText).Replace(" ", "-");
                category.MetaTitle = category.MetaTitle.Replace(req.OldText, req.NewText);
                category.MetaKeyword = category.MetaKeyword.Replace(req.OldText, req.NewText);
                category.MetaDesc = category.MetaDesc.Replace(req.OldText, req.NewText);
                category.MetaJson = category.MetaJson.Replace(req.OldText, req.NewText);
                category.Heading = category.Heading.Replace(req.OldText, req.NewText);
                category.HeadingContent = category.HeadingContent.Replace(req.OldText, req.NewText);
            
            SEODE de = new SEODE
            {
                Id = category.Id,
                Category = category.Category,
                Url = category.Url,
                MetaTitle = category.MetaTitle,
                MetaDesc = category.MetaDesc,
                MetaKeyword = category.MetaKeyword,
                MetaJson = category.MetaJson,
                Heading = category.Heading,
                HeadingContent = category.HeadingContent
            };

            string result = _seoBL.UdpateCategory(de);
            if (string.IsNullOrEmpty(result))
                return JsonResultHelper.Success("Update successfully.");
            else
                return JsonResultHelper.Error(result);
        }

        [HttpPost]
        public JsonResult AddLocality(string locality,string lat,string lng,string city,int radius)
        {
            if (string.IsNullOrWhiteSpace(locality))
                return JsonResultHelper.Error("Locality required.");
            if (string.IsNullOrWhiteSpace(lat) || string.IsNullOrWhiteSpace(lng) || string.IsNullOrWhiteSpace(city))
                return JsonResultHelper.Error("Lat Lng City required.");

            if (radius <= 0)
                return JsonResultHelper.Error("Radius required Integer value only.");

            try
            {
                List<SEOLocalityMaster> lstCategory = new List<SEOLocalityMaster>();
                string googleResponse = string.Empty;
                string[] types = { "restaurant", "transit_station" };
                var googleApiKey = System.Configuration.ConfigurationManager.AppSettings["GooglePlaceApiKey"].ToString();

                foreach (var type in types)
                {
                    //var url = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={lat},{lng}&radius={radius}&type={type}&city={city}&key={googleApiKey}";
                    var url = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={lat},{lng}&radius={radius}&type={type}&key={googleApiKey}";
                    using (WebClient wc = new WebClient()) 
                    {
                        googleResponse = wc.DownloadString(url);
                    }

                    dynamic googleData = Newtonsoft.Json.JsonConvert.DeserializeObject(googleResponse);

                    dynamic results = googleData.results;
                    foreach (var item in results) 
                    {
                        
                        string categoryLocality = item.name + " " + item.vicinity + " " + city;
                        string slat = item.geometry.location.lat;
                        string slng = item.geometry.location.lng;

                        lstCategory.Add(new SEOLocalityMaster { Locality = categoryLocality, Lat = slat, Lng = slng, City = city });
                    }
                }

                _seoBL.AddLocality(lstCategory);

                return JsonResultHelper.Success("Locality added successfully", new { Url = "/seo/ListLocality" });
            }
            catch(Exception e)
            {
                return JsonResultHelper.Error(e.Message);
            }
        }            

        [HttpGet]
        public ActionResult AddLocality()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ListLocality(string searchKey,int page=1)
        {
            var data=_seoBL.GetAllLocality(searchKey, page);
            return View(data);
        }

        [HttpPost]
        public JsonResult DeleteLocality(int id)
        {
            _seoBL.DeleteLocality(id);
            return JsonResultHelper.Success("Delete successfully.");
        }

        [HttpGet]
        public ActionResult EditLocality(int id)
        {
            var data=_seoBL.GetLocalityById(id);
            return View(data);
        }

        [HttpPost]
        public ActionResult EditLocality(SEOLocalityMaster model)
        {
            if (model.Id <= 0)
                return JsonResultHelper.Error("Invalid Id.");

            if (string.IsNullOrWhiteSpace(model.Locality))
                return JsonResultHelper.Error("Locality required.");

            string result=_seoBL.UpdateLocality(model);
            if (string.IsNullOrWhiteSpace(result))
                return JsonResultHelper.Success("Update successfully.");
            else
                return JsonResultHelper.Error(result);
        }
    }
}