﻿using FH.Web.Security;
using System.Web.Mvc;
using FH.Admin.Service;
using FH.Admin.Entities;
using FH.Admin_Web.ViewModel;
using System;
using FH.Util;
using FH.Validator;
using System.Linq;
using Amazon.AWSSupport.Model;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class PlanController : BaseController
    {
        private readonly PlanBL _PlanBL;
         
        public PlanController()
        {
            _PlanBL = new PlanBL();
        }

        [HttpGet]
        public ActionResult Add()
        { 
            return View();
        }

        [HttpPost]
        public ActionResult AddPlan(PlanMaster req)
        {

            PlanMaster de = new PlanMaster()
            {
                Name = req.Name,
                Description = req.Description,
                Price = req.Price,
                Discount = req.Discount,
                Validity = req.Validity,
                NumberOfLeads = req.NumberOfLeads,
                Status = req.Status,
                VarifiedTag = req.VarifiedTag,
                Color = req.Color,
                SEOData = req.SEOData,
                Category = req.Category,
                CreatedBy = Validator.Validation.DecryptToInt(CurrentUser.User.UserId),
            };  

            var id = _PlanBL.AddPlan(de);
            if (id.Id > 0) {
                return JsonResultHelper.Success("Plan Add successfully.");
            }
            else
            {
                return JsonResultHelper.Error("Some thing error occer. Please try again later.");
            }
        }

        [HttpGet]
        public ActionResult List()
        {
            PlanMaster de = new PlanMaster()
            {
            };

            var plan = _PlanBL.PlanList(de);

            GetAllPlantVM plans = new GetAllPlantVM();
            plans.Plans = plan; 
            return View(plans); 
        }

        [HttpGet]
        public ActionResult AddSales()
        {
            PlanMaster de = new PlanMaster()
            {
                Status = 1
            };

            GetAllPlantVM plans = new GetAllPlantVM();
            plans.Plans = _PlanBL.PlanList(de);
            return View(plans);
        }

        [HttpPost]
        public ActionResult GetOwnerDetails(GetownerDetails req)
        {
            PlanMaster de = new PlanMaster()
            {
                PlanId = req.PlanId,
                Mobile = req.Mobile
            };

            GetOwnerDetailVM plans = new GetOwnerDetailVM();
            plans.Plans = _PlanBL.PlanList(de);
            plans.PropertyList = _PlanBL.GetOwnerPropertyDetails(de);
            //_PlanBL.GetOwnerDetails(de);
            return View(plans);
        }

        [HttpPost]
        public ActionResult DeletPlan(int PlanOrderID)
        {
            PlanMaster de = new PlanMaster()
            {
                PlanOrderID = PlanOrderID,
            };

             _PlanBL.DeleteOderByAdmin(de);
            //_PlanBL.GetOwnerDetails(de);
            return JsonResultHelper.Success("Plan Deleted successfully.");
        } 

        [HttpPost]
        public ActionResult GenrateOrder(GenerateOrder req)
        {
            PlanMaster de = new PlanMaster()
            {
                PlanId = req.PlanId,
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
            };
            var plan = _PlanBL.PlanList(de).First(); 
            if (plan != null && req.PropertyId.Length > 0)
            {
                PlanMaster PlanOrder = new PlanMaster()
                {
                    CreatedBy = req.OwnerId,
                    Payment = (req.Price * req.PropertyId.Length),
                    OrderGenratedByAdmin = 1 
                }; 

                var PlanOrderDetails = _PlanBL.AddPlanOrder(PlanOrder).First();
                if (PlanOrderDetails != null)
                {
                    foreach (string PropertyID in req.PropertyId)
                    {
                        PlanMaster PlanDetails = new PlanMaster()
                        {
                            PlanId = plan.PlanId,
                            NumberOfLeads = plan.NumberOfLeads,
                            PropertyId = Int32.Parse(PropertyID),
                            CreatedBy = req.OwnerId, 
                            VarifiedTag = req.VarifiedTag,
                            Payment = req.Price,
                            Validity = plan.Validity,
                            PlanOrderID = PlanOrderDetails.PlanOrderID
                        }; 
                        int id = _PlanBL.AddPlanToVendor(PlanDetails);
                        if (id == 0)
                        {
                            return JsonResultHelper.Error("Something went wrong.");
                        }
                    }
                }
            }
            return JsonResultHelper.Success("Order Genrate successfully.");
        }

        [HttpGet]
        public ActionResult SellList(GetAllPlanSellVM req)
        {
            PlanMaster de = new PlanMaster()
            {
                PropertyId = req.PropertyId,
                CreatedBy = req.PropertyId,
            };
            var order = _PlanBL.GetMyOder(de);
            var data = _PlanBL.GetMyPlans(de);

            req.MyPlans = data;
            req.MyOrders = order;
             
            return View(req);
        }

        [HttpGet]
        public ActionResult Edit(int planid)
        {
            PlanMaster de = new PlanMaster()
            {
                PlanId = planid
            };

            var plan = _PlanBL.GetPlan(de);

            if (plan == null)
                return RedirectToAction("List");
            else
            {
                EditPlanModel model = new EditPlanModel()
                {
                    PlanId =plan.PlanId,
                    Name = plan.Name,
                    Category = plan.Category,
                    Description = plan.Description,
                    Price = plan.Price,
                    Discount = plan.Discount,
                    Validity = plan.Validity,
                    NumberOfLeads = plan.NumberOfLeads,
                    Status = plan.Status,
                    VarifiedTag = plan.VarifiedTag,
                    Color = plan.Color,
                    SEOData = plan.SEOData
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult EditPlan(EditPlanModel req)
        {
            PlanMaster de = new PlanMaster()
            {
                PlanId = req.PlanId,
                Name = req.Name,
                Description = req.Description,
                Price = req.Price,
                Discount = req.Discount,
                Validity = req.Validity,
                NumberOfLeads = req.NumberOfLeads,
                Status = req.Status,
                VarifiedTag = req.VarifiedTag,
                Color = req.Color,
                SEOData = req.SEOData,
                CreatedBy = req.CreatedBy,
                Category = req.Category,
            };

            _PlanBL.Update(de);
             return JsonResultHelper.Success("Plan Edit successfully.");
        }

    }
}