﻿using FH.Web.Security;
using System.Web.Mvc;
using FH.Admin.Service;
using FH.Admin.Entities;
using FH.Admin_Web.ViewModel;
using System;
using FH.Util;
using Newtonsoft.Json;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class TenantController : BaseController
    {
        private readonly TenantBL _tenantBL;

        public TenantController()
        {
            _tenantBL = new TenantBL();
        }

        [HttpGet]
        public ActionResult Add(int pid=0)
        {
            AddTenantModel model = new AddTenantModel();
            model.PropertyId = pid;

            TenantDE de = new TenantDE(){ 
                PropertyId = pid,
                CheckInDate = DateTime.Now
            }; 

            model.PGRoomData = _tenantBL.getAvailableRooms(de);

           // model.PropertyId = pid;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddTenantModel req)
        {
            if(ModelState.IsValid)
            {
                TenantDE de = new TenantDE() 
                {
                    PropertyId = req.PropertyId,
                    Name = req.Name,
                    Email = req.Email,
                    Mobile = req.Mobile,
                    RoomId = req.RoomId,
                    Occupancy = req.Occupancy,
                    SecurityAmount = req.SecurityAmount,
                    MonthlyRent = req.MonthlyRent,
                    SecurityDeposit=req.SecurityDeposit,
                    CheckInDate = req.CheckInDate,
                    AdminId=Validator.Validation.DecryptToInt(CurrentUser.User.UserId)
                };

                var output=_tenantBL.AddTenant(de);
                if (output.Id > 0)
                    return RedirectToAction("List");
                else
                    ModelState.AddModelError("", output.Message);
            }

            return View(req);
        }

        [HttpPost]
        public ActionResult GetAvailableRooms(TenantDE req)
        {
            TenantDE de = new TenantDE()
            {
                PropertyId = req.PropertyId,
                CheckInDate = req.CheckInDate
            }; 
            var result = _tenantBL.getAvailableRooms(de);
            if (result != null)
            {  
                var result1 = JsonConvert.SerializeObject(result);
                return JsonResultHelper.Success(result1);
            }
            else 
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpGet]
        public ActionResult List(GetAllTenantVM req)
        {
            TenantDE de = new TenantDE()
            {
                TenantId= req.Tid,
                IsActive1 = req.IsActive1,
                PropertyId = req.Pid,
                Mobile=req.Mobile,
                PageNo=req.Page,
                RoomNo = req.RoomNo
            };

            var tenant=_tenantBL.GetAllTenant(de);

            req.TenantList = tenant.TenantList;
            req.Pager = new Pager(tenant.TotalRecord, tenant.PageNo, tenant.PageSize);
            
            return View(req);
        }

        [HttpGet]
        public ActionResult LastMonthRentNotPaid(GetNOTTenantVM req)
        {
            TenantDE de = new TenantDE()
            {
                TenantId = req.Tid,
                PropertyId = req.Pid,
                Mobile = req.Mobile,
                RoomNo = req.RoomNo,
                IsActive = req.IsActive,
            }; 
            var tenant = _tenantBL.gtnotpaid(de);

            foreach (var tenantData in tenant)
            {
                if (tenantData != null)
                {
                    if (tenantData.CheckInDate.Month == tenantData.RentMonth && tenantData.CheckInDate.Year == tenantData.RentYear)
                    {
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                        var fromDate = tenantData.CheckInDate;
                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                        int days = (30 - tenantData.FromDate.Day) + 1;
                        //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                        tenantData.RentAmount = Math.Round(rentPerDay * days, 2);
                        if (tenantData.RentAmount < 1) { tenantData.RentAmount = 1; }
                    }
                    else if (tenantData.CheckOutDate.Month == tenantData.RentMonth && tenantData.CheckOutDate.Year == tenantData.RentYear)
                    {
                        var lastDay = new DateTime(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month, DateTime.DaysInMonth(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month));
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / lastDay.Day : 0;
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = tenantData.CheckOutDate;
                        int days = tenantData.ToDate.Day;
                        tenantData.RentAmount = Math.Round(rentPerDay * days, 2);
                        if (tenantData.RentAmount < 1) { tenantData.RentAmount = 1; }
                    }
                    else
                    {
                        var fromDate = new DateTime(tenantData.RentYear, tenantData.RentMonth, 1);
                        tenantData.FromDate = fromDate;
                        tenantData.RentAmount = tenantData.MonthlyRent;
                        tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                    }
                } 
            }

            req.TenantList = tenant;

            return View(req);
        }

        [HttpGet]
        public ActionResult PropertyMap(PropertyMapVM req)
        {
            TenantDE de = new TenantDE()
            {
                PropertyId = req.Pid,
                CheckInDate = req.CheckInDate
            };
            var result = _tenantBL.getAvailableRooms(de);
            //var tenant = _tenantBL.GetAllTenant(de);

            req.PGRoomData = result;

            return View(req);
        }

        [HttpGet]
        public ActionResult Edit(int tid)
        {
            TenantDE de = new TenantDE()
            {
               TenantId=tid
            };

            var tenant = _tenantBL.GetTenant(de);

            if (tenant == null)
                return RedirectToAction("List");
            else
            {
                EditTenantModel model = new EditTenantModel()
                {
                    TenantId = tenant.TenantId,
                    Name = tenant.Name,
                    Email = tenant.Email,
                    Mobile = tenant.Mobile,
                    RoomNo = tenant.RoomNo,
                    RoomId = tenant.RoomId,
                    Occupancy = tenant.Occupancy,
                    MonthlyRent = tenant.MonthlyRent,
                    SecurityAmount = tenant.SecurityAmount,
                    SecurityDeposit=tenant.SecurityDepossit,
                    CheckInDate = tenant.CheckInDate,
                    CheckOutDate = tenant.CheckOutDate,
                    IsActive = tenant.IsActive,
                };

                TenantDE de1 = new TenantDE()
                {
                    PropertyId = tenant.PropertyId
                };
                model.PGRoomData = _tenantBL.getAvailableRooms(de1);


                de1.RoomId = tenant.RoomId;
                model.TenantRoom = _tenantBL.getTenantRooms(de1);
                  
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditTenantModel req)
        {
            if(ModelState.IsValid)
            {
                TenantDE de = new TenantDE()
                {
                    TenantId = req.TenantId,
                    CheckOutDate = req.CheckOutDate,
                    Name = req.Name,
                    Email = req.Email,
                    Mobile = req.Mobile,
                    RoomNo = req.RoomNo,
                    RoomId = req.RoomId,
                    Occupancy = req.Occupancy,
                    MonthlyRent = req.MonthlyRent,
                    SecurityAmount = req.SecurityAmount,
                    IsActive = req.IsActive,
                    CheckInDate = req.CheckInDate,
                    AdminId = Validator.Validation.DecryptToInt(CurrentUser.User.UserId)
                };

                _tenantBL.Update(de);

                return RedirectToAction("List");
            }

            return View(req);
        }

        [HttpPost]
        public ActionResult CollectPayment(CollectCashData req) {
            TenantDE data = new TenantDE()
            {
                CreatedBy = Validator.Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "RENT",
                StatusMessage = req.Remark,
                TenantId = req.TenantId,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };
             
            var result = _tenantBL.CollectPayment(data);
            if (result != null) {
                return JsonResultHelper.Success("Payment Collected");
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }


        [HttpPost]
        public ActionResult CollectSecurity(CollectCashData req)
        {
            TenantDE data = new TenantDE()
            {
                CreatedBy = Validator.Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "SECURITY",
                TenantId = req.TenantId,
                StatusMessage = req.Remark,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.CollectSecurity(data);
            if (result != null)
            {
                return JsonResultHelper.Success("Payment Collected");
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }


        [HttpPost]
        public ActionResult RefundSecurity(CollectCashData req)
        {
            TenantDE data = new TenantDE()
            {
                CreatedBy = Validator.Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = -(req.rentAmount),
                PaymentType = "SECREFUND", 
                StatusMessage = req.Remark,
                TenantId = req.TenantId,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.RefundSecurity(data); 
            if (result != null) 
            {
                return JsonResultHelper.Success("Payment Refund");
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpGet]
        public PartialViewResult TenantPaymentHistory(int tenantId)
        {
            TenantDE de = new TenantDE()
            {
                TenantId = tenantId
            };
            
            //TanentNewPayment
            var tenantData = _tenantBL.GetTanentStayDetails(de);

            if (tenantData != null)
            {
                tenantData.SecurityAmountDue = tenantData.SecurityAmount - tenantData.SecurityDeposit;

                if (tenantData.CheckInDate.Month == DateTime.Now.Month && tenantData.CheckInDate.Year == DateTime.Now.Year)
                {
                    var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                    var fromDate = tenantData.CheckInDate;
                    tenantData.FromDate = fromDate;
                    tenantData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                    int days = (30 - tenantData.FromDate.Day) + 1;
                    //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                    tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                    if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                } else if(tenantData.CheckOutDate.Month == DateTime.Now.Month && tenantData.CheckOutDate.Year == DateTime.Now.Year) {
                    var lastDay = new DateTime(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month, DateTime.DaysInMonth(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month));
                    var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / lastDay.Day : 0;
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    tenantData.FromDate = fromDate;
                    tenantData.ToDate = tenantData.CheckOutDate;
                    int days = tenantData.ToDate.Day;
                    tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                    if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                } else {  
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    tenantData.FromDate = fromDate;
                    tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                }
            }



            var data = _tenantBL.GetTenantPaymentLog(de);

            foreach (var item in data)
            {
                if (item.RentForMonthEndDate == tenantData.ToDate && item.RentForMonthStartDate == tenantData.FromDate && item.DepositAmount == tenantData.MonthlyRent && item.PaymentStatus == "True")
                {
                    tenantData.MonthlyRent = 0; break;
                }
                
            }

            VMPaymentDetails result = new VMPaymentDetails();
            result.TenantPaymentHistory = data;
            result.TanentStayDetails = tenantData;
            return PartialView("_GetPaymentHistory", result);
        }
    }
}