﻿using System.Web.Mvc;
using System.Linq;
using FH.Admin.Entities;
using FH.Web.Security;
using FH.Validator;
using System;
using System.Globalization;
using System.Collections.Generic;
using FH.Admin.Service;

namespace FH.Admin_Web.Controllers
{    
    [CustomAuthorize]
    public class PostController : Controller
    {
        [HttpGet]
        public ActionResult PG()
        {
            return View();
        }        

        [HttpPost]
        public ActionResult PG(PropertyPG property)
        {
            /****************PG Room Detail*************/
            if(property.RoomRentalDetail == null || property.RoomRentalDetail.Count() <= 0)
                return ValidationError("Rental detail required.");

            foreach (var room in property.RoomRentalDetail)
            {
                if (!string.IsNullOrEmpty(room.RoomType) && room.RoomType.ToLower() != "single" && room.RoomType.ToLower() != "double" && room.RoomType.ToLower() != "three" && room.RoomType.ToLower() != "four")
                    return ValidationError("Invalid room type.");

                if (room.ExpectedRent <= 0 || room.ExpectedDeposit <= 0 || room.ExpectedRent > room.ExpectedDeposit)
                    return ValidationError("Expected rent can't be more than deposit.");
            }

            if (property.RoomAmenities != null)
            {
                foreach (var roomAmenity in property.RoomAmenities)
                {
                    if (!Validation.IsInt(roomAmenity.AmenityId))
                        return ValidationError("Invalid room amenities.");
                }
            }

            /*********** Locality Detail ***********/            

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.City))
                return ValidationError("City required.");
            if (!City.GetAll().ContainsKey(property.PropertyMaster.City))
                return ValidationError("Invalid city.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Locality))
                return ValidationError("Locality required.");
            if (property.PropertyMaster.Locality.Length > 100)
                return ValidationError("Locality can't be more than 100 characters.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Latitude) || string.IsNullOrEmpty(property.PropertyMaster.Latitude))
                return ValidationError("Please select locality from list.");

            if (!string.IsNullOrWhiteSpace(property.PropertyMaster.Street) && property.PropertyMaster.Street.Length > 100)
                return ValidationError("Address can't be more than 100 characters.");

            /**************** PG Detail ****************/
            if (string.IsNullOrEmpty(property.PgDetail.TenantGender))
                return ValidationError("PG available for required.");
            else if (property.PgDetail.TenantGender.ToLower() != "male" && property.PgDetail.TenantGender.ToLower() != "female" && property.PgDetail.TenantGender.ToLower() != "any")
                return ValidationError("Invalid PG available for type.");

            if (!string.IsNullOrEmpty(property.PgDetail.PreferredGuest) && !PreferredGuest.GetAll().ContainsKey(property.PgDetail.PreferredGuest))
                return ValidationError("Invalid preferred guest.");

            if (!string.IsNullOrEmpty(property.PgDetail.AvailableFrom))
            {
                string availableFrom = property.PgDetail.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    property.PgDetail.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Invalid available from date format.");
            }

            /*************** PG Rules ******************/

            if (property.pgRules != null)
            {
                foreach (var rule in property.pgRules)
                {
                    if (!PGRulesMaster.GetAll().ContainsKey(rule.RuleId))
                        return ValidationError("Invalid PG rules.");
                }
            }

            List<PGRule> pgRules = new List<PGRule>();
            if (property.pgRules != null)
            {
                foreach (var rule in property.pgRules)
                {
                    var item = PGRulesMaster.GetAll().Where(x => x.Key == rule.RuleId).FirstOrDefault();
                    pgRules.Add(new PGRule { RuleId = item.Key, RuleName = item.Value });
                }
            }

            property.pgRules = pgRules;
            property.PgDetail.PreferredGuest = (!string.IsNullOrWhiteSpace(property.PgDetail.PreferredGuest) ? PreferredGuest.GetAll().Where(x => x.Key.ToLower() == property.PgDetail.PreferredGuest.ToLower()).FirstOrDefault().Value : null);
            property.PropertyMaster.CreatedBy = Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));

            using (var uow = new UnitOfWork())
            {
                try
                {
                    //int propertyId = uow.PropertyBL.AddPropertyPG(property);
                    //if (propertyId > 0)
                    //{
                    //    uow.Commit();
                    //    return Json(new { Success = true, Data = RenderRazorViewToString(ControllerContext, "_Congratulation", null) });
                    //}
                    //else
                        return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }

        private JsonResult ValidationError(string message)
        {
            return Json(new { Success = false, ErrorMessage = message });
        }
    }
}