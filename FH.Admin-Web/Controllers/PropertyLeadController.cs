﻿using FH.Web.Security;
using System.Web.Mvc;
using FH.Admin.Service;
using FH.Admin.Entities;
using System;
using FH.Util;
using System.Globalization;
using FH.Validator;
using System.Linq;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class PropertyLeadController : BaseController
    {        
        [HttpGet]
        public ActionResult New(int page = 1)
        {            
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize,"NEW");
               // leadVM.City = uow.CityMasterBL.GetAllCity();
               // leadVM.Area = uow.CityZoneAreaMappingBL.GetArea(1);
                return View(leadVM);                
            }           
        }
       
        [HttpGet]
        public ActionResult FollowUp(int page=1)
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize,"FollowUp");               
                return View(leadVM);
            }
        }

        [HttpGet]
        public ActionResult Documentation(int page = 1)
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize, "DOCUMENTATION");
                return View(leadVM);
            }
        }

        [HttpGet]
        public ActionResult Assigned(int page = 1)
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize, "ASSIGNED");
               // leadVM.LeadFollowUp = uow.PropertyLeadBL.GetLeadFollowUpTeam();
                return View(leadVM);
            }
        }

        [HttpGet]
        public ActionResult Visited(int page = 1)
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize, "VISITED");
                //leadVM.LeadFollowUp = uow.PropertyLeadBL.GetLeadFollowUpTeam();
                return View(leadVM);
            }
        }

        [HttpGet]
        public ActionResult Done(int page = 1)
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize, "DONE");
                //leadVM.LeadFollowUp = uow.PropertyLeadBL.GetLeadFollowUpTeam();
                return View(leadVM);
            }
        }

        [HttpGet]
        public ActionResult Payment(int page = 1)
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize, "PAYMENT");
                //leadVM.LeadFollowUp = uow.PropertyLeadBL.GetLeadFollowUpTeam();
                return View(leadVM);
            }
        }

        [HttpGet]
        public ActionResult Cancelled(int page = 1)
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            using (var uow = new UnitOfWork())
            {
                PropertyLeadVM leadVM = new PropertyLeadVM();
                ViewBag.PageIndex = page;
                ViewBag.PageSize = pageSize;
                leadVM.Lead = uow.PropertyLeadBL.GetAllLead(page, pageSize, "CANCELLED");
                //leadVM.LeadFollowUp = uow.PropertyLeadBL.GetLeadFollowUpTeam();
                return View(leadVM);
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult GetFollowUpTeam(int areaId=0)
        {
            if (areaId <= 0)
                return Json(new { Success =false,ErrorMessage="Invalid input."});

            using (var uow = new UnitOfWork())
            {
                return Json(new { Success = true,Team=uow.PropertyLeadBL.GetLeadFollowUpTeam(areaId) });
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult LeadAssignToFollowUpTeam(int leadId,int assignTo)
        {
            if(assignTo !=0 && leadId !=0)
            {
                using (var uow = new UnitOfWork())
                {
                    int assignBy = Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));
                    try
                    {
                        uow.PropertyLeadBL.UpdateLeadStatusAndAssignTo(leadId,assignTo, assignBy, EnLeadStatus.FOLLOWUP.ToString());
                        uow.Commit();
                        return Json(new { Success = true});
                    }
                    catch
                    {
                        return Json(new { Success = false,ErrorMessage="Something went wrong." });
                    }
                }
            }
            return Json(new { Success = false, ErrorMessage = "Invalid Input." });
        }

        //[HttpPost]
        //public ActionResult LeadAssignToAreaManager(int leadId, int assignTo, int propertyId)
        //{
        //    if (assignTo != 0 && leadId != 0 && propertyId != 0)
        //    {
        //        using (var uow = new UnitOfWork())
        //        {
        //            int assignBy = Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));
        //            try
        //            {
        //                uow.PropertyLeadBL.UpdateLeadStatusAndAssignTo(leadId, propertyId, assignTo, assignBy, EnLeadStatus.ASSIGNED.ToString());
        //                uow.Commit();
        //                return Json(new { Success = true });
        //            }
        //            catch (Exception e)
        //            {
        //                return Json(new { Success = false, ErrorMessage = "Something went wrong." });
        //            }
        //        }
        //    }
        //    return Json(new { Success = false, ErrorMessage = "Invalid Input." });
        //}

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateLeadStatus(int leadId,string status,string comment)
        {
            if (leadId <= 0)
                return Json(new {Success=false,ErrorMessage="Lead Id not supplied." });
            
            if(status !=EnLeadStatus.CANCELLED.ToString() && status !=EnLeadStatus.DONE.ToString())
                return Json(new { Success = false, ErrorMessage = "Invalid Status." });

            if(!string.IsNullOrWhiteSpace(comment) && comment.Length>200)
                return Json(new { Success = false, ErrorMessage = "Comment can't be more than 200 characters." });

            using (var uow = new UnitOfWork())
            {
                try
                {
                    int updatedBy = Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));
                    uow.PropertyLeadBL.UpdateLeadStatus(leadId, status, comment,updatedBy);
                    uow.Commit();
                    return Json(new { Success =true});
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong." });
                }
            }
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateLeadStatusToDocumentation(LeadPaymentDetail entity)
        {
            if (entity.LeadId <= 0)
                return Json(new { Success = false,ErrorMessage="Lead id not supplied." });
            
            if(entity.TotalBrokerage<=0)
                return Json(new { Success = false, ErrorMessage = "Total brokerage amount must be greater than 0." });

            if (entity.ReceivedAmount>0 && entity.ReceivedAmount>entity.TotalBrokerage)
                return Json(new { Success = false, ErrorMessage = "Received amount should not be more than Total brokerage amount." });
            if(entity.OurCharges<=0)
                return Json(new { Success = false, ErrorMessage = "Our charges must be greater than 0." });
            if(entity.OurCharges>entity.TotalBrokerage)
                return Json(new { Success = false, ErrorMessage = "Our charges amount should not be more than Total brokerage amount." });

            if (entity.IsPaymentReceived)
            {
                DateTime outPaymentReceivedDate;
                if (DateTime.TryParseExact(entity.PaymentReceivedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outPaymentReceivedDate))
                {
                    entity.PaymentReceivedDate = DateTime.ParseExact(entity.PaymentReceivedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
            }
            else
            {
                DateTime outPaymentExpectedDate;
                if (DateTime.TryParseExact(entity.PaymentExpectedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outPaymentExpectedDate))
                {
                    entity.PaymentExpectedDate = DateTime.ParseExact(entity.PaymentExpectedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
            }
            
            using (var uow = new UnitOfWork())
            {
                entity.CreatedBy = Convert.ToInt16(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));
                    int result = uow.PropertyLeadBL.UpdateLeadPaymentDetail(entity);
                    if (result > 0)
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false,ErrorMessage="Something went wrong."});
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult AddClientRequirementNote(int leadId,string clientNote)
        {
            if (leadId <= 0)
                return Json(new { Success = false,ErrorMessage="Invalid LeadId."});

            if (string.IsNullOrWhiteSpace(clientNote))
                return Json(new { Success = false, ErrorMessage = "Note Required." });
            else if(clientNote.Length>500)
                return Json(new { Success = false, ErrorMessage = "Note can't be greater than 500 characters." });
            
            using (var uow = new UnitOfWork())
            {
                uow.PropertyLeadBL.AddClientRequirementNote(leadId, clientNote);
                uow.Commit();
                return Json(new { Success = true});
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult GetLeadLog(int leadId)
        {
            if(leadId>0)
            {
                using (var uow = new UnitOfWork())
                {
                   return Json(new{Lead= uow.PropertyLeadBL.GetLeadLog(leadId) });
                }
            }
            return Json(new { Lead =""});
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult AddNewLead(NewLead lead)
        {  
            if(string.IsNullOrWhiteSpace(lead.Name))
            return Json(new { Success = false, ErrorMessage = "Name required." });
            if(lead.Name.Length>50)
            return Json(new { Success = false, ErrorMessage = "Name can't be more than 50 characters." });
            if(Validation.MobileNo(lead.Mobile))
            return Json(new { Success = false, ErrorMessage = "Invalid mobile." });
            if(!string.IsNullOrWhiteSpace(lead.Email)&& Validation.EmailAddress(lead.Email))
                return Json(new { Success = false, ErrorMessage = "Invalid email id." });
            if (!string.IsNullOrWhiteSpace(lead.Email) && lead.Email.Length>50)
                return Json(new { Success = false, ErrorMessage = "Email can't be more than 50 characters." });
            
            using (var uow = new UnitOfWork())
            {
                lead.CreatedBy = Convert.ToInt16(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));               
                Tuple<int,string> result=uow.PropertyLeadBL.AddNewLead(lead);
                if (result.Item1 == 1)
                {
                    uow.Commit();                    
                    return Json(new { Success = true, Message = result.Item2 });
                }
                else
                    return Json(new { Success = false, ErrorMessage = result.Item2 });
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult AssignMorePropertyToUser(NewLead entity)
        {
            if (entity.PropertyId <= 0)
                return Json(new { Success = false,ErrorMessage="Invalid input."});

            if(entity.LeadId<=0)
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            DateTime outVisitDate;
            if (DateTime.TryParseExact(entity.VisitDate, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outVisitDate))
                entity.VisitDate = DateTime.ParseExact(entity.VisitDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            else
                return Json(new { Success = false, ErrorMessage = "Invalid visit date." });

            if (string.IsNullOrWhiteSpace(entity.VisitTime))
                return Json(new { Success = false, ErrorMessage = "Visit Time required." });
            else if (!PropertyVisitTime.GetAll().ContainsKey(entity.VisitTime))
                return Json(new { Success = false, ErrorMessage = "Invalid visit time." });
            entity.VisitTime = PropertyVisitTime.GetAll().Where(x => x.Key == entity.VisitTime).FirstOrDefault().Value;

            entity.CreatedBy = Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));
            using (var uow = new UnitOfWork())
            {
               var result= uow.PropertyLeadBL.AssignMoreProperty(entity);
                if (result.Item1 == 1)
                { uow.Commit(); return Json(new { Success = true }); }
                else
                    return Json(new { Success = false, ErrorMessage = result.Item2 });
            }
        }
    }
}