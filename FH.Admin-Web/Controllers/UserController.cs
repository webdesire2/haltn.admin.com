﻿using System.Web.Mvc;
using FH.Admin.Entities;
using FH.Validator;
using FH.Web.Security;
using FH.Admin.Service;
using Newtonsoft.Json;
using System.Web.Security;
using System;
using System.Web;
using System.Collections.Generic;
using System.Net;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class UserController : BaseController
    {
        [HttpGet]
       public ActionResult Login(string redirectUrl)
        {            
            return View(new UserDetails());
        }

        [HttpPost]
        public ActionResult Login(UserDetails entity)
        {
            bool isValid = true;

            if (Validation.RequiredField(entity.Email))
            { TempData["email"] = "required."; isValid = false; }
            else if (Validation.EmailAddress(entity.Email))
            { TempData["email"] = "invalid email id."; isValid = false; }

            if (Validation.RequiredField(entity.Password))
            { TempData["password"] = "required."; isValid = false; }
            else if (Validation.MaxLength(entity.Password, 50))
            { TempData["password"] = "max 50 characters allowed."; isValid = false; }
            else if (Validation.MinLength(entity.Password, 5))
            { TempData["password"] = "minimum 5 characters required."; isValid = false; }

            if(isValid)
            {
                string testPAss = EncreptDecrpt.Decrypt(entity.Password);
                string password=EncreptDecrpt.Encrypt(entity.Password);
                using(UnitOfWork uow=new UnitOfWork())
                    {
                     UserDetails userDetail=uow.UserBL.GetUserDetails(entity.Email);
                    if (userDetail != null)
                    {
                        if (userDetail.Password != password)
                            TempData["error"] = "invalid password.";

                        if(userDetail.Email==entity.Email && userDetail.Password==password)
                        {
                            userDetail.UserId = EncreptDecrpt.Encrypt(userDetail.UserId);
                            string userData = JsonConvert.SerializeObject(userDetail);
                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                1,
                                userDetail.Email,
                                DateTime.Now,
                                DateTime.Now.AddMinutes(30),
                                false,
                                userData);

                            string encTicket = FormsAuthentication.Encrypt(authTicket);
                            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                            Response.Cookies.Add(faCookie);

                            if (Request.QueryString["redirectUrl"] != null && !string.IsNullOrEmpty(Request.QueryString["redirectUrl"].ToString()))
                                return Redirect(Request.QueryString["redirectUrl"]);
                              else
                                return RedirectToAction("dashboard", "user");
                        }
                    }
                    else
                    {
                        TempData["error"] = "invalid username.";
                    }

                }            
            }
           
            entity.Password = "";
            return View(entity);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
           return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Dashboard()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Registration()
        {
            UserRegistration user = new UserRegistration();

            using (var uow = new UnitOfWork())
            {
                user.City=uow.CityMasterBL.GetAllCity();
                user.Department = uow.DepartmentMasterBL.GetAllDepartment();                             
            }
                return View(user);
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult Registration(UserRegistration entity)
        {           
            entity.Name = Validation.Trim(entity.Name);
            entity.Email = Validation.Trim(entity.Email);
            entity.Mobile = Validation.Trim(entity.Mobile);

            if (string.IsNullOrWhiteSpace(entity.Name))
                return ValidationMessage("Name required.");
            else if(entity.Name.Length>50)
                return ValidationMessage("Name maximum 50 characters allowed.");

            if (string.IsNullOrWhiteSpace(entity.Email))
                return ValidationMessage("Email required.");
            else if (entity.Name.Length > 50)
                return ValidationMessage("Email maximum 50 characters allowed.");
            else if(Validation.EmailAddress(entity.Email))
                return ValidationMessage("Invalid Email.");

            if(string.IsNullOrEmpty(entity.Mobile))
                return ValidationMessage("Mobile no required.");
            else if(Validation.MobileNo(entity.Mobile))
                return ValidationMessage("Invalid Mobile no.");

            if (entity.CityId <= 0)
                return ValidationMessage("Please select City.");

            if (entity.DepartmentId<=0)
                return ValidationMessage("Please select Department.");
            
            if (entity.TeamId <= 0)
                return ValidationMessage("Please select Team.");

            if (entity.DesignationId <= 0)
                return ValidationMessage("Please select Designation.");            
            
            if(entity.TeamId==Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["FollowupTeamId"]) && entity.ZoneId<=0)
                return ValidationMessage("Please select Zone.");

            using (var uow = new UnitOfWork())
            {
                int result=uow.UserBL.AddUser(entity);
                if (result==1)
                {
                    uow.Commit();
                    return Json(new { Success = true, Message = "Record insert successfully." });
                }
                else
                    return Json(new { Success = false, ErrorMessage = "User already exist." });

            }            
        } 

        [HttpPost]
        public PartialViewResult GetUser()
        {
            using (var uow = new UnitOfWork())
            {
                return PartialView("_UserList",uow.UserBL.GetAllEmployee());
            }
        }

        [HttpPost]
        public JsonResult GetTeamAndDesignation(int departmentId)
        {
            using (var uow = new UnitOfWork())
            {
              var data= uow.UserBL.GetTeamAndDesignation(departmentId);
                return Json(new {Team=data.Team,Designation=data.Designation });
            }
        }

        [HttpGet]
        public ActionResult GetAreaManagerPanel()
        {
            using (var uow = new UnitOfWork())
            {
                var cityList=uow.UserBL.GetAllCity();
                return PartialView("_AreaManager", cityList);
            }                
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult IsUserMobileExist(string mobile)
        {
            if(string.IsNullOrEmpty(mobile))
                return Json(new { Success = false, ErrorMessage = "Mobile No required." });
            else if(Validation.MobileNo(mobile))
                return Json(new { Success = false,ErrorMessage="Invalid Mobile."});

            using (var uow = new UnitOfWork())
            {              
                bool result=uow.UserBL.IsUserMobileNoExist(mobile);
                if (result)
                    return Json(new { Success = true});
                else
                    return Json(new { Success = false, IsExist =false});
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult GetUserByIdAndMobile(int userId=0,string mobile="")
        {
            if (userId <= 0 && string.IsNullOrEmpty(mobile))
                return ValidationMessage("Invalid request.");
            if(!string.IsNullOrEmpty(mobile) && Validation.MobileNo(mobile))
                return ValidationMessage("Invalid mobile no.");

            using (var uow = new UnitOfWork())
            {
                UserDetails user=uow.UserBL.GetUserByIdAndMobile(userId, mobile);
                if (user != null)
                    return Json(new { Success = true, User = user });
                else
                    return ValidationMessage("Owner detail not found.");
            }
        }
        private JsonResult ValidationMessage(string message)
        {
            return Json(new { Success = false, ErrorMessage = message });
        }
    }

}