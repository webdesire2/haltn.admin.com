﻿using System;
using System.Web.Mvc;
using FH.Logger;

namespace FH.Admin_Web
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            string strController = filterContext.RouteData.Values["controller"].ToString();
            string strAction = filterContext.RouteData.Values["action"].ToString();
            Exception e = filterContext.Exception;
            //Log.Write(strController, strAction, "", e.Message, "Admin",true);

            filterContext.ExceptionHandled = true;

            if (Request.IsAjaxRequest())
            {
                filterContext.Result = new JsonResult
                {
                    Data = new { Success = false, ErrorMessage = "Something Went Wrong,Please Try Again." },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                // ViewBag.Error = "Something Went Wrong,Please Try Again.";
                //filterContext.Result = new ViewResult()
                //{
                //    ViewName = "Error"                   
                //};
            }
        }
    }
}
