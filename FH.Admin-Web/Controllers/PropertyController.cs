﻿using System.Web.Mvc;
using FH.Admin.Entities;
using FH.Admin.Service;
using FH.Web.Security;
using FH.Validator;
using System.Collections.Generic;
using FH.Util;
using System.Net;
using System.Linq;
using System;
using System.Web;
using System.IO;
using System.Globalization;
using FH.Admin_Web.ViewModel;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class PropertyController : BaseController
    {
        [HttpGet]
        public ActionResult Index(int page = 1, string type = "", string status = "", int propertyId = 0, int ownerId = 0, string ownerMobile = "")
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            int totalRecord;
            int pageIndex = page;

            VMPropertyListing vmProperty = new VMPropertyListing();

            using (UnitOfWork uow = new UnitOfWork())
            {

                vmProperty.propertyMaster = uow.PropertyBL.GetAllProperty(pageSize, pageIndex, type, status, propertyId, ownerId, ownerMobile, out totalRecord);
                vmProperty.pager = new Pager(totalRecord, pageIndex, pageSize);
            }
            ViewBag.type = type;
            ViewBag.status = status;
            ViewBag.OwnerId = ownerId == 0 ? "" : ownerId.ToString();
            ViewBag.PropertyId = propertyId == 0 ? "" : propertyId.ToString();
            ViewBag.OwnerMobile = ownerMobile;

            return View(vmProperty);
        }

        [HttpGet]
        public ActionResult Edit(string pid)
        {
            using (var uow = new UnitOfWork())
            {
                Property propertyDetail = uow.PropertyBL.GetProperty(Convert.ToInt32(pid));
                if (propertyDetail == null)
                    return PartialView("_Error");
                else
                {
                    //string cityCode = propertyDetail.PropertyFlatRoomFlatmate == null ? propertyDetail.PropertyPG.PropertyMaster.City : propertyDetail.PropertyFlatRoomFlatmate.PropertyMaster.City;
                    //int cityId = uow.CityMasterBL.GetAllCity().Where(x => x.CityCode == cityCode).FirstOrDefault().CityId;
                    // propertyDetail.AreaMaster = uow.CityZoneAreaMappingBL.GetArea(cityId);
                    ViewBag.cityMaster = uow.CityMasterBL.GetAllCity();
                    if (propertyDetail.PropertyFlatRoomFlatmate != null)
                    {
                        return View("Edit", propertyDetail);
                    }
                    else if (propertyDetail.PropertyPG != null)
                    {
                        return View("EditPG", propertyDetail);
                    }
                } 
            }

            return PartialView("_Error");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdatePropertyDetail(string pid, string ptype, PropertyDetailFlatRoom property)
        {
            int propertyId = Convert.ToInt32(pid);
            if (propertyId == 0)
                return ValidationMessage("Invalid Input.");

            if (string.IsNullOrWhiteSpace(ptype))
                return ValidationMessage("Invalid Input.");

            string propertyType = ptype.ToUpper();

            if (propertyType != EnPropertyType.Flat.ToString().ToUpper() && propertyType != EnPropertyType.Room.ToString().ToUpper() && propertyType != EnPropertyType.Flatmate.ToString().ToUpper())
                return ValidationMessage("Invalid Property Type.");

            if (!ApartmentType.GetAll().ContainsKey(property.ApartmentType))
                ValidationMessage("Invalid Apartment Type.");

            if ((propertyType == "FLAT" || propertyType == "FLATMATE") && !BHKType.GetAll().ContainsKey(property.BhkType))
                return ValidationMessage("Invalid BHK Type.");

            if ((propertyType == "FLATMATE" || propertyType == "ROOM") && !RoomType.GetAll().ContainsKey(property.RoomType))
                return ValidationMessage("Invalid Room Type.");

            if (propertyType == "FLATMATE" && (property.TenantGender.ToLower() != "male" && property.TenantGender.ToLower() != "female"))
                return ValidationMessage("Invalid Tenant Gender.");

            if (string.IsNullOrEmpty(property.PropertySize) || property.PropertySize.Length > 10)
                return ValidationMessage("Invalid Build Up Area.");

            if (!FacingType.GetAll().ContainsKey(property.Facing))
                return ValidationMessage("Invalid Facing Type.");

            if (!PropertyAgeType.GetAll().ContainsKey(property.PropertyAge))
                return ValidationMessage("Invalid Property Age Type.");

            if (property.FloorNo == null || property.FloorNo <= 0)
                return ValidationMessage("Floor No Should Be Positive Numeric.");

            if (property.TotalFloor == null || property.TotalFloor <= 0)
                return ValidationMessage("Total Floor Should Be Positive Numeric.");

            if (property.FloorNo > property.TotalFloor)
                return ValidationMessage("Floor No Should be Less than Total Floor.");

            if (!WaterSupplyType.GetAll().ContainsKey(property.WaterSupply))
                return ValidationMessage("Invalid Water Supply Type.");

            if (property.GateSecurity != "Yes" && property.GateSecurity != "No")
                return ValidationMessage("Invalid Gate Security Type.");

            property.ApartmentType = ApartmentType.GetAll().Where(x => x.Key == property.ApartmentType).Select(x => x.Value).FirstOrDefault();

            if (propertyType == "FLAT" || propertyType == "FLATMATE")
                property.BhkType = BHKType.GetAll().Where(x => x.Key == property.BhkType).Select(x => x.Value).FirstOrDefault();
            else
                property.BhkType = null;

            if (propertyType == "FLATMATE" || propertyType == "ROOM")
                property.RoomType = RoomType.GetAll().Where(x => x.Key == property.RoomType).Select(x => x.Value).FirstOrDefault();
            else
                property.RoomType = null;

            if (propertyType != "FLATMATE")
                property.TenantGender = null;

            property.Facing = FacingType.GetAll().Where(x => x.Key == property.Facing).Select(x => x.Value).FirstOrDefault();
            property.PropertyAge = PropertyAgeType.GetAll().Where(x => x.Key == property.PropertyAge).Select(x => x.Value).FirstOrDefault();
            property.WaterSupply = WaterSupplyType.GetAll().Where(x => x.Key == property.WaterSupply).Select(x => x.Value).FirstOrDefault();
            using (var uow = new UnitOfWork())
            {
                try
                {
                    string result = uow.PropertyBL.UpdatePropertyDetail(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), property);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = result });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateLocalityDetail(string pid, PropertyMaster property)
        {
            int propertyId = Convert.ToInt32(pid);
            if (propertyId == 0)
                return ValidationMessage("Invalid Property Id.");

            // if (!City.GetAll().ContainsKey(property.City))
            //   return ValidationMessage("Invalid City.");
            if (string.IsNullOrWhiteSpace(property.ContactPersonName))
                return ValidationMessage("Contact person name required.");
            else if (property.ContactPersonName.Length > 50)
                return ValidationMessage("Contact person name can't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(property.ContactPersonMobile))
                return ValidationMessage("Contact person mobile no required.");
            else if (Validation.MobileNo(property.ContactPersonMobile))
                return ValidationMessage("Invalid contact person mobile no.");

            if (string.IsNullOrEmpty(property.Locality))
                return ValidationMessage("Locality Required.");
            else if (property.Locality.Length > 100)
                return ValidationMessage("Locality should not be greater than 100 characters.");

            if (string.IsNullOrEmpty(property.Latitude) || string.IsNullOrEmpty(property.Latitude))
                return ValidationMessage("Please select Locality from list.");

            if (property.Street.Length > 100)
                return ValidationMessage("Street should not be greater than 100 characters.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    string result = uow.PropertyBL.UpdateLocalityDetail(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), property);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = result });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateRentalDetail(string pid, string ptype, PropertyDetailFlatRoom property)
        {
            int propertyId = (!string.IsNullOrWhiteSpace(pid) ? Convert.ToInt32(pid) : 0);
            if (propertyId == 0)
                return ValidationMessage("Invalid Property Id.");

            if (string.IsNullOrWhiteSpace(ptype))
                return ValidationMessage("Invalid Property Type.");

            string propertyType = ptype.ToUpper();

            if (propertyType != EnPropertyType.Flat.ToString().ToUpper() && propertyType != EnPropertyType.Room.ToString().ToUpper() && propertyType != EnPropertyType.Flatmate.ToString().ToUpper())
                return ValidationMessage("Invalid Property Type.");

            //if (property.OwnerExpectedRent <= 0)
            //    return ValidationMessage("Owner expected Rent should be greater than 0.");

            //if (property.OwnerExpectedDeposit <= 0)
            //    return ValidationMessage("Owner expected deposit should be greater than 0.");

            if (property.ExpectedRent <= 0)
                return ValidationMessage("Expected Rent should be greater than 0.");
            //if (property.ExpectedDeposit <= 0)
            //    return ValidationMessage("Expected Deposit should be greater than 0.");

            if (!string.IsNullOrEmpty(property.AvailableFrom))
            {
                string availableFrom = property.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    property.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationMessage("Invalid Input AvailableFrom.");
            }

            if (!PreferredTenant.GetAll().ContainsKey(property.PreferredTenant))
                return ValidationMessage("Invalid Input Preferred Tenant.");

            if (!ParkingType.GetAll().ContainsKey(property.Parking))
                return ValidationMessage("Invalid Input Parking.");

            if (!FurnishingType.GetAll().ContainsKey(property.Furnishing))
                return ValidationMessage("Invalid Input Furnishing Type.");
            else
            {
                if (property.Furnishing == "FF" || property.Furnishing == "SF")
                {
                    if (property.FurnishingDetails == null)
                        return ValidationMessage("Furnishing Details Required.");
                }
            }

            if (!string.IsNullOrWhiteSpace(property.Description) && property.Description.Length > 500)
                return ValidationMessage("Description should't be greater than 500 characters.");

            property.PreferredTenant = PreferredTenant.GetAll().Where(x => x.Key == property.PreferredTenant).Select(x => x.Value).FirstOrDefault();
            property.Parking = ParkingType.GetAll().Where(x => x.Key == property.Parking).Select(x => x.Value).FirstOrDefault();
            property.Furnishing = FurnishingType.GetAll().Where(x => x.Key == property.Furnishing).Select(x => x.Value).FirstOrDefault();

            using (var uow = new UnitOfWork())
            {
                try
                {
                    string result = uow.PropertyBL.UpdateRentalDetail(propertyId, propertyType, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), property);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = result });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }

        }

        [HttpPost]
        public ActionResult UploadGallery()
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                //   var supportedTypes = new[] { ".png", ".jpg", "jpeg", ".gif", ".bmp" };
                string fileName = Path.GetFileNameWithoutExtension(postedFile.FileName);
                string fileExt = Path.GetExtension(postedFile.FileName);

                // if (!supportedTypes.Contains(fileExt.ToLower()))
                //   return Json(new { Success = false, Error = "Only PNG/JPG/JPEG/GIF/BMP image is allowed.", Id = Request.Form["Id"]});

                //if (postedFile.ContentLength > (1048576 * 5)) // 5MB
                //    return Json(new { Success = false, Error = "File size cannot be exceed 5 MB.", Id = Request.Form["Id"] });

                if (!string.IsNullOrEmpty(CurrentUser.User.UserId))
                {
                    // string filePath = FileManager.FilePath(fileName);
                    FileManager.S3Response reponse = FileManager.Upload(postedFile.InputStream, FileManager.S3Folder.Property, fileName, fileExt);
                    if (reponse.Success)
                        return Json(new { Success = true, Id = Request.Form["Id"], Url = reponse.FileUrl, File = reponse.FileName });
                    else
                        return Json(new { Success = false, Id = Request.Form["Id"] });
                }
            }
            return Json(new { Success = false, Error = "File not found.", Id = Request.Form["Id"] });
        }

        //
        [HttpPost]
        public ActionResult addPGRoomData()
        {
            if (Request.Params["pid"] == null && string.IsNullOrWhiteSpace(Request.Params["pid"]))
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            if (Request.Params["RoomNo"] == null && string.IsNullOrWhiteSpace(Request.Params["RoomNo"]))
                return Json(new { Success = false, ErrorMessage = "Invalid input Room No." });

            if (Request.Params["RoomType"] == null && string.IsNullOrWhiteSpace(Request.Params["RoomType"]))
                return Json(new { Success = false, ErrorMessage = "Invalid input Room Type." });

            if (Request.Params["FloorNo"] == null && string.IsNullOrWhiteSpace(Request.Params["FloorNo"]))
                return Json(new { Success = false, ErrorMessage = "Invalid input Floor No." });

            int propertyId = Convert.ToInt32(Request.Params["pid"]);
            if (propertyId == 0)
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            using (var uow = new UnitOfWork())
            {
                try
                {
                    PGRoomData RoomData = new PGRoomData()
                    {
                        RoomNo = Convert.ToInt32(Request.Params["RoomNo"]),
                        RoomType = Request.Params["RoomType"],
                        FloorNo = Request.Params["FloorNo"],
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now, 
                        PropertyId = propertyId
                    };   
                    var RoomDetails = uow.PropertyBL.addPGRoomData(RoomData);
                    if (RoomDetails != null)
                    {
                        uow.Commit();
                        return Json(new { Success = true, RoomData = RoomDetails });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
                catch (Exception e)
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }



        //
        [HttpPost]
        public ActionResult deletePGRoomData()
        {
            if (Request.Params["pid"] == null && string.IsNullOrWhiteSpace(Request.Params["pid"]))
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            if (Request.Params["PGRoomInventoryId"] == null && string.IsNullOrWhiteSpace(Request.Params["PGRoomInventoryId"]))
                return Json(new { Success = false, ErrorMessage = "Invalid Input." });

            using (var uow = new UnitOfWork())
            {
                try
                {
                    PGRoomData RoomData = new PGRoomData()
                    {
                        PGRoomInventoryId = Convert.ToInt32(Request.Params["PGRoomInventoryId"]),
                    };
                    uow.PropertyBL.deletePGRoomData(RoomData);
                    uow.Commit();
                    return Json(new { Success = true, Message = "Room Deleted Successfully." });
                }
                catch (Exception e)
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }


        [HttpPost]
        public ActionResult AddNewGallery()
        {
            if (Request.Params["pid"] == null && string.IsNullOrWhiteSpace(Request.Params["pid"]))
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            int propertyId = Convert.ToInt32(Request.Params["pid"]);
            if (propertyId == 0)
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                //         var supportedTypes = new[] { ".png", ".jpg", "jpeg", ".gif", ".bmp" };
                string fileName = Path.GetFileNameWithoutExtension(postedFile.FileName);
                string fileExt = Path.GetExtension(postedFile.FileName);

                //       if (!supportedTypes.Contains(fileExt.ToLower()))
                //         return Json(new { Success = false, ErrorMessage = "Only PNG/JPG/JPEG/GIF/BMP files is allowed." });

                //if (postedFile.ContentLength > (1048576 * 5)) // 5MB
                //    return Json(new { Success = false, ErrorMessage = "File size cannot be exceed 1 MB." });

                if (!string.IsNullOrEmpty(CurrentUser.User.UserId))
                {

                    FileManager.S3Response responseS3 = FileManager.Upload(postedFile.InputStream, FileManager.S3Folder.Property, fileName, fileExt);
                    if (!responseS3.Success)
                    {
                        return ValidationMessage("File Not Upload.");
                    }
                    using (var uow = new UnitOfWork())
                    {
                        if (!string.IsNullOrEmpty(responseS3.FileName))
                        {
                            string fileId = uow.PropertyBL.AddNewGallery(propertyId, Validation.DecryptToInt(CurrentUser.User.UserId), responseS3.FileUrl);
                            uow.Commit();
                            return Json(new { Success = true, Path = responseS3.FileUrl, FileId = fileId });
                        }
                        else
                            return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                    }
                }
            }
            return Json(new { Success = false, ErrorMessage = "File not found." });
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult DeleteGallery(string pid, int image)
        {
            var errorMessage = Json(new { Success = false, ErrorMessage = "Invalid input." });
            int propertyId = Convert.ToInt32(pid);
            if (propertyId == 0)
                return errorMessage;
            if (image == 0)
                return errorMessage;

            using (var uow = new UnitOfWork())
            {
                try
                {
                    string filePath = uow.PropertyBL.DeleteGallery(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), image);
                    if (string.IsNullOrEmpty(filePath))
                    {
                        return Json(new { Success = false, ErrorMessage = "Invalid request." });
                    }
                    else
                    {
                        uow.Commit();
                        FileManager.Delete(filePath);
                        return Json(new { Success = true });
                    }
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult RemoveGalleryFile(string imagePath)
        {
            if (!string.IsNullOrEmpty(imagePath) && !string.IsNullOrEmpty(CurrentUser.User.UserId))
            {
                if (FileManager.Delete(imagePath))
                    return Json(new { Success = true });
            }
            return Json(new { Success = false, ErrorMessage = "Invalid request." });
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateAmenity(string pid, IEnumerable<PropertyAmenitiesDetails> amenities)
        {
            var errorMessage = Json(new { Success = false, ErrorMessage = "Invalid input." });
            int propertyId = Convert.ToInt32(pid);
            if (propertyId == 0)
                return errorMessage;

            using (var uow = new UnitOfWork())
            {
                string result = uow.PropertyBL.UpdatePropertyAmenity(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), amenities);
                if (string.IsNullOrEmpty(result))
                {
                    uow.Commit();
                    return Json(new { Success = true });
                }
                else
                    return Json(new { Success = false, ErrorMessage = result });
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdatePGRoomDetail(string pid, List<PGRoomRentalDetail> rentalDetail, IEnumerable<PGRoomAmenity> roomAmenity)
        {
            var errorMessage = Json(new { Success = false, ErrorMessage = "Invalid input." });
            int propertyId = Convert.ToInt32(pid);
            if (propertyId == 0)
                return errorMessage;

            if (rentalDetail == null || rentalDetail.Count() <= 0)
                return errorMessage;

            foreach (var room in rentalDetail)
            {
                if (!string.IsNullOrEmpty(room.RoomType) && room.RoomType.ToLower() != "single" && room.RoomType.ToLower() != "double" && room.RoomType.ToLower() != "three" && room.RoomType.ToLower() != "four")
                    return errorMessage;
            }

            if (roomAmenity != null)
            {
                foreach (var amenity in roomAmenity)
                {
                    if (!Validation.IsInt(amenity.AmenityId))
                        return errorMessage;
                }
            }

            using (var uow = new UnitOfWork())
            {
                string result = uow.PropertyBL.UpdatePGRoomDetail(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), rentalDetail, roomAmenity);
                if (string.IsNullOrWhiteSpace(result))
                {
                    uow.Commit();
                    return Json(new { Success = true });
                }
                else
                    return Json(new { Success = false, ErrorMessage = result });
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdatePGDetail(string pid, PGDetail pgDetail, IEnumerable<PGRule> pgRules)
        {
            var errorMessage = Json(new { Success = false, ErrorMessage = "Invalid input." });
            int propertyId = Convert.ToInt32(pid);
            if (propertyId == 0)
                return errorMessage;

            if (string.IsNullOrEmpty(pgDetail.TenantGender))
                return errorMessage;
            else if (pgDetail.TenantGender.ToLower() != "male" && pgDetail.TenantGender.ToLower() != "female" && pgDetail.TenantGender.ToLower() != "co-living" && pgDetail.TenantGender.ToLower() != "roomrk")
                return errorMessage;

            if (string.IsNullOrEmpty(pgDetail.PreferredGuest))
                return errorMessage;
            else if (!PreferredGuest.GetAll().ContainsKey(pgDetail.PreferredGuest))
                return errorMessage;

            if (!string.IsNullOrEmpty(pgDetail.AvailableFrom))
            {
                string availableFrom = pgDetail.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    pgDetail.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return errorMessage;
            }

            if (pgRules != null)
            {
                foreach (var rule in pgRules)
                {
                    if (!PGRulesMaster.GetAll().ContainsKey(rule.RuleId))
                        return errorMessage;
                }
            }

            List<PGRule> lstRules = new List<PGRule>();
            if (pgRules != null)
            {
                foreach (var rule in pgRules)
                {
                    var item = PGRulesMaster.GetAll().Where(x => x.Key == rule.RuleId).FirstOrDefault();
                    lstRules.Add(new PGRule { RuleId = item.Key, RuleName = item.Value });
                }
            }

            pgDetail.PreferredGuest = PreferredGuest.GetAll().Where(x => x.Key.ToLower() == pgDetail.PreferredGuest.ToLower()).FirstOrDefault().Value;

            using (var uow = new UnitOfWork())
            {
                string result = uow.PropertyBL.UpdatePGDetail(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), pgDetail, lstRules);
                if (string.IsNullOrWhiteSpace(result))
                {
                    uow.Commit();
                    return Json(new { Success = true });
                }
                else
                    return Json(new { Success = false, ErrorMessage = result });
            }
        }

        [HttpPost]
        public ActionResult UpdateStatus(string status, int propertyId)
        {
            if (propertyId <= 0)
                return Json(new { Success = false, Error = "Invalid Input." });

            if (!PropertyStatus.GetAll().ContainsKey(status))
                return Json(new { Success = false, Error = "Invalid Status." });

            using (var uow = new UnitOfWork())
            {
                try
                {
                    if (status.ToLower() == "approved")
                    {
                        SEOBL seobl = new SEOBL();
                        seobl.MapCategoryToProperty(propertyId);

                    }
                    string result = uow.PropertyBL.UpdatePropertyStatus(propertyId, status);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = result });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Operation failed.Try again." });
                }
            }
        }

        [HttpPost]
        public ActionResult UpdateArea(int propertyId, int areaId)
        {
            if (propertyId <= 0 || areaId <= 0)
                return Json(new { Success = false, Error = "Invalid Input." });

            using (var uow = new UnitOfWork())
            {
                uow.PropertyBL.UpdatePropertyArea(propertyId, areaId);
                uow.Commit();
                return Json(new { Success = true });
            }
        }

        [HttpGet]
        public ActionResult PostPG()
        {
            using (var uow = new UnitOfWork())
            {
                var amenities = uow.AmenityMasterBL.GetAll();
                ViewBag.roomAmenity = amenities.Where(x => x.AmenityType == "PGRoom");
                ViewBag.pgAmenity = amenities.Where(x => x.AmenityType == "PG");
                ViewBag.cityMaster = uow.CityMasterBL.GetAllCity();
            }
            return View();
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PostPG(PropertyPG property)
        {
            if (property.PropertyMaster.CreatedBy <= 0)
                return ValidationMessage("PG Owner not supplied.");
            /****************PG Room Detail*************/
            if (property.RoomRentalDetail == null || property.RoomRentalDetail.Count() <= 0)
                return ValidationMessage("Rental detail required.");

            foreach (var room in property.RoomRentalDetail)
            {
                if (!string.IsNullOrEmpty(room.RoomType) && room.RoomType.ToLower() != "single" && room.RoomType.ToLower() != "double" && room.RoomType.ToLower() != "three" && room.RoomType.ToLower() != "four")
                    return ValidationMessage("Invalid room type.");

                //if (room.ExpectedRent <= 0 || room.ExpectedDeposit <= 0 || room.ExpectedRent > room.ExpectedDeposit)
                //    return ValidationMessage("Expected rent can't be more than deposit.");
                if (room.TotalSeat <= 0 || room.AvailableSeat <= 0 || room.TotalSeat < room.AvailableSeat)
                    return ValidationMessage("Total seat should be more than available seat.");
            }

            if (property.RoomAmenities != null)
            {
                foreach (var roomAmenity in property.RoomAmenities)
                {
                    if (!Validation.IsInt(roomAmenity.AmenityId))
                        return ValidationMessage("Invalid room amenities.");
                }
            }

            /*********** Locality Detail ***********/
            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonName))
                return ValidationMessage("Contact person name required.");
            else if (property.PropertyMaster.ContactPersonName.Length > 50)
                return ValidationMessage("Contact person name can't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.PropertyName))
                return ValidationMessage("Property Name required.");
            else if (property.PropertyMaster.PropertyName.Length > 100)
                return ValidationMessage("Property Name can't be more than 100 characters.");

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonMobile))
                return ValidationMessage("Contact person mobile no required.");
            else if (Validation.MobileNo(property.PropertyMaster.ContactPersonMobile))
                return ValidationMessage("Invalid contact person mobile no.");

            //if (string.IsNullOrWhiteSpace(property.PropertyMaster.City))
            //    return ValidationMessage("City required.");
            // if (!City.GetAll().ContainsKey(property.PropertyMaster.City))
            //   return ValidationMessage("Invalid city.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Locality))
                return ValidationMessage("Locality required.");
            if (property.PropertyMaster.Locality.Length > 100)
                return ValidationMessage("Locality can't be more than 100 characters.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Latitude) || string.IsNullOrEmpty(property.PropertyMaster.Latitude))
                return ValidationMessage("Please select locality from list.");

            if (!string.IsNullOrWhiteSpace(property.PropertyMaster.Street) && property.PropertyMaster.Street.Length > 100)
                return ValidationMessage("Address can't be more than 100 characters.");

            /**************** PG Detail ****************/
            if (string.IsNullOrEmpty(property.PgDetail.TenantGender))
                return ValidationMessage("PG available for required.");
            else if (property.PgDetail.TenantGender.ToLower() != "male" && property.PgDetail.TenantGender.ToLower() != "female" && property.PgDetail.TenantGender.ToLower() != "co-living" && property.PgDetail.TenantGender.ToLower() != "roomrk")
                return ValidationMessage("Invalid PG available for type.");

            if (!string.IsNullOrEmpty(property.PgDetail.PreferredGuest) && !PreferredGuest.GetAll().ContainsKey(property.PgDetail.PreferredGuest))
                return ValidationMessage("Invalid preferred guest.");

            if (!string.IsNullOrEmpty(property.PgDetail.AvailableFrom))
            {
                string availableFrom = property.PgDetail.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    property.PgDetail.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationMessage("Invalid available from date format.");
            }

            /*************** PG Rules ******************/

            if (property.pgRules != null)
            {
                foreach (var rule in property.pgRules)
                {
                    if (!PGRulesMaster.GetAll().ContainsKey(rule.RuleId))
                        return ValidationMessage("Invalid PG rules.");
                }
            }

            List<PGRule> pgRules = new List<PGRule>();
            if (property.pgRules != null)
            {
                foreach (var rule in property.pgRules)
                {
                    var item = PGRulesMaster.GetAll().Where(x => x.Key == rule.RuleId).FirstOrDefault();
                    pgRules.Add(new PGRule { RuleId = item.Key, RuleName = item.Value });
                }
            }

            property.pgRules = pgRules;
            property.PgDetail.PreferredGuest = (!string.IsNullOrWhiteSpace(property.PgDetail.PreferredGuest) ? PreferredGuest.GetAll().Where(x => x.Key.ToLower() == property.PgDetail.PreferredGuest.ToLower()).FirstOrDefault().Value : null);
            property.PropertyMaster.PostedBy = EncreptDecrpt.Decrypt(CurrentUser.User.UserId);
            using (var uow = new UnitOfWork())
            {
                try
                {
                    int propertyId = uow.PropertyBL.AddNewPG(property);
                    if (propertyId > 0)
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult UpdatePGFeedbackStatus(int id, bool status)
        {
            if (id > 0)
            {
                using (var uow = new UnitOfWork())
                {
                    uow.PropertyBL.UpdatePropertyFeedbackStatus(id, status);
                    uow.Commit();
                    return Json(new { Success = true, Message = "Status Update Successfully." });
                }
            }
            return ValidationMessage("Something went wrong.");
        }

        [HttpGet]
        public ActionResult ListPG(int page = 1, string status = "")
        {
            int pageSize = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["TablePageSize"]);
            int totalRecord;
            int pageIndex = page;

            IEnumerable<PGListing> pgList = null;
            using (UnitOfWork uow = new UnitOfWork())
            {

                pgList = uow.PropertyBL.GetAllPG(pageSize, pageIndex, status, out totalRecord);
                ViewBag.Pager = new Pager(totalRecord, pageIndex, pageSize);
            }

            ViewBag.Status = status;

            return View(pgList);
        }

        [HttpGet]
        public ActionResult ListPGFeedBack(int pid = 0)
        {
            IEnumerable<PropertyFeedBack> lst = null;
            using (var uow = new UnitOfWork())
            {
                if (pid > 0)
                    lst = uow.PropertyBL.GetPGFeedBack(pid);
                else
                    lst = uow.PropertyBL.GetAllPGFeedBack();
            }

            ViewBag.PGId = pid;
            return View(lst);
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult AddPGFeedback(PropertyFeedBack entity)
        {
            if (entity.PropertyId > 0)
            {
                if (!string.IsNullOrEmpty(entity.Description) && entity.Description.Length > 500)
                    return ValidationMessage("Description should not be more than 500 characters.");

                if (string.IsNullOrEmpty(entity.FeedBackByName))
                    return ValidationMessage("Feedback By Name required.");
                else if (entity.FeedBackByName.Length > 50)
                    return ValidationMessage("Feedback By Name should not be more than 50 characters.");

                using (var uow = new UnitOfWork())
                {
                    entity.IsActive = true;
                    uow.PropertyBL.AddPGFeedback(entity);
                    uow.Commit();
                    return Json(new { Success = true, Message = "Data Added Successfully." });
                }
            }
            return ValidationMessage("Something went wrong.");
        }
        private JsonResult ValidationMessage(string message)
        {
            return Json(new { Success = false, ErrorMessage = message });
        }

        [HttpGet]
        public ActionResult Post()
        {
            using (var uow = new UnitOfWork())
            {
                ViewBag.furnishingMaster = uow.PropertyBL.GetAllFurnishing();
                ViewBag.amenityMaster = uow.AmenityMasterBL.GetAll().Where(x => string.IsNullOrEmpty(x.AmenityType));
            }
            return View();
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PostFlatRoom(PropertyFlatRoom property)
        {
            /*********Property Detail **********/
            if (string.IsNullOrEmpty(property.PropertyMaster.PropertyType))
                return ValidationMessage("Property type required.");

            string propertyType = property.PropertyMaster.PropertyType.ToUpper();
            if (propertyType != EnPropertyType.Flat.ToString().ToUpper() && propertyType != EnPropertyType.Room.ToString().ToUpper() && propertyType != EnPropertyType.Flatmate.ToString().ToUpper())
                return ValidationMessage("Invalid Property type.");

            if (string.IsNullOrWhiteSpace(property.PropertyDetail.ApartmentType))
                return ValidationMessage("Apartment type required.");
            if (!ApartmentType.GetAll().ContainsKey(property.PropertyDetail.ApartmentType))
                return ValidationMessage("Invalid apartment type.");

            if (propertyType == "FLAT" || propertyType == "FLATMATE")
            {
                if (string.IsNullOrWhiteSpace(property.PropertyDetail.BhkType))
                    return ValidationMessage("BHK type required.");
                if (!BHKType.GetAll().ContainsKey(property.PropertyDetail.BhkType))
                    return ValidationMessage("Invalid BHK type.");

            }

            if (propertyType == "FLATMATE" || propertyType == "ROOM")
            {
                if (string.IsNullOrWhiteSpace(property.PropertyDetail.RoomType))
                    return ValidationMessage("Room type required.");
                if (!RoomType.GetAll().ContainsKey(property.PropertyDetail.RoomType))
                    return ValidationMessage("Invalid Room type.");
            }

            if (propertyType == "FLATMATE")
            {
                if (!string.IsNullOrWhiteSpace(property.PropertyDetail.TenantGender) && property.PropertyDetail.TenantGender.ToLower() != "male" && property.PropertyDetail.TenantGender.ToLower() != "female")
                    return ValidationMessage("Invalid gender type.");
            }

            else if (!string.IsNullOrWhiteSpace(property.PropertyDetail.PropertySize) && (property.PropertyDetail.PropertySize.Length > 5 || !Validation.IsInt(property.PropertyDetail.PropertySize)))
                return ValidationMessage("Invalid Property size.");

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.Facing) && !FacingType.GetAll().ContainsKey(property.PropertyDetail.Facing))
                return ValidationMessage("Invalid facing type.");

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.PropertyAge) && !PropertyAgeType.GetAll().ContainsKey(property.PropertyDetail.PropertyAge))
                return ValidationMessage("Invalid property age.");

            if (property.PropertyDetail.FloorNo != null && property.PropertyDetail.TotalFloor == null)
                return ValidationMessage("Total floor required.");
            if (property.PropertyDetail.FloorNo == null && property.PropertyDetail.TotalFloor != null)
                return ValidationMessage("Floor no required.");
            if (property.PropertyDetail.FloorNo != null && property.PropertyDetail.TotalFloor != null && property.PropertyDetail.FloorNo > property.PropertyDetail.TotalFloor)
                return ValidationMessage("Floor can't be greater than Total Floor.");

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.WaterSupply) && !WaterSupplyType.GetAll().ContainsKey(property.PropertyDetail.WaterSupply))
                return ValidationMessage("Invalid water supply type.");

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.GateSecurity) && property.PropertyDetail.GateSecurity != "Yes" && property.PropertyDetail.GateSecurity != "No")
                return ValidationMessage("Invalid Gate security.");

            /*********** Locality Detail ***********/

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonName))
                return ValidationMessage("Contact person name required.");
            else if (property.PropertyMaster.ContactPersonName.Length > 50)
                return ValidationMessage("Contact person name can't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonMobile))
                return ValidationMessage("Contact person mobile no required.");
            else if (Validation.MobileNo(property.PropertyMaster.ContactPersonMobile))
                return ValidationMessage("Invalid contact person mobile no.");

            //if (string.IsNullOrWhiteSpace(property.PropertyMaster.City))
            //    return ValidationMessage("City required.");
            // if (!City.GetAll().ContainsKey(property.PropertyMaster.City))
            //   return ValidationError("Invalid city.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Locality))
                return ValidationMessage("Locality required.");
            if (property.PropertyMaster.Locality.Length > 100)
                return ValidationMessage("Locality can't be greater than 100 characters.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Latitude) || string.IsNullOrEmpty(property.PropertyMaster.Latitude))
                return ValidationMessage("Please select Locality from list.");

            if (!string.IsNullOrWhiteSpace(property.PropertyMaster.Street) && property.PropertyMaster.Street.Length > 100)
                return ValidationMessage("Property address can't be more than 100 characters.");

            /**************** Rental Detail **********/
            //if (property.PropertyDetail.OwnerExpectedRent == 0)
            //    return ValidationMessage("Owner Expected Rent required.");

            //if (property.PropertyDetail.OwnerExpectedDeposit == 0)
            //    return ValidationMessage("Owner Expected Deposit required.");

            if (property.PropertyDetail.ExpectedRent == 0)
                return ValidationMessage("Expected Rent required.");

            //if (property.PropertyDetail.ExpectedDeposit == 0)
            //    return ValidationMessage("Expected Deposit required.");

            if (!string.IsNullOrEmpty(property.PropertyDetail.AvailableFrom))
            {
                string availableFrom = property.PropertyDetail.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    property.PropertyDetail.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationMessage("Invalid available from date format.");
            }

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.PreferredTenant) && !PreferredTenant.GetAll().ContainsKey(property.PropertyDetail.PreferredTenant))
                return ValidationMessage("Invalid preferred tenant type.");

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.Parking) && !ParkingType.GetAll().ContainsKey(property.PropertyDetail.Parking))
                return ValidationMessage("Invalid parking type.");

            if (string.IsNullOrWhiteSpace(property.PropertyDetail.Furnishing))
                return ValidationMessage("Furnishing type required.");
            if (!FurnishingType.GetAll().ContainsKey(property.PropertyDetail.Furnishing))
                return ValidationMessage("Invalid furnishing type.");
            else
            {
                if (property.PropertyDetail.Furnishing == "FF" || property.PropertyDetail.Furnishing == "SF")
                {
                    if (property.PropertyDetail.FurnishingDetails == null)
                        return ValidationMessage("Furnishing details required.");
                }
            }

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.Description) && property.PropertyDetail.Description.Length > 500)
                return ValidationMessage("Description can't be more than 500 characters.");

            property.PropertyDetail.ApartmentType = (!string.IsNullOrWhiteSpace(property.PropertyDetail.ApartmentType) ? ApartmentType.GetAll().Where(x => x.Key == property.PropertyDetail.ApartmentType).Select(x => x.Value).FirstOrDefault() : null);

            if (propertyType == "FLAT" || propertyType == "FLATMATE")
                property.PropertyDetail.BhkType = (!string.IsNullOrWhiteSpace(property.PropertyDetail.BhkType) ? BHKType.GetAll().Where(x => x.Key == property.PropertyDetail.BhkType).Select(x => x.Value).FirstOrDefault() : null);
            else
                property.PropertyDetail.BhkType = null;

            if (propertyType == "FLATMATE" || propertyType == "ROOM")
                property.PropertyDetail.RoomType = (!string.IsNullOrWhiteSpace(property.PropertyDetail.RoomType) ? RoomType.GetAll().Where(x => x.Key == property.PropertyDetail.RoomType).Select(x => x.Value).FirstOrDefault() : null);
            else
                property.PropertyDetail.RoomType = null;

            if (propertyType != "FLATMATE")
                property.PropertyDetail.TenantGender = null;

            property.PropertyDetail.Facing = (!string.IsNullOrWhiteSpace(property.PropertyDetail.Facing) ? FacingType.GetAll().Where(x => x.Key == property.PropertyDetail.Facing).Select(x => x.Value).FirstOrDefault() : null);
            property.PropertyDetail.PropertyAge = (!string.IsNullOrWhiteSpace(property.PropertyDetail.PropertyAge) ? PropertyAgeType.GetAll().Where(x => x.Key == property.PropertyDetail.PropertyAge).Select(x => x.Value).FirstOrDefault() : null);
            property.PropertyDetail.WaterSupply = (!string.IsNullOrWhiteSpace(property.PropertyDetail.WaterSupply) ? WaterSupplyType.GetAll().Where(x => x.Key == property.PropertyDetail.WaterSupply).Select(x => x.Value).FirstOrDefault() : null);
            property.PropertyDetail.PreferredTenant = (!string.IsNullOrWhiteSpace(property.PropertyDetail.PreferredTenant) ? PreferredTenant.GetAll().Where(x => x.Key == property.PropertyDetail.PreferredTenant).Select(x => x.Value).FirstOrDefault() : null);
            property.PropertyDetail.Parking = (!string.IsNullOrWhiteSpace(property.PropertyDetail.Parking) ? ParkingType.GetAll().Where(x => x.Key == property.PropertyDetail.Parking).Select(x => x.Value).FirstOrDefault() : null);
            property.PropertyDetail.Furnishing = FurnishingType.GetAll().Where(x => x.Key == property.PropertyDetail.Furnishing).Select(x => x.Value).FirstOrDefault();
            property.PropertyMaster.CreatedBy = property.PropertyMaster.CreatedBy;

            using (var uow = new UnitOfWork())
            {
                try
                {
                    int propertyId = uow.PropertyBL.AddPropertyFlatRoomFlatMate(property);
                    uow.Commit();
                    return Json(new { Success = true });

                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again" });
                }
            }
        }

        //[HttpPost]
        //[AjaxRequestValidation]        
        //public ActionResult SavePropertySEO(AddPropertySEOModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        SEOBL bl = new SEOBL();
        //        SEODE de = new SEODE()
        //        {
        //            PropertyId=model.PropertyId,
        //            MetaTitle=model.MetaTitle,
        //            MetaKeyword=model.MetaKeyword,
        //            MetaDesc=model.MetaDesc,
        //            MetaJson=model.JsonDesc,
        //            UserId=Validation.DecryptToInt(CurrentUser.User.UserId)
        //        };

        //        bl.AddPropertySEO(de);
        //        return JsonResultHelper.Success("Data update successfully.");
        //    }
        //    else
        //        return JsonResultHelper.ModelError(ModelState);            
        //}

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult SavePropertySEOCategory(int pId, int catId)
        {
            if (pId <= 0)
                return JsonResultHelper.Error("Invalid propertyId");
            if (catId <= 0)
                return JsonResultHelper.Error("Invalid categoryId");

            SEODE de = new SEODE()
            {
                PropertyId = pId,
                CategoryId = catId
            };

            SEOBL _seo = new SEOBL();
            string res = _seo.AddPropertyCategory(de);
            if (string.IsNullOrEmpty(res))
                return JsonResultHelper.Success("Category added successfully.", _seo.GetPropertyCategory(de));
            else
                return JsonResultHelper.Error("Category already exist.");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult RemovePropertyCategory(int pId = 0, int pcatId = 0)
        {
            if (pId <= 0)
                return JsonResultHelper.Error("Invalid input.");
            if (pcatId <= 0)
                return JsonResultHelper.Error("Invalid input.");

            SEODE de = new SEODE()
            {
                PropertyId = pId,
                Id = pcatId
            };

            SEOBL bl = new SEOBL();
            bl.RemovePropertyCategory(de);
            return JsonResultHelper.Success("Category removed.", bl.GetPropertyCategory(de));
        }

        [HttpGet]
        public ActionResult AddOffer(int id)
        {
            PropertyBL bl = new PropertyBL();
            var offer = bl.GetOffer(id);

            if (offer == null)
                return PartialView("_Error");

            AddOffer model = new AddOffer
            {
                PropertyId = offer.PropertyId,
                Title = offer.OfferTitle,
                OfferPercent = offer.OfferPercent == null ? 0 : offer.OfferPercent
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddOffer(AddOffer model)
        {
            if (ModelState.IsValid)
            {
                PropertyBL bl = new PropertyBL();
                bl.AddOffer(model.Title, model.OfferPercent, model.PropertyId);
            }

            return RedirectToAction("Index");
        }


        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult GenerateCategory(int propertyId, string lat, string lng)
        {
            try
            {
                if (propertyId <= 0)
                    return JsonResultHelper.Error("propertyId required.");
                if (string.IsNullOrWhiteSpace(lat))
                    return JsonResultHelper.Error("lat required.");
                if (string.IsNullOrWhiteSpace(lng))
                    return JsonResultHelper.Error("lng required.");

                AddPropertyCategory(lat, lng, propertyId);
                return JsonResultHelper.Success("Category mapped successfully.");
            }
            catch (Exception e)
            {
                return JsonResultHelper.Error(e.Message);
            }
        }

        private void AddPropertyCategory(string lat, string lng, int propertyid)
        {
            try
            {
                SEOBL _seoBL = new SEOBL();
                PropertyBL bl = new PropertyBL();

                dynamic pdata = bl.GetPropertyDetailForSEO(propertyid);

                string propertyType = pdata.PropertyType;
                string propertyLocality = pdata.Locality;
                string prefix = string.Empty;

                if (pdata.PropertyType == "PG")
                    prefix = (pdata.TenantGender == "Male" ? "Boys"
                        : pdata.TenantGender == "Female" ? "Girls" : "Co-Living");

                else if (pdata.PropertyType == "FLAT")
                    prefix = pdata.BhkType;
                else if (pdata.PropertyType == "ROOM")
                    prefix = pdata.RoomType;
                else if (pdata.PropertyType == "FLATMATE")
                    prefix = (pdata.TenantGender == "Male" ? "Boys"
                        : pdata.TenantGender == "Female" ? "Girls" : "Co-Living");

                ///// Add Property SEO ////////////////////////////////               
                SEODE de = new SEODE()
                {
                    PropertyId = propertyid,
                    MetaTitle = string.Concat(prefix, " ", pdata.Locality),
                    MetaKeyword = string.Concat(prefix, " ", pdata.Locality),
                    MetaDesc = string.Concat(prefix, " ", pdata.Locality),
                    MetaJson = "",
                    UserId = Validation.DecryptToInt(CurrentUser.User.UserId)
                };

                _seoBL.AddPropertySEO(de);
                ////////////////////////////////////////////


                /////// Add New Category ////////////////////////                
                List<SEOCategory> lst = new List<SEOCategory>();

                lst.Add(_seoBL.GenerateCategory(propertyLocality, propertyType.ToLower(), prefix, lat, lng));

                string googleResponse = string.Empty;
                string[] types = { "restaurant", "transit_station" };
                var googleApiKey = System.Configuration.ConfigurationManager.AppSettings["GooglePlaceApiKey"].ToString();
                foreach (var type in types)
                {
                    var url = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={lat},{lng}&radius=1500&type={type}&key={googleApiKey}";
                    using (WebClient wc = new WebClient())
                    {
                        googleResponse = wc.DownloadString(url);
                    }

                    dynamic googleData = Newtonsoft.Json.JsonConvert.DeserializeObject(googleResponse);

                    dynamic results = googleData.results;
                    foreach (var item in results)
                    {
                        string category = item.name + " " + item.vicinity + " " + pdata.City;
                        string itemLat = item.geometry.location.lat;
                        string itemLng = item.geometry.location.lng;

                        var objseoCategory = _seoBL.GenerateCategory(category, propertyType.ToLower(), prefix, itemLat, itemLng);

                        lst.Add(objseoCategory);
                    }
                }

                foreach (var item in lst)
                {
                    SEODE seode = new SEODE
                    {
                        Category = item.Category,
                        Url = item.Url,
                        MetaTitle = item.MetaTitle,
                        MetaKeyword = item.MetaKeyword,
                        MetaDesc = item.MetaDesc,
                        MetaJson = "",
                        Heading = item.Heading,
                        HeadingContent = item.HeadingContent,
                        PropertyType = item.PropertyType,
                        Prefix = item.Prefix,
                        Lat = item.Lat,
                        Lng = item.Lng,
                        City = pdata.City,
                        UserId = Validator.Validation.DecryptToInt(CurrentUser.User.UserId)
                    };

                    _seoBL.AddCategory(seode);
                }

                /////////////////////////////////////////////////////////////////
            }
            catch (Exception e)
            {
                throw new Exception("google api error: " + e.Message);
            }
        }
    }
}