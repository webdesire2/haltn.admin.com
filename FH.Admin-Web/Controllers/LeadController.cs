﻿using FH.Web.Security;
using System.Web.Mvc;
using FH.Admin.Entities;
using FH.Admin.Service;
using System;
using FH.Admin_Web.ViewModel;
using FH.Util;
using System.Collections.Generic;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class LeadController : BaseController
    {
        private readonly LeadBL _leadBL;

        public LeadController()
        {
            _leadBL = new LeadBL();
        }

        [HttpGet]
        public ActionResult SiteVisit()
        {
            LeadDE de = new LeadDE()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now
            };

            var data=_leadBL.GetSiteVisit(de);

            return View(data);
        }

        [HttpGet]
        public ActionResult AddOfflineLead()
        {
            return View();
        }

        [HttpGet]       
        public PartialViewResult PropertyForLead(PropertySearchForLead req)
        {
            string roomType = "";
            if (req.PType.ToUpper() == "PG")
            {
                if (!string.IsNullOrEmpty(req.RoomType) && req.RoomType == "Private Room")
                    roomType += roomType == "" ? "Single,Single & Sharing" : ",Single,Single & Sharing";
                if (!string.IsNullOrEmpty(req.RoomType) && req.RoomType == "Shared Room")
                    roomType += roomType == "" ? "Single & Sharing,Sharing" : ",Single & Sharing,Sharing";
            }
            else
                roomType = req.RoomType;

            LeadDE de = new LeadDE()
            {
                PropertyIds = req.PId,
                PropertyType = req.PType,
                Latitude = req.Lat,
                Longitude = req.Lng,
                Gender=req.Gender,
                Apartment=req.Apartment,
                Bhk=req.Bhk,
                RoomType=roomType,
                Tenant=req.Tenant
            };

            var data = _leadBL.GetProperty(de);
            return PartialView("_PropertyForLead", data);
        }

        [AjaxRequestValidation]
        [HttpPost]
        public JsonResult AddOfflineLead(AddOfflineLead req)
        {
            if (string.IsNullOrWhiteSpace(req.ClientName))
                return JsonResultHelper.Error("Client name required.");
             if (req.ClientName.Length > 50)
                return JsonResultHelper.Error("Client name can't be more than 50 characters.");

             if(string.IsNullOrWhiteSpace(req.ClientMobile))
                return JsonResultHelper.Error("Client mobile required.");

            if (Validator.Validation.MobileNo(req.ClientMobile))
                return JsonResultHelper.Error("Invalid mobile no.");

            if (req.PId.Length == 0)
                return JsonResultHelper.Error("Please select property.");

            LeadDE de = new LeadDE()
            {
                Name = req.ClientName,
                Mobile = req.ClientMobile,
                PropertyIds = string.Join(",", req.PId)
            };

            var data=_leadBL.AddOfflineLead(de);
            
            if(data!=null)
            {
                foreach(var item in data)
                { 
                    //SendSMS.Send(req.ClientMobile,item.CustomerMsg);
                    string templateID = "1207162468545629475";
                    SendSMS.Send(item.ContactPersonMobile,item.OwnerMsg, templateID);
                }

                return JsonResultHelper.Success("Message sent successfully.");
            }

            return JsonResultHelper.Error("Something went wrong.");
        }

        [HttpGet]
        public ActionResult AllLead()
        {
            return View();
        }

        [HttpGet] 
        public ActionResult WebsiteLead(WebsiteLeadVM req)
        {
            if(req.StartDate == null || req.EndDate == null)
            { 
                req.StartDate = DateTime.Now.ToString();
                req.EndDate = DateTime.Now.AddDays(-120).ToString();
            }
            LeadDE de = new LeadDE()
            {
                StartDate = DateTime.Parse(req.StartDate),
                EndDate = DateTime.Parse(req.EndDate)
            }; 

            var data = _leadBL.GetWebsiteLeads(de);

            req.Leads = data;

            return View(req);
        }



        [HttpPost]
        public PartialViewResult AllLeadList(GetAllLeadModel req)
        {
           
                LeadDE de = new LeadDE()
                {
                  PropertyIds=req.PropertyId,
                  Latitude=req.Lat,
                  Longitude=req.Lng,
                  Gender=req.gender,
                  LeadId=req.LeadId,
                  PropertyType=req.PropertyType,
                  LeadSource = req.LeadSource
                };

                var data=_leadBL.GetAllPGLead(de);
                return PartialView("_PropertyLeadList", data);           
        }
    }
}