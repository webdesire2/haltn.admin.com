﻿using System.Web.Mvc;
using FH.Admin.Service;
using FH.Admin.Entities;
using System.Collections.Generic;
using FH.Validator;
using FH.Web.Security;
using System.Net;
using System.Linq;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class MasterController :BaseController
    {        
        [HttpGet]
        public ActionResult CityZoneAreaMapping()
        {
            CityZoneAreaMapping entity = new CityZoneAreaMapping();

            using (UnitOfWork uow = new UnitOfWork())
            {
                entity=uow.CityZoneAreaMappingBL.GetDetails();               
            }
                return View(entity);
        }

        [HttpPost]
        public ActionResult AddNewZone(string zoneName, int cityId=0)
        {
            zoneName=zoneName.Trim();         
            Dictionary<string, string> modelError = new Dictionary<string, string>();

            if (cityId <= 0)
                modelError.Add("City", ValidationMessage.invalidInput);
            
            if (Validation.RequiredField(zoneName))
                modelError.Add("Zone", ValidationMessage.required);
            else if(Validation.MaxLength(zoneName,20))
                modelError.Add("Zone", "Max 20 characters allowed.");

            if(modelError.Count==0)
            {
                ZoneMaster entity = new ZoneMaster();
                entity.CityId = cityId;
                entity.ZoneName = zoneName;

                using (var uow = new UnitOfWork())
                {
                    try
                    {
                        string result= uow.CityZoneAreaMappingBL.AddNewZone(entity);
                        uow.Commit();
                        if(result=="Inserted")                        
                            return Json(new { Success = true,Message="Zone added successfully" });
                        else if(result== "Already Exist")
                            return Json(new { Success = false,Message="Zone already exist."});                      
                        else
                            return Json(new { Success = false, Message = "Operation failed.Try again." });
                    }
                    catch
                    {
                        return Json(new { Success = false, Message = "Operation failed.Try again." });
                    }
                }
            }
            else
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Success = false,Error=modelError});
            }
        }

        [HttpPost]
        public ActionResult AddNewArea(string areaName, int cityId = 0,int zoneId=0)
        {
            areaName = areaName.Trim();
            Dictionary<string, string> modelError = new Dictionary<string, string>();
            
            if (cityId <= 0)
                modelError.Add("AreaCity", ValidationMessage.invalidInput);

            if (zoneId <= 0)
                modelError.Add("AreaZone", ValidationMessage.invalidInput);

            if (Validation.RequiredField(areaName))
                modelError.Add("Area", ValidationMessage.required);
            else if (Validation.MaxLength(areaName,50))
                modelError.Add("Area", "Max 50 characters allowed.");

            if (modelError.Count == 0)
            {
                AreaMaster entity = new AreaMaster();
                entity.CityId = cityId;
                entity.AreaName = areaName;
                entity.ZoneId = zoneId;

                using (var uow = new UnitOfWork())
                {
                    try
                    {
                        IEnumerable<AreaMaster> lstArea = new List<AreaMaster>();

                        string result = uow.CityZoneAreaMappingBL.AddNewArea(entity);

                        if (result == "Inserted")
                        {
                            lstArea = uow.CityZoneAreaMappingBL.GetArea();
                            uow.Commit();
                            return Json(new { Success = true, Area = lstArea.Select(x => new { x.City.CityName, x.Zone.ZoneName, x.AreaName }) }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == "Already Exist")
                            return Json(new { Success = false, Message = "Zone-Area already exist." });
                        else
                            return Json(new { Success = false, Message = "Operation failed.Try again." });
                    }
                    catch
                    {
                        return Json(new { Success = false, Message = "Operation failed.Try again." });
                    }
                }
            }
            else
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Success = false, Error = modelError });
            }
        }
        
        [HttpGet]
        public ActionResult GetArea()
        {
            IEnumerable<AreaMaster> lstArea = new List<AreaMaster>();        
                using (var uow = new UnitOfWork())
                {
                    lstArea = uow.CityZoneAreaMappingBL.GetArea();
                }                                  
            return Json(new { Area = lstArea.Select(x => new { x.City.CityName, x.Zone.ZoneName, x.AreaName }) },JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetArea(int cityId)
        {
            using (var uow = new UnitOfWork())
            {
                return Json(new { Area = uow.CityZoneAreaMappingBL.GetArea(cityId).Select(x => new { x.AreaId,x.AreaName }) }, JsonRequestBehavior.AllowGet); 
            }
        }

        [HttpPost]
        public ActionResult GetAllZoneArea(int cityId)
        {
            if(cityId>0)
            {
                using (var uow = new UnitOfWork())
                {
                    var lst=uow.CityZoneAreaMappingBL.GetAllAreaZone(cityId);
                   return Json(new { Zone=lst.ZoneMaster,Area=lst.AreaMaster});
                }
            }
            return Json(new { Zone="",Area=""});
        }

        [HttpGet]
        public JsonResult GetAllCity()
        {
            using (var uow = new UnitOfWork())
            {
               return Json(new { City = uow.CityMasterBL.GetAllCity() },JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetTeam(int departmentId=0)
        {
            using (var uow = new UnitOfWork())
            {
                if (departmentId>0)
                { return Json(new { Team = uow.TeamMasterBL.GetAllTeam().Where(x=>x.DepartmentId==departmentId)}, JsonRequestBehavior.AllowGet); }
                else
                { return Json(new { Team = uow.TeamMasterBL.GetAllTeam() }, JsonRequestBehavior.AllowGet); }
            }
        }

        [HttpGet]
        public JsonResult GetDesignation(int departmentId=0)
        {
            using (var uow = new UnitOfWork())
            {
                if (departmentId>0)
                    return Json(new { Designation = uow.DesignationMasterBL.GetAllDesignation().Where(x => x.DepartmentId == departmentId) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Designation = uow.DesignationMasterBL.GetAllDesignation()}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetZone(int cityId)
        {
            if(cityId>0)
            {
                using (var uow = new UnitOfWork())
                {
                    return Json(new { Zone = uow.CityZoneAreaMappingBL.GetZone(cityId) }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { Success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAreaManagerByArea(int areaId = 0)
        {
            if (areaId > 0)
            {

                using (var uow = new UnitOfWork())
                {
                    IEnumerable<UserDetails> lstAreaManager = uow.UserBL.GetAreaManager(areaId);
                    return Json(new { Success = true, Data = lstAreaManager.Select(x => new { x.UserId, x.Name, x.Email, x.Mobile }) });
                }
            }
            return Json(new { Sucess = false });
        }

    }
}