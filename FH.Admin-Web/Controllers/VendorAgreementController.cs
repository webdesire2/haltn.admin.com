﻿using System.Web.Mvc;
using System.Collections.Generic;
using FH.Admin_Web.ViewModel;
using FH.Validator;
using FH.Admin.Service;
using FH.Web.Security;
using System;
using System.Web;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class VendorAgreementController : BaseController
    {   
        [HttpGet]
        public ActionResult SendEmail()
        {
            int pid = 0;
            if (Request.QueryString["pid"] != null)
                pid =Convert.ToInt32(Request.QueryString["pid"].ToString());

            VendorAgreementBL _bl = new VendorAgreementBL();
            var data=_bl.GetVendorEmail(new Admin.Entities.VendorAgreementParam() {PropertyId=pid });

            ViewBag.Email=data.Email;
            ViewBag.PId = data.PropertyId;
            
            return View(new List<EmailSentStatus>());
        }

        [HttpPost]
        public ActionResult SendEmail(FormCollection fc,HttpPostedFileBase fileUpload)
        {
            string error = string.Empty;

            int pId=Convert.ToInt32(fc["pId"]);
            if (pId <= 0)
                error = "Property Id required.";

            string emailId= fc["emailId"];
            if (string.IsNullOrWhiteSpace(error) && string.IsNullOrWhiteSpace(emailId))
                error = "Email id required";

            else if(string.IsNullOrWhiteSpace(error) && Validation.EmailAddress(emailId))
                error = "Invalid email id";

            if (string.IsNullOrWhiteSpace(error) && fileUpload == null)
                  error = "Please uplaod agreement document.";

            ViewBag.Email = emailId;
            if (string.IsNullOrWhiteSpace(error))
            {
                List<EmailSentStatus> lstEmailStatus = new List<EmailSentStatus>();
                string templatePath = Server.MapPath("/EmailTemplates/VendorAgreement/VendorAgreement.html");
                VendorAgreementBL bl = new VendorAgreementBL();
                  bool mailSuccess= bl.SendAgreementMail(pId,emailId, templatePath, fileUpload.InputStream, fileUpload.FileName);

                EmailSentStatus es = new EmailSentStatus();
                es.Email = emailId;
                es.Status = mailSuccess ? "Email Sent":"Not Sent";

                lstEmailStatus.Add(es);
                return View(lstEmailStatus);                
            }

            ViewBag.error = error;
          
            return View();
              
        }

        [HttpGet]
        public ActionResult ListAgreement()
        {
            return View();
        }
    }
}