﻿using System.Web.Mvc;
using FH.Admin.Entities;
using FH.Web.Security;
using FH.Validator;
using FH.Admin.Service;
using FH.Admin_Web.ViewModel;
using System.Linq;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class VendorController : BaseController
    {
        [HttpGet]
        public ActionResult AgentList()
        {
            using (var uow = new UnitOfWork())
            {
               return View(uow.VendorBL.GetAllAgent());
            }               
        }

        [HttpGet]
        [AjaxRequestValidation]
        public ActionResult getAgentModel(int ownerId)
        {
            if (ownerId>0)
            {
                Agent objAgent = new Agent();
                using (var uow = new UnitOfWork())
                {
                    objAgent=uow.VendorBL.GetAgent(ownerId);                                       
                    return PartialView("_AddEditAgent", objAgent);
                }
            }
            return ValidationError("Something went wrong.");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult UpdateAgent(Agent entity)
        {            
            if (string.IsNullOrEmpty(entity.Name)) return ValidationError("Name required.");
            if (entity.Name.Length > 50) return ValidationError("Name,maximum 50 characters allowed.");

            if (Validation.EmailAddress(entity.Email)) return ValidationError("Invalid email.");
            if (Validation.MobileNo(entity.Mobile)) return ValidationError("Invalid mobile no.");
          
            using (var uow = new UnitOfWork())
            {
                uow.VendorBL.UpdateAgent(entity);
                uow.Commit();
                return Json(new { Success = true});
            }
        }
        
        [HttpGet]
        public ActionResult BuyerList()
        {
            using (var uow = new UnitOfWork())
            {
               return View(uow.VendorBL.GetAllBuyer());
            }   
        }


        [HttpGet]
        public ActionResult AccountList()
        {
            using (var uow = new UnitOfWork())
            {
                UserDetails de = new UserDetails();
                return View(uow.VendorBL.AccountList(de));
            }
        }

        [HttpGet]
        public ActionResult AccountDetails(int id = 0)
        {
            if (id > 0)
            {
                UserDetails de = new UserDetails()
                {
                    AccountId = id
                };

                using (var uow = new UnitOfWork())
                {
                    var owner = uow.VendorBL.AccountList(de).FirstOrDefault();
                    if (owner != null && owner.UserId > 0)
                    {
                        return View(owner);
                    }
                }
            }
            return RedirectToAction("AccountList");
        }

        [HttpGet]
        public ActionResult UpdateAccount(int id = 0)
        {
            if (id > 0)
            {
                UserDetails de = new UserDetails()
                { 
                    AccountId = id
                };

                using (var uow = new UnitOfWork())
                {
                    var owner = uow.VendorBL.AccountList(de).FirstOrDefault();
                    if (owner != null && owner.UserId > 0)
                    { 
                        return View(owner);
                    }
                }
            }
            return RedirectToAction("AccountList");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateAccount(AccountList entity)
        {
            if (string.IsNullOrEmpty(entity.PaytmMID)) return ValidationError("MID Required.");
            if (entity.AccountId == 0) return ValidationError("Account Id Required.");

            using (var uow = new UnitOfWork())
            {
                uow.VendorBL.EditAccount(entity);
                uow.Commit();
                return RedirectToAction("AccountList");
            }
        }

        [HttpGet]
        public ActionResult OwnerList(int id=0,string mobile="")
        {
            using (var uow = new UnitOfWork())
            {
                UserDetails de = new UserDetails()
                {
                    UserId=id.ToString(),
                    Mobile=mobile
                };

                return View(uow.VendorBL.GetAllOwner(de));
            }
        }
       
        private JsonResult ValidationError(string msg)
        {
            return Json(new { Success = false, ErrorMessage = msg });
        }

        [HttpGet]
        public ActionResult AddOwner()
        {   
            return View(new AddOwnerModel());
        }

        [HttpPost]       
        public ActionResult AddOwner(AddOwnerModel req)
        {
            if(ModelState.IsValid)
            {
                OwnerDE de = new OwnerDE()
                {
                    Name = req.Name,
                    Email = req.Email,
                    Password = EncreptDecrpt.Encrypt(req.Mobile),
                    Mobile = req.Mobile
                };

                using (var uow = new UnitOfWork())
                {
                    var owner=uow.VendorBL.GetOwner(de);
                    if (owner != null && owner.Count()>0)
                    {
                        if(owner.Any(x=>x.Mobile==req.Mobile))
                            ModelState.AddModelError("", "Mobile no already exist.");
                        if(owner.Any(x=>x.Email == req.Email))
                            ModelState.AddModelError("", "Email id already exist.");
                    }
                    else
                    {
                        uow.VendorBL.AddOwner(de);
                        uow.Commit();
                        return RedirectToAction("OwnerList");
                    }                    
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult EditOwner(int id=0)
        {
            if (id > 0)
            {
                OwnerDE de = new OwnerDE()
                {
                    UserId = id
                };

                using (var uow = new UnitOfWork())
                {
                    var owner = uow.VendorBL.GetOwner(de).FirstOrDefault();
                    if (owner != null && owner.UserId>0)
                    {
                        AddOwnerModel model = new AddOwnerModel()
                        {
                            UserId = owner.UserId,
                            Name = owner.Name,
                            Email = owner.Email,
                            Mobile = owner.Mobile,
                            IsActive = owner.IsActive
                        };

                        return View(model);
                    }
                }
            }
            return RedirectToAction("OwnerList");
        }        

        [HttpPost]
        public ActionResult EditOwner(AddOwnerModel req)
        {
            if (ModelState.IsValid)
            {    
                using (var uow = new UnitOfWork())
                {
                    OwnerDE de = new OwnerDE()
                    {                                              
                        Mobile = req.Mobile,
                        Email=req.Email
                    };
                    
                    var owner = uow.VendorBL.GetOwner(de);
                    if (owner == null)                    
                        ModelState.AddModelError("", "Invalid user.");                    
                    else if (owner.Where(x=>x.UserId != req.UserId).Any(x=>x.Mobile==req.Mobile))                     
                        ModelState.AddModelError("", "Mobile no already exist.");                   
                    else if(!string.IsNullOrEmpty(req.Email) && owner.Where(x => x.UserId != req.UserId).Any(x => x.Email == req.Email))
                        ModelState.AddModelError("", "Email id already exist.");
                    else
                    {
                        OwnerDE deUser = new OwnerDE()
                        {
                            UserId=req.UserId,
                            Name=req.Name,
                            Email=req.Email,
                            Mobile = req.Mobile,
                            IsActive=req.IsActive
                        };
                        uow.VendorBL.UpdateOwner(deUser);
                        uow.Commit();
                        return RedirectToAction("OwnerList");
                    }
                }
            }

            return View();
        }
    }
}