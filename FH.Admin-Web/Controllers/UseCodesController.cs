﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.Admin.Entities;
using FH.Validator;
using FH.Admin.Service;
using FH.Util;

namespace FH.Admin_Web.Controllers
{
    [CustomAuthorize]
    public class UseCodesController : BaseController
    {
        private readonly UseCodeMasterBL _useCodeBL;

        public UseCodesController()
        {
            _useCodeBL = new UseCodeMasterBL();
        }

        [HttpGet]
        public ActionResult Index()
        {           
            return View(_useCodeBL.GetAll());
        }

        [HttpGet]
        public ActionResult AddNew()
        {
            return PartialView("_AddNew");
        }

        [HttpPost]
        public ActionResult AddNew(UseCodeMaster de)
        {
            string errorMsg=UseCodeValidation.Validate(de);
            if(string.IsNullOrWhiteSpace(errorMsg))
            {
                de.CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId);
                int id = _useCodeBL.AddUseCode(de);
                if(id > 0)
                    return JsonResultHelper.Success("UseCode added successfully.");
                else
                    return JsonResultHelper.Error("UseCode already exist.");
            }
            else
                return JsonResultHelper.Error(errorMsg);
        }

        [HttpGet]
        public ActionResult EditUseCode(UseCodeMaster de)
        {
            if(de.UseCodeId>0)
            {
               var data=_useCodeBL.GetById(de);
                if (data == null)
                   return Json(new {JsonRequestBehavior.AllowGet,Success =false,Message ="Something went wrong."});
                else
                    return PartialView("_EditUseCode",data);
            }
            else
                return JsonResultHelper.Error("Invalid input.");
        }

        [HttpPost]
        public ActionResult UpdateUseCode(UseCodeMaster de)
        {
            if (de.UseCodeId > 0)
            {
                de.UpdatedBy = Validation.DecryptToInt(CurrentUser.User.UserId);
                _useCodeBL.UpdateUseCode(de);
                return JsonResultHelper.Success("Update successfully.");
            }
            else
                return JsonResultHelper.Error("Invalid input.");
        }
    }
}