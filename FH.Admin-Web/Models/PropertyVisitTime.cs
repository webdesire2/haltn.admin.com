﻿using System.Collections.Generic;
namespace FH.Admin_Web
{
    public class PropertyVisitTime
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lst = new Dictionary<string, string>();
            lst.Add("1", "10:00 AM");
            lst.Add("2", "11:00 AM");
            lst.Add("3", "12:00 PM");
            lst.Add("4", "1:00 PM");
            lst.Add("5", "2:00 PM");
            lst.Add("6", "3:00 PM");
            lst.Add("7", "4:00 PM");
            lst.Add("8", "5:00 PM");
            lst.Add("9", "6:00 PM");
            lst.Add("10", "7:00 PM");
            lst.Add("11", "8:00 PM");
            return lst;
        }
    }
}