﻿using System.Collections.Generic;

namespace FH.Admin_Web
{
    public class PreferredGuest
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lsttype = new Dictionary<string, string>();
            lsttype.Add("WP", "Working Professional");
            lsttype.Add("ST", "Student");
            lsttype.Add("BO", "Both");            
            return lsttype;
        }
    }
}