﻿
using System.Collections.Generic;

namespace FH.Admin.Web
{
    public class PlaceType
    {
        public static IEnumerable<string> GetAll()
        {
            IList<string> lstType = new List<string>();
            lstType.Add("airport");
            lstType.Add("atm");
            lstType.Add("bank");
            lstType.Add("bus_station");
            lstType.Add("doctor");
            lstType.Add("gym");
            lstType.Add("hospital");
            lstType.Add("restaurant");
            lstType.Add("school");
            lstType.Add("shopping_mall");
            lstType.Add("train_station");
            lstType.Add("movie_theater");
            return lstType;
        }
    }
}