﻿
using System.Collections.Generic;

namespace FH.Admin_Web
{
    public class RoomType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstType = new Dictionary<string, string>();
            lstType.Add("PR", "Private Room");
            lstType.Add("SR", "Shared Room");            
            return lstType;
        }

    }
}