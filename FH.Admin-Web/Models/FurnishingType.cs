﻿using System.Collections.Generic;

namespace FH.Admin_Web
{
    public class FurnishingType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstType = new Dictionary<string, string>();
            lstType.Add("FF", "Furnished");
            lstType.Add("SF", "Semi-furnished");
            lstType.Add("UF", "Unfurnished");         
            return lstType;
        }
    }
}