﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FH.Validator
{
    public static class Validation
    {
        public static bool RequiredField(string propertyValue)
        {
            if (string.IsNullOrEmpty(propertyValue))
                return true;
            else
                return false;
        }

        public static bool EmailAddress(string propertyValue)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(propertyValue);
            if (match.Success)
                return false;
            else
                return true;
        }

        public static bool MobileNo(string propertyValue)
        {
            Regex regex = new Regex(@"^[0-9]{10}$");
            Match match = regex.Match(propertyValue);
            if (match.Success)
                return false;
            else
                return true;

        }

        public static bool IsInt(object propertyValue)
        {
            int value;            
            if (int.TryParse(propertyValue.ToString(), out value))
                return true;
            else
                return false;
        }

        public static bool MaxLength(string propertyValue, int maxLenght)
        {
            if (string.IsNullOrEmpty(propertyValue))
                return false;
            if (propertyValue.Length > maxLenght)
                return true;
            else
                return false;
        }

        public static bool MinLength(string propertyValue, int minLenght)
        {
            if (propertyValue.Length <= minLenght)
                return true;
            else
                return false;
        }

        public static  bool IsDatetime(string datetime)
        {
            DateTime output;
            if (DateTime.TryParse(datetime, out output))
                return true;
            else
                return false;
        }

        public static int DecryptToInt(string input)
        {
            if (string.IsNullOrEmpty(input))
                return 0;
            else
            {
                string decInput = FH.Web.Security.EncreptDecrpt.Decrypt(input);
                if (string.IsNullOrEmpty(decInput))
                    return 0;
                else if(Convert.ToInt32(decInput) > 0)
                    return Convert.ToInt32(decInput);
                else
                    return 0;
            }
        }

        public static string Trim(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            else
                return input.Trim();
        }
        
        public static KeyValuePair<string,string> String(string name,string value,string validationType,int maxLen=0,int minLen=0)
        {
            KeyValuePair<string, string> modelError;
           
            if(!string.IsNullOrEmpty(validationType))
            {
                string[] aryValidationType=validationType.Split(',');
                foreach(var type in aryValidationType)
                {
                    switch (type.ToLower())
                    {
                        case "required":
                            if (RequiredField(value))
                            { modelError = new KeyValuePair<string, string>(name, "Required."); return modelError; }
                            break;

                        case "max":
                            if (MaxLength(value, maxLen))
                            { modelError = new KeyValuePair<string, string>(name, "Maximum " + maxLen + " characters allowed."); return modelError; }
                            break;

                        case "min":
                            if (MinLength(value, minLen))
                            { modelError = new KeyValuePair<string, string>(name, "Minimum " + minLen + " characters required."); return modelError; }
                            break;

                        case "email":
                            if(EmailAddress(value))
                            { modelError = new KeyValuePair<string, string>(name, "Invalid Email Id."); return modelError; }
                            break;

                        case "mobile":
                            if (MobileNo(value))
                            { modelError = new KeyValuePair<string, string>(name, "Invalid Mobile no."); return modelError; }
                            break;                       

                        default:
                            break;
                    }                                     
                }
            }

            return new KeyValuePair<string, string>();
        }
        
        public static void AddRange(this Dictionary<string,string> source, KeyValuePair<string,string> collection)
        {
            if (collection.Key != null && collection.Value !=null)
            {               
                    if (!source.ContainsKey(collection.Key))
                    {
                    source.Add(collection.Key,collection.Value);
                    }             
            }
        }
        
        public static string String(string value, string validationType, int maxLen = 0, int minLen = 0)
        {

            if (!string.IsNullOrEmpty(validationType))
            {
                string[] aryValidationType = validationType.Split(',');
                foreach (var type in aryValidationType)
                {
                    switch (type.ToLower())
                    {
                        case "required":
                            if (RequiredField(value))
                            { return "Required."; }
                            break;

                        case "max":
                            if (MaxLength(value, maxLen))
                            { return "Maximum " + maxLen + " characters allowed."; }
                            break;

                        case "min":
                            if (MaxLength(value, minLen))
                            { return "Minimum " + maxLen + " characters required."; }
                            break;

                        case "email":
                            if (EmailAddress(value))
                            { return "Invalid email id."; }
                            break;

                        case "mobile":
                            if (MobileNo(value))
                            { return "Invalid mobile number."; }
                            break;

                        default:
                            break;
                    }
                }
            }
            return null;
        }


    }
}