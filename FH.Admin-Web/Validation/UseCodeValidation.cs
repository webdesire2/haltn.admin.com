﻿using FH.Admin.Entities;
using System;
using System.Linq;

namespace FH.Validator
{
    public class UseCodeValidation
    {
        public static string Validate(UseCodeMaster de)
        {
            if (string.IsNullOrWhiteSpace(de.Code))
                return "UseCode required.";
            if (de.Code.Length > 20)
                return "UseCode can't be more than 20 characters.";

            if (string.IsNullOrWhiteSpace(de.UseCodeType))
                return "UseCode Type required.";

            var lstCodeType = Enum.GetValues(typeof(UseCodeType)).Cast<UseCodeType>().Select(x => x.ToString()).ToList();
            if (!lstCodeType.Contains(de.UseCodeType))
                return "Invalid usecode type";

            if (de.UseCodeTypePercentage <= 0)
                return "Invalid use code percentage.";

            return string.Empty;
        }
    }
}