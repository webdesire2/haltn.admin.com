﻿ $(function () {
        $.ajaxSetup({
            error: function (x, status, error) {               
                if (x.status == 403) {
                    alert("Sorry, your session has expired. Please login again to continue");
                    window.location.href = "/user/Login";
                }               
                else if(x.status == 400)
                {                 
                    var response = JSON.parse(x.responseText);
                    if (response.Success == false) {
                        var error = response.Error;
                        for (var key in error) {
                            $("#lbl" + key).text(error[key]);
                        }
                        hideError();
                    }
                }              
            }
        });
    });


function hideError()
{
    $('.error').show();
    setTimeout(function () { $('.error').hide()},3000);
}

function hidePopupAlert()
{
    setTimeout(function () { $('.popupAlert').hide(); }, 2000);
}

function showPopupModal(data)
{
    var bodyDiv = '<div class="modal-body">'+data+'</div>';
    $('#modalPopup').html(bodyDiv);
    $('#myModalPopup').modal('show');
    
    setTimeout(function () { $('#myModalPopup').modal('hide'); }, 3000);
}

////////////////////Post Property////////////////////////////
$('#PropertyDetails').on("click", "#btnSaveProperty", function (e) {
    var apartmentType = $("#ddApartmentType option:selected").val();
    var apartmentName = $('#txtApartmentName').val().trim();
    var facing = $("#ddFacing option:selected").val();
    var propertySize = $('#txtPropertySize').val().trim();
    var age = $('#txtPropertyAge').val().trim();
    var floorNo=$('#txtFloorNo').val().trim();
    var totalFloor=$('#txtTotalFloor').val().trim();
    var bathrooms = $('#txtBathrooms').val().trim();
    var balconies=$('#txtBalconies').val().trim();
    var waterSupply = $("#ddWaterSupply option:selected").val();
    var gateSecurity = $("#ddGateSecurity option:selected").val();

    var bhkType = null;
    var roomType =null;
    var tenantType = null; var nonVegAllowed = null; var cupboard = null;

    var isValidated = true;
    $('.error').text('');

    if ($('#hdnPropertyType').val().toUpperCase() == "FLAT")
    { bhkType = $("#ddBHKType option:selected").val(); }
    else if ($('#hdnPropertyType').val().toUpperCase() == "ROOM")
    { roomType = $("#ddRoomType option:selected").val(); }
    else if($('#hdnPropertyType').val().toUpperCase() == "FLATMATE")
    {
        bhkType = $("#ddBHKType option:selected").val(); roomType = $("#ddRoomType option:selected").val(); tenantType = $("#ddTenantType option:selected").val();
        nonVegAllowed = $("#ddNonVegAllowed option:selected").val(); cupboard=$("txtCupboard").val();
    }

    if(apartmentName.length > 50)
    { $('#lblApartmentName').text('Max 50 characters allowed.'); isValidated = false; }

    if(isNaN(propertySize) && propertySize!="")
    { $('#lblPropertySize').text('Only numeric value allowed.'); isValidated = false; }

    if(isNaN(age) && age!="")
    { $('#lblPropertyAge').text('Only numeric value allowed.'); isValidated = false; }

    if(floorNo == "")
    { $('#lblFloorNo').text('Required.'); isValidated = false; }
    else if (isNaN(floorNo))
    { $('#lblFloorNo').text('Only numeric value allowed.'); isValidated = false; }

    if (totalFloor == "")
    { $('#lblTotalFloor').text('Required.'); isValidated = false; }
    else if (isNaN(totalFloor))
    { $('#lblTotalFloor').text('Only numeric value allowed.'); isValidated = false; }

    if (isNaN(bathrooms) && bathrooms != "")
    { $('#lblBathRoom').text('Only numeric value allowed.'); isValidated = false; }

    if (isNaN(balconies) && balconies != "")
    { $('#lblBalconies').text('Only numeric value allowed.'); isValidated = false; }

    hideError();

    if (isValidated == false)
        return false;

        var postData = {
            PropertyId: $('#hdnPropertyId').val(),
            PropertyType: $('#hdnPropertyType').val().trim(),
            ApartmentType: apartmentType,
            ApartmentName: apartmentName,
            BhkType: bhkType,
            RoomType: roomType,
            TenantType: tenantType,
            NonVegAllowed: nonVegAllowed,
            Cupboard: cupboard,
            PropertySize: propertySize,
            Facing: facing,
            PropertyAge: age,
            FloorNo: floorNo,
            TotalFloor: totalFloor,
            BathRooms: bathrooms,
            Balconies: balconies,
            WaterSupply: waterSupply,
            GateSecurity: gateSecurity
        };

        waitingDialog.show('Loading ...');

        $.ajax({
            url: "/property/UpdatePropertyDetails",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            success: function (data, status) {
                if (data.Success == true) {
                    $('#hdnPropertyId').val(data.pid);                    
                    $('.postPropertyStepBtn a[href="#LocalityDetails"]').tab('show');                    
                }
                else
                    alert("Operation failed,Please try again.");
            },
            complete: function () {
                waitingDialog.hide();
            }

        });
});

$('#RentalDetails').on("click", "#btnSaveRental", function (e) {
    var lease=$("input[name='lease']:checked").val();
    var expectedRent=$('#txtExpectedRent').val().trim();
    var expectedDeposit=$('#txtExpectedDeposit').val().trim();
    var rentNegotiable= $("input[name='chkRentNegotiable']:checked").val();
    var availableFrom=$('#dpAvaliableFrom').val().trim();
    var preferredTenant=$("#ddPreferredTenant option:selected").val().trim();
    var parking=$("#ddParking option:selected").val().trim();
    var description=$("#txtDescription").val().trim();    
    var furnishing = $("#ddFurnishing option:selected").val();
    var objFurnishing =[];

    var isValid = true;
    $('.error').text('');

    if(furnishing=='FF' || furnishing=='SM')
    {
        $.each($(".ddFurnish option:selected"), function () {
            var Key = $(this).parent().attr('id');
            var Value = $(this).val();
            objFurnishing.push({ Key, Value });
        });
        $('input[name="chkFurnish"]').each(function () {
            if ($(this).is(":checked"))
            {
                var Key = $(this).attr("id");
                var Value = "1";
                objFurnishing.push({Key, Value });
            }
        });
    }

    if(expectedRent == "")
    { $('#lblExpectedRent').text('Required.'); isValid = false; }
    else if(isNaN(expectedRent))
    {$('#lblExpectedRent').text('Only Numeric value allowed.');isValid=false; }
    
    if(expectedDeposit !="" && isNaN(expectedDeposit))
    {$('#lblExpectedDeposit').text('Only Numeric value allowed.');isValid=false; }

    if(description.length>500)
    {$('#lblDescription').text('Max 500 characters allowed.');isValid=false; }

    hideError();
    if(isValid)
    {
        var postData = {
            PropertyId: $('#hdnPropertyId').val(),
            AvaliableForLease:lease,
            ExpectedRent: expectedRent,
                ExpectedDeposit: expectedDeposit,
        IsRentNegotiable:rentNegotiable,
        AvailableFrom: availableFrom,
        PreferredTenants: preferredTenant,
        Parking:parking,
        Description:description,
        Furnishing:furnishing,
        furnishingDetail: objFurnishing
    };

    waitingDialog.show('Loading ...');

    $.ajax({
        url: "/property/UpdateRentalDetails",
        type: "POST",
        dataType:"json" ,
        contentType:"application/json; charset=utf-8",
        data: JSON.stringify(postData),
        success: function (data, status) {
            if (data.Success == true) {
                $('.postPropertyStepBtn a[href="#Gallery"]').tab('show');                
            }
            else
                alert("Operation failed,Please try again.");
        } ,
        complete: function () {
            waitingDialog.hide();
        }
    });
}

});

$('#LocalityDetails').on("click", "#btnSaveLocality", function (e) {
    var latitude= $('#hdnLat').val().trim();
    var longitude= $('#hdnLng').val().trim();
    var city=$("#ddCity option:selected").val();
    var locality=$('#txtLocality').val().trim();
    var street = $("#txtStreet").val().trim();

    var isValid = true;
    $('.error').text('');

    if (latitude == "" || longitude == "")
    { $('#lblLocality').text('Please select locality.'); isValid = false; }

    if (city == "")
    { $('#lblCity').text('Required.'); isValid = false; }
       
    if (street == "")
    { $('#lblStreet').text('Required.'); isValid = false; }

                hideError();

                if (isValid) {
                    var postData = {
                        pid: $('#hdnPropertyId').val(),
                        Latitude: latitude,
                        Longitude: longitude,
                        City: city,
                        Locality: locality,
                        Street: street
                    };

                    waitingDialog.show('Loading ...');

                    $.ajax({
                        url: "/property/UpdateLocalityDetails",
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(postData),
                        success: function (data, status) {
                            if (data.Success == true) {
                                if ($('#hdnPropertyType').val().toUpperCase() == 'PG') {
                                    $('.postPropertyStepBtn a[href="#PGDetails"]').tab('show');                                    
                                }

                                else { $('.postPropertyStepBtn a[href="#RentalDetails"]').tab('show');}
                            }
                            else
                                alert("Operation failed,Please try again.");
                        },
                        complete: function () {
                            waitingDialog.hide();
                        }
                    });
                }
});

$('#Amenities').on("click", "#btnSaveAmenities", function (e) {
    var objAmenities = [];
    $('input[name="chkAmenity"]').each(function () {
        if ($(this).is(":checked"))
        {
            var amenity = $(this).attr("data-key");
            objAmenities.push(amenity);
        }
    });

    var postData = {
        amenities: objAmenities,
        propertyId: $('#hdnPropertyId').val(),
    };

    waitingDialog.show('Loading ...');

    $.ajax({
        url: "/property/UpdateAmenities",
        type: "POST",
    dataType:"json" ,
    contentType:"application/json; charset=utf-8",
    data: JSON.stringify(postData),
    success: function (data, status) {
        if (data.Success == true) {
          
        }
        else
            alert(data.Error);
    },   
    complete: function () {
        waitingDialog.hide();
    }
});
});

$('#PGRoomPropertyDetails').on("click", "#btnSavePGRoom", function (e) {
    var roomType = $('#dvPGRoom .active').attr("id");
    var expectedRent=$('#txtExpectedRent').val();
    var expectedDeposit=$('#txtExpectedDeposit').val();
    var isValid = true;
    $('.error').text('');

    var objRoomAmenities = [];
    $('input[name="chkRoomAmenity"]').each(function () {
        if ($(this).is(":checked"))
        {
            var amenity = $(this).attr("data-key");
            objRoomAmenities.push(amenity);
        }
    });

    if (expectedDeposit == "")
    { { $('#lblExpectedDeposit').text('Required.'); isValid = false; } }
    if (isNaN(expectedDeposit) && expectedDeposit != "")
    { $('#lblExpectedDeposit').text('Only numeric value allowed.'); isValid = false; }

    if (expectedRent == "")
    { $('#lblExpectedRent').text('Required.'); isValid = false; }
    else if (isNaN(expectedRent) && expectedRent != "")
    { $('#lblExpectedRent').text('Only numeric value allowed.'); isValid = false; }

    hideError();

    if (isValid) {
        var postData = {
            RoomType: roomType,
            propertyId: $('#hdnPropertyId').val(),
            ExpectedRent: expectedRent,
            ExpectedDeposit: expectedDeposit,
            RoomAmenity: objRoomAmenities
        };

        waitingDialog.show('Loading ...');

        $.ajax({
            url: "/property/UpdatePGRoomDetails",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            success: function (data, status) {
                if (data.Success == true) {
                    $('#hdnPropertyId').val(data.pid);
                    $('.postPropertyStepBtn a[href="#LocalityDetails"]').tab('show');                   
                }
                else
                    alert("Operation failed,Please try again.");
            },
            complete: function () {
                waitingDialog.hide();
            }
        });
    }
});

$('#PGDetails').on("click", "#btnSavePGDetail", function (e) {
        
    var tenantGender = ""; var fooding = ""; var laundry = ""; var wardern = ""; var cleaning = "";
    var objPGRules = [];
          
    tenantGender = $('input[name="rdTenantGender"]:checked').val();
    fooding = $('input[name="rdFooding"]:checked').val();
    laundry = $('input[name="rdLaundry"]:checked').val();
    cleaning = $('input[name="rdCleaning"]:checked').val();
    wardern = $('input[name="rdWarden"]:checked').val();

    $('input[name="chkRules"]').each(function () {
        if ($(this).is(":checked"))
        {
            var Key =parseInt($(this).attr("data-key"));
            var Value = $(this).val();            
            objPGRules.push({Key,Value});
        }
    });        
        
    var postData = {
        pId: $('#hdnPropertyId').val(),
        TenantGender: tenantGender,
        PreferredGuest: $('#ddPreferredGuest option:selected').val(),
        AvailableFrom: $('#dpAvaliableFrom').val(),
        Fooding: fooding,
        Laundry: laundry,
        RoomCleaning: cleaning,
        Warden:wardern,
        Description: $('#txtDescription').val(),
        pgRules: objPGRules
    };

    waitingDialog.show('Loading ...');

    $.ajax({
        url: "/property/UpdatePGDetails",
        type: "POST",
    dataType:"json" ,
    contentType:"application/json; charset=utf-8",
    data: JSON.stringify(postData),
    success: function (data, status) {
        if (data.Success == true) {
            $('.postPropertyStepBtn a[href="#Gallery"]').tab('show');           
        }
        else
            alert("Operation failed,Please try again.");
    },    
    complete: function () {
        waitingDialog.hide();
    }
});
});

$('.propertyStatus').on("click", "#btnPropertyStatus", function (e) {
    var currentStaus = $('#hdnCurrentStatus').val();
    var selectedStatus = $('#ddStatus option:selected').val();
    var propertyId = $('#hdnPropertyId').val();
    var latitude = $('#hdnLat').val().trim();
    var longitude = $('#hdnLng').val().trim();
    var areaManagerId = $('#hdnPropertyAreaManager').val();    
    
    if (latitude == "" || longitude == "")
    { showPopupModal("Locality not selected."); return false; }

    if (currentStaus != selectedStatus) {        
        if(selectedStatus.toLowerCase()=="approved" && areaManagerId<=0)
        {
            showPopupModal("Please assign property to area manager then update the status.");
            return false;
        }        
        var postData = {
            status: selectedStatus,
            propertyId: propertyId,            
            areaManagerId: areaManagerId,
            latitude: latitude,
            longitude:longitude
        };

        waitingDialog.show('Loading ...');
        $.ajax({
            url: "/property/UpdateStatus",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            success: function (data, status) {
                if (data.Success == true) {
                    showPopupModal("Operation Sucessfull.");
                }
                else
                    showPopupModal(data.Error);
            },
            complete: function () {
                waitingDialog.hide();
            }
        });
    }
});

$('.propertyStatus').on("click", "#btnPropertyAssign", function (e) {
    var localityCity = $('#hdnPropertyCity').val();    

    if (localityCity === "") {
        showPopupModal("Property locality details not updated.");
        return false;
    }

    waitingDialog.show();

    $.ajax({
        url: "/property/PropertyAssignModel",
        type: "POST",
        dataType: "html",        
        data:"propertyCity="+localityCity,
        success: function (data, status) {
            if (data != null && data !="") {
                $('#myPropertyModalPopup').html(data);
                $('#myPropertyModalPopup').modal('show');
            }
            } ,
        complete: function () {
            waitingDialog.hide();
        }
    });
});

$('#myPropertyModalPopup').on("change", "#ddAMArea", function () {
    var aId =parseInt($(this).val());   
    if(aId>0)
    {      
        var postData = {areaId:aId};
        $.ajax({
            url: "/property/GetAreaManagerByArea",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data:JSON.stringify(postData),
            success: function (data, status) {
                if (data.Success === true) {
                    $('#tblAreaManager tbody').html('');
                    $.each(data.Data, function (key, value) {
                        $("#tblAreaManager tbody").append("<tr><td><input type=radio name=rdAreaManager value='" + value.UserId +"'/></td><td>" + value.Name + "</td><td>" +
                        value.Email + "</td><td>" + value.Mobile + "</td></tr>");
                    });
                    $('#tblAreaManager').show();
                }
            }            
        });
    }
});

$('#myPropertyModalPopup').on("click", "#btnAssignSave", function (e) {    
    if ($('#hdnPropertyCity').val() === "") {
        showPopupModal("Property locality details not updated.");
        return false;
    }
    
    var selectedManager = $('input[name="rdAreaManager"]:checked').val();
    if (selectedManager > 0)
    {
        $('#hdnPropertyAreaManager').val(selectedManager);
        $('#myPropertyModalPopup').modal('hide');
    }
});

function previousTab(currentTab)
{
    if (currentTab == "locality") {
        if ($('#hdnPropertyType').val().toUpperCase() == 'PG')
        { $('.postPropertyStepBtn a[href="#PGRoomPropertyDetails"]').tab('show'); }
        else { $('.postPropertyStepBtn a[href="#PropertyDetails"]').tab('show'); }
    }
        if (currentTab == "PGRoomPropertyDetails")
        { $('.postPropertyStepBtn a[href="#Gallery"]').tab('show');}

        if (currentTab == "RentalDetails")
        { $('.postPropertyStepBtn a[href="#LocalityDetails"]').tab('show'); }

    if (currentTab == "PGDetails")
    { $('.postPropertyStepBtn a[href="#LocalityDetails"]').tab('show'); }
        
    if (currentTab == "Gallery")
    {
        if ($('#hdnPropertyType').val().toUpperCase() == 'PG') {
            $('.postPropertyStepBtn a[href="#PGDetails"]').tab('show');
        }
        else { $('.postPropertyStepBtn a[href="#RentalDetails"]').tab('show'); }
    }

    if(currentTab=="Amenities")
        $('.postPropertyStepBtn a[href="#Gallery"]').tab('show');
}

function nextTab(currentTab) {
    if (currentTab == "PropertyDetails" || currentTab == "PGRoomPropertyDetails")
    { $('.postPropertyStepBtn a[href="#LocalityDetails"]').tab('show'); }

    if (currentTab == "locality") {
        if ($('#hdnPropertyType').val().toUpperCase() == 'PG')
        { $('.postPropertyStepBtn a[href="#PGDetails"]').tab('show'); }
        else { $('.postPropertyStepBtn a[href="#RentalDetails"]').tab('show'); }
    }

    if(currentTab == "PGDetails" || currentTab == "RentalDetails")
    { $('.postPropertyStepBtn a[href="#Gallery"]').tab('show'); }
    

    if(currentTab=="Gallery")    
        $('.postPropertyStepBtn a[href="#Amenities"]').tab('show');

}

