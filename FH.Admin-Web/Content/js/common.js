﻿$(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            loader.show();
            xhr.setRequestHeader('RequestVerificationToken',$('#RequestVerificationTokenAjax').val());
        },
        error: function (x, status, error) {
            loader.hide();
            if (x.status == 403) {
                alert("Sorry, your session has expired. Please login again to continue");
                window.location.href = "/User/Login";
            }
            else if(x.status==406)
            {
                showError("Unauthorized request");
            }
            else if (x.status == 400) {
                showError("Something went wrong.please try again.");
            }
            else {
                showError("Something went wrong.please try again.");
            }
        },
        complete: function () { loader.hide(); }
    });


});

function showError(text) {
    $('.validation_error').text(text).addClass('active');
    setTimeout(function () { $('.validation_error').text('').removeClass('active'); }, 3000);
    return false;
}

function showSuccessMessage(text) {
    $('.success-message').text(text).addClass('active');
    setTimeout(function () { $('.success-message').text('').removeClass('active'); }, 3000);    
}

function reload(text)
{
    $('.success-message').text(text).addClass('active');
    setTimeout(function () {location.reload()},2000);
}

function redirect(url,txt) {
    if (txt !== '')
        showSuccessMessage(txt);

    window.location.href =url;
}
var loading = {
    show: function (btn) {
        $(btn).html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    },
    hide: function (btn, text) {
        $(btn).html(text).removeAttr("disabled");
    }
};

var loader = {
    show: function () {       
            var div = "<div class='loader'></div>";
            $('body').append(div);
    },
    hide: function () {
        $('.loader').remove();
    }
};

function validateEmail(i) {
    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return regex.test(i) ? true : false;
}

function validateMobile(i) {
    var mobilefilter = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/;
    return mobilefilter.test(i) ? true : false;
}

function validateOnlyAlpha(i) {
    var regex = new RegExp("^[a-zA-Z ]+$");
    return regex.test(i) ? true : false;
}

function isAlpha(event) {
    var value = String.fromCharCode(event.which);
    var regex = new RegExp("^[a-zA-Z ]+$");
    return regex.test(value);
}

function isInt(event) {
    var value = String.fromCharCode(event.which);
    var regex = new RegExp("^[0-9]+$");
    return regex.test(value);
}

function validateNumeric(val)
{
    if (val.match(/^-?\d*$/)) return true;

    return false;
}
$(function () {
    $('.custom_collapse_btn').click(function () {
        $(this).parent('.ibox-title').next('.custom_collapse_body').slideToggle();
        $(this).find('i').toggleClass('fa-minus fa-plus')
    });

    $('.table_collapse_btn').click(function () {
        $(this).parents('tr').next('tr.table_collapse_body').slideToggle();
        $(this).find('i').toggleClass('fa-minus fa-plus')
    });
});
