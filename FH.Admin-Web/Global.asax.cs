﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using FH.Admin.Entities;
using Newtonsoft.Json;
using FH.Web.Security;
using System.Net;

namespace FH.Admin_Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas(); 
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                UserDetails serializeModel = JsonConvert.DeserializeObject<UserDetails>(authTicket.UserData);
                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserId = serializeModel.UserId.ToString();
                newUser.Name = serializeModel.Name;
                newUser.Email = serializeModel.Email;
                newUser.Mobile = serializeModel.Mobile;
                newUser.Roles = new string[1] { "1" };

                HttpContext.Current.User = newUser;
            }
        }
    }
}
