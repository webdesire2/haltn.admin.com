﻿using System.Collections.Generic;
using Newtonsoft.Json;
using FH.Admin.Web;
using System.Net;
using FH.Admin.Entities;

namespace FH.Util
{
    public class NearbySearchApi
    {
        public static IEnumerable<PropertyNearbyPlace> FindPlace(string latitude,string longitude,string apiKey,int propertyId)
        {
            List<PropertyNearbyPlace> lstNearByPlace = new List<PropertyNearbyPlace>();            

            string apiRequest = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key="+apiKey+"&location="+latitude +"," + longitude + "&radius=5000";
            
            foreach (var placeType in PlaceType.GetAll())
            {
                using (var wc = new WebClient())
                {
                   dynamic apiResponse=JsonConvert.DeserializeObject(wc.DownloadString(apiRequest + "&type=" + placeType));                    
                    if(apiResponse.status=="OK")
                    {
                        int count = 1;      
                        foreach(var result in apiResponse.results)
                        {
                            if (count <= 5)
                            {
                                string placeName = result.name;
                                string lat = result.geometry.location.lat;
                                string lng = result.geometry.location.lng;
                                lstNearByPlace.Add(new PropertyNearbyPlace { PropertyId = propertyId, PlaceType = placeType,PlaceName = placeName, Latitude = lat, Longitude = lng,Distance="",TravelTime="" });                            
                            }
                            count++;
                        }
                    }                
                }
            }

            return lstNearByPlace;
        }
    }

    
    
       
}