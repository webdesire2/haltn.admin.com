﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FH.Admin_Web
{
    public class Pager
    {
        public Pager(int totalRecord, int? pageIndex, int pageSize = 100,bool pageLoadEnable=true)
        {
            PageLoad = pageLoadEnable;
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalRecord / (decimal)pageSize);
            var currentPage = pageIndex != null ? (int)pageIndex : 1;
            var startPage = currentPage - 5;
            var endPage = currentPage + 4;
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            TotalRecord = totalRecord;
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
        }

        public int TotalRecord { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
        public bool PageLoad { get; set; } = true;
    }
}