﻿
namespace FH.Util
{
    enum EnPropertyType
    {
     Flat=1,
     Room=2,
     Flatmate=3,
     PG=4                  
    }

    enum EnPropertyStatusByFH
    {
        Pending,
        Rejected,
        Approved,
        RentOut
    }

    enum EnPropertyStatusByAgent
    {
        Active,
        Deactive
    }

    enum EnLeadStatus
    {
        NEW,        
        FOLLOWUP,
        DOCUMENTATION,        
        DONE,
        CANCELLED
    }
}