﻿
using System.Web.Mvc;
using System.Linq;

namespace FH.Util
{
    public class JsonResultHelper
    {
        public static JsonResult Success(string message)
        {
            return new JsonResult()
            {
                Data = new {Success = true, Message = message },
            };
        }

        public static JsonResult Success(string message,object data)
        {
            return new JsonResult()
            {
                Data = new { Success = true, Message = message,Data=data},
            };
        }

        public static JsonResult Error(string message)
        {
            return new JsonResult()
            {
                Data = new {Success = false, Message = message },
            };
        }

        public static JsonResult ModelError(ModelStateDictionary modelState)
        {
            var error = modelState.Values.Where(x=>x.Errors.Count()>0).Select(x => x.Errors.Select(y => y.ErrorMessage).FirstOrDefault()).FirstOrDefault();
            return new JsonResult()
            {
                Data = new { Success = false, Message =error }
            };
        }
    }
}