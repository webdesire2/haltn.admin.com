﻿
////////////////////Post Property////////////////////////////
var propertyPost = {
    PropertyMaster: {},
    PropertyDetail: {},
    GalleryDetails: [],
    AmenityDetails: []    
};

$('.apartment_type').click(function () {
    $('.apartment_type').removeClass('active');
    $(this).addClass('active');
});

$('.bhk-type').click(function () {
    $('.bhk-type').removeClass('active');
    $(this).addClass('active');
});

$('.room-type').click(function () {
    $('.room-type').removeClass('active');
    $(this).addClass('active');
});

$('.tenant-gender').click(function () {
    $('.tenant-gender').removeClass('active');
    $(this).addClass('active');
});

$('.facing').click(function () {
    $('.facing').removeClass('active');
    $(this).addClass('active');
});

$('.bathroom').click(function () {
    $('.bathroom').removeClass('active');
    $(this).addClass('active');
});
$('.balcony').click(function () {
    $('.balcony').removeClass('active');
    $(this).addClass('active');
});
$('.water_supply').click(function () {
    $('.water_supply').removeClass('active');
    $(this).addClass('active');
});
$('.gate_security').click(function () {
    $('.gate_security').removeClass('active');
    $(this).addClass('active');
});



$('.furnish').click(function () {
    $('.furnish').removeClass('active');
    $(this).addClass('active');
    if ($(this).data('key') == 'FF' || $(this).data('key') == 'SF')
    { $('.furnished_item').removeClass('hide'); }
    else
    { $('.furnished_item').addClass('hide'); }
});

$('.preferred_tenant').click(function () {
    $('.preferred_tenant').removeClass('active');
    $(this).addClass('active');
});

$('.parking').click(function () {
    $('.parking').removeClass('active');
    $(this).addClass('active');
});

$('.amenity').click(function () {    
    $(this).toggleClass('active');
});

$('#btn_property_detail').click(function () {
    var roomType = "";
    var tenantGender = "";
    var bhkType = "";
    var propertyType = $('#property_type').val().toLowerCase();
    var apartmentType = $('.apartment_type.active').data('key');

    if (propertyType == "flat" || propertyType == "flatmate")
        bhkType = $('.bhk-type.active').data('key');

    if (propertyType == "flatmate")
        tenantGender = $('.tenant-gender.active').data('key');

    if (propertyType == "flatmate" || propertyType == "room")
        roomType = $('.room-type.active').data('key');

    var apartment = $('#apartment_name').val().trim();
    var area = $('#area').val();
    var facing = $('.facing.active').data('key');
    var propAge = $('#property_age').val();
    var floor = $('#floor').val();
    var totalFloor = $('#total_floor').val();
    var bathroom = $('.bathroom.active').data('key');
    var balcony = $('.balcony.active').data('key');
    var waterSupply = $('.water_supply.active').data('key');
    var gateSecurity = $('.gate_security.active').data('key');
    var isAgentAllowed = $('.agent_contact.active').data('key');

    if (propertyType == "" || propertyType == undefined)
        return showError("Invalid property type.");

    if (apartmentType == "" || apartmentType == undefined)
        return showError("Please select apartment type.");

    if ((propertyType == "flat" || propertyType == "flatmate") && bhkType == "" || bhkType == undefined)
        return showError("Please select BHK type.");

    if ((propertyType == "flatmate" || propertyType == "room") && (roomType == "" || roomType == undefined))
        return showError("Please select Room Type.");

    if (propertyType == "flatmate" && (tenantGender == "" || tenantGender == undefined))
        return showError("Please select Available For.");

    if (apartment.length > 50)
        return showError("Apartment name shouldn't be more than 50 characters.");

    if (area == "" || area == undefined)
        return showError("Please enter build up area.");

    if (facing == "" || facing == undefined)
        facing = "";

    if (propAge == "" || propAge == undefined)
        propAge = "";
    if (floor == "" || floor == undefined)
        floor = "";
    else if (totalFloor == "")
        return showError("Total floor required.");

    if (totalFloor == "" || totalFloor == undefined)
        totalFloor = "";
    else if (floor == "")
        return showError("Floor no required.");

    if (floor != "" && totalFloor != "" && parseInt(floor) > parseInt(totalFloor))
        return showError("Floor no can't be greater than total floor.");

    if (bathroom == undefined)
        bathroom = "";
    if (balcony == undefined)
        balcony = "";
    if (waterSupply == undefined)
        waterSupply = "";
    if (gateSecurity == undefined)
        gateSecurity = "";

    var postData = {
        ApartmentType: apartmentType, BhkType: bhkType, RoomType: roomType, TenantGender: tenantGender, ApartmentName: apartment, PropertySize: area, Facing: facing, PropertyAge: propAge,
        FloorNo: floor, TotalFloor: totalFloor, BathRooms: bathroom, Balconies: balcony, WaterSupply: waterSupply, GateSecurity: gateSecurity, IsAgentAllowed: isAgentAllowed
    }

    propertyPost.PropertyDetail = postData;
    $('#property_detail_info').hide();
    $('#locality_info').show();

});

$('#btn_back_locality').click(function () {
    $('#property_detail_info').show();
    $('#locality_info').hide();
});

$('#btn_locality').click(function () {
    var ownerId = $('#hdnOwnerId').val();
    var propertyType = $('#property_type').val();
    var city = $('#hdnCity').val();
    var locality = $('#locality').val().trim();
    var street = $('#street').val().trim();
    var lat = $('#hdnLat').val().trim();
    var lng = $('#hdnLng').val().trim();
    var contactPersonName = $('#contactPersonName').val();
    var contactPersonMobile = $('#contactPersonMobile').val();
    var locationMapLink = $('#locationMapLink').val();

    if (ownerId === "")
        return showError("Please select Owner.");

    if (contactPersonName === "") { showError("Contact person name required."); return false; }
    else if (contactPersonName.length > 50) { showError("Contact person name can't be more than 50 characters."); return false; }

    if (contactPersonMobile === "") { showError("Contact person mobile no required."); return false; }
    if (!validateMobile(contactPersonMobile)) { showError("Invalid Contact person mobile no."); return false; }

   // if (city == "") { showError("Please select city."); return false }

    if (locality == "") { showError("Locality required."); return false }
    else if (locality.length > 100) { showError("Maximum 100 characters allowed for locality."); return false }

    if (lat == "" || lng == "") { showError("Please select locality from list."); return false }

    if (street.length > 100) { showError("Maximum 100 characters allowed for street/area."); return false }

    var postData = { CreatedBy: ownerId, PropertyType: propertyType, City: city, Locality: locality, Street: street, Latitude: lat, Longitude: lng, ContactPersonName: contactPersonName, ContactPersonMobile: contactPersonMobile, LocationMapLink: locationMapLink };
    propertyPost.PropertyMaster = postData;
    $('#locality_info').hide();
    $('#rental_info').show();
});

$('#btn_back_rental').click(function () {
    $('#locality_info').show();
    $('#rental_info').hide();
});

$('#btn_rental').click(function () {
    //var ownerExpectedRent = $('#ownerRent').val().trim();
    //var ownerExpectedDeposit = $('#ownerDeposit').val().trim();
    var rent = $('#rent').val().trim();
    var deposit = $('#deposit').val().trim();
    var availableFrom = $('#avaliable_from').val();
    var rentNegotiable = false;
    if ($("input[name='rent_negotiable']").is(':checked'))
        rentNegotiable = true;

    var preferredTenant = $('.preferred_tenant.active').data('key');
    var parking = $('.parking.active').data('key');
    var furnishing = $('.furnish.active').data('key');
    var furnishingDetail = [];
    var description = $('#description').val().trim();


    //if (ownerExpectedRent == "" || !validateNumeric(ownerExpectedRent))
    //    return showError("Owner Expected rent should be numeric.");
   
    if (rent == "" || !validateNumeric(rent))
        return showError("Expected rent should be numeric.");
    
    if (preferredTenant == "" || preferredTenant == undefined)
        return showError("Please select Preferred Tenant.");

    if (furnishing == "" || furnishing == undefined)
        return showError("Please select Furnishing.");

    if (furnishing == 'FF' || furnishing == 'SF') {
        $.each($(".ddFurnish option:selected"), function () {
            if ($(this).val() != "") {
                var FurnishingMasterId = $(this).parent().attr('id');
                var Value = $(this).val();
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });

        $('input[name="chkFurnish"]').each(function () {
            if ($(this).is(":checked")) {
                var FurnishingMasterId = $(this).attr("id");
                var Value = "1";
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });
    }

    if (description.length > 500)
        return showError("Description shouldn't be more than 500 characters.");

    var postData = {
        ExpectedRent: rent, ExpectedDeposit: deposit, AvailableFrom: availableFrom, IsRentNegotiable: rentNegotiable, PreferredTenant: preferredTenant,
        Parking: parking, Furnishing: furnishing, FurnishingDetails: furnishingDetail, Description: description
    };

    $.extend(propertyPost.PropertyDetail, postData);
    $('#rental_info').hide();
    $('#gallery_info').show();

});

$('#btn_gallery_back').click(function () {
    $('#rental_info').show();
    $('#gallery_info').hide();
});

$('#gallery_info').on("click", "#btnGallery", function (e) {
    $('#gallery_info').hide();
    $('#amenity_info').show();
});

$('#btn_amenity_back').click(function () {
    $('#gallery_info').show();
    $('#amenity_info').hide();
});

$('#btn_amenity').click(function () {
    var amenities = [];

    $('.amenity').each(function () {
        if ($(this).hasClass('active')) {
            var amenity = $(this).data("key");
            amenities.push({ AmenityId: amenity });
        }
    });

    propertyPost.AmenityDetails = amenities;

    saveFlatRoomDetail("btn_amenity");
});

function saveFlatRoomDetail(btn) {
    var postData = {
        property: propertyPost
    };

    $('#' + btn).html('Please wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/PostFlatRoom",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(postData),
        success: function (data, status) {
            if (data.Success == true)
            {
                showSuccessMessage("Property posted successfully.");
                window.location.href = "/property/index";
            } 
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#' + btn).html('Submit').removeAttr("disabled");
            loader.hide();
        }
    });
}
$('#btn__edit_property_detail').click(function () {    
    var roomType = "";
    var tenantGender = "";
    var bhkType = "";
    var propertyType = $('#PropertyType').val().toLowerCase();
    var apartmentType = $('.apartment_type.active').data('key');

    if (propertyType == "flat" || propertyType == "flatmate")
        bhkType = $('.bhk-type.active').data('key');

    if (propertyType == "flatmate")
        tenantGender = $('.tenant-gender.active').data('key');

    if (propertyType == "flatmate" || propertyType == "room")
        roomType = $('.room-type.active').data('key');

    var apartment = $('#apartment_name').val().trim();
    var area = $('#area').val();
    var facing = $('.facing.active').data('key');
    var propAge = $('#property_age').val();
    var floor = parseInt($('#floor').val());
    var totalFloor = parseInt($('#total_floor').val());
    var bathroom = parseInt($('.bathroom.active').data('key'));
    var balcony = parseInt($('.balcony.active').data('key'));
    var waterSupply = $('.water_supply.active').data('key');
    var gateSecurity = $('.gate_security.active').data('key');

    if (propertyType == "" || propertyType == undefined)
    { showError("Invalid property type."); return false; }

    if (apartmentType == "" || apartmentType == undefined)
    { showError("Please select apartment type."); return false }

    if ((propertyType == "flat" || propertyType == "flatmate") && bhkType == "" || bhkType == undefined)
    { showError("Please select BHK type."); return false }

    if ((propertyType == "flatmate" || propertyType == "room") && (roomType == "" || roomType == undefined)) {
        showError("Please select Room Type."); return false;
    }

    if (propertyType == "flatmate" && (tenantGender == "" || tenantGender == undefined)) {
        showError("Please select Available For."); return false;
    }

    if (apartment.length > 50)
    { showError("Apartment name shouldn't be more than 50 characters."); return false }

    if (area == "")
    { showError("Please enter build up area."); return false }

    if (facing == "" || facing == undefined)
    { showError("Please select facing."); return false }

    if (propAge == "")
    { showError("Please enter property age."); return false }

    if (floor == "")
    { showError("Please enter floor no."); return false }

    if (totalFloor == "")
    { showError("Please enter total floor."); return false }

    if (bathroom == "")
    { showError("Please select bathroom."); return false }

    if (waterSupply == "")
    { showError("Please select water supply type."); return false }

    if (gateSecurity == "")
    { showError("Please select gate security."); return false }

    var postData = {
        ApartmentType: apartmentType, BhkType: bhkType, RoomType: roomType, TenantGender: tenantGender, ApartmentName: apartment, PropertySize: area, Facing: facing, PropertyAge: propAge,
        FloorNo: floor, TotalFloor: totalFloor, BathRooms: bathroom, Balconies: balcony, WaterSupply: waterSupply, GateSecurity: gateSecurity
    };

    var btnId=$(this).attr('id');
    $('#'+btnId).html('Loading <i class="fa fa-refresh fa-spin"></i>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdatePropertyDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#PropertyId').val(), ptype: $('#PropertyType').val(), property: postData }),
        success: function (data, status) {
            if (data.Success == true)
                showError("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#' + btnId).html('Update').removeAttr("disabled");
            loader.hide();
        }
    });

});

$('#btn_edit_locality').click(function () {
    var contactPersonName = $('#contactPersonName').val();
    var contactPersonMobile = $('#contactPersonMobile').val();
    var city = $('#hdnCity').val();
    var locality = $('#locality').val().trim();
    var street = $('#street').val().trim();
    var lat = $('#hdnLat').val().trim();
    var lng = $('#hdnLng').val().trim();
    var locationMapLink = $('#locationMapLink').val();

    if (contactPersonName == "") {
        return showError("Contact person name required.");
    }

    if (contactPersonMobile == "") {
        return showError("Contact person mobile no required.");
    }

    //if (city == "")
    //{ showError("Please select city."); return false }

    if (locality == "")
    { showError("Locality required."); return false }
    else if (locality.length > 100)
    { showError("Maximum 100 characters allowed for locality."); return false }

    if (lat == "" || lng == "")
    { showError("Please select locality from list."); return false }

    if (street.length > 100)
    { showError("Maximum 100 characters allowed for street/area."); return false }

    var postData = { contactPersonName: contactPersonName, contactPersonMobile: contactPersonMobile, City: city, Locality: locality, Street: street, Latitude: lat, Longitude: lng, LocationMapLink: locationMapLink };

    $.ajax({
        url: "/property/UpdateLocalityDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#PropertyId').val(), property: postData }),
        success: function (data, status) {
            if (data.Success == true)
                showError("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        }
    });
});

$('#btn_edit_rental').click(function () {
    //var ownerExpectedRent = $('#ownerRent').val().trim();
    //var ownerExpectedDeposit = $('#ownerDeposit').val().trim();
    var rent = $('#rent').val().trim();
    var deposit = $('#deposit').val().trim();
    var availableFrom = $('#avaliable_from').val();
    var rentNegotiable = false;
    if ($("input[name='rent_negotiable']").is(':checked'))
        rentNegotiable = true;

    var preferredTenant = $('.preferred_tenant.active').data('key');
    var parking = $('.parking.active').data('key');
    var furnishing = $('.furnish.active').data('key');
    var furnishingDetail = [];
    var description = $('#description').val().trim();

    //if (ownerExpectedRent == "" || !validateNumeric(ownerExpectedRent)) { showError("Owner expected rent should be numeric."); return false }
    
    if (rent == "" || !validateNumeric(rent))
    { showError("Expected rent should be numeric."); return false }
    
    if (preferredTenant == "" || preferredTenant == undefined)
    { showError("Please select Preferred Tenant."); return false }

    if (parking == "" || parking == undefined)
    { showError("Please select Parking."); return false }

    if (furnishing == "" || furnishing == undefined)
    { showError("Please select Furnishing."); return false }

    if (furnishing == 'FF' || furnishing == 'SF') {
        $.each($(".ddFurnish option:selected"), function () {
            if ($(this).val() != "") {
                var FurnishingMasterId = $(this).parent().attr('id');
                var Value = $(this).val();
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });

        $('input[name="chkFurnish"]').each(function () {
            if ($(this).is(":checked")) {
                var FurnishingMasterId = $(this).attr("id");
                var Value = "1";
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });
    }

    if (description.length > 500)
    { showError("Description shouldn't be more than 500 characters."); return false }

    var postData = {
        ExpectedRent: rent, ExpectedDeposit: deposit, AvailableFrom: availableFrom, IsRentNegotiable: rentNegotiable, PreferredTenant: preferredTenant,
        Parking: parking, Furnishing: furnishing, FurnishingDetails: furnishingDetail, Description: description
    };

    $.ajax({
        url: "/property/UpdateRentalDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#PropertyId').val(), ptype: $('#PropertyType').val(), property: postData }),
        success: function (data, status) {
            if (data.Success == true)
                showError("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        }
    });

});

$('#gallery_info').on("click", "#btnGallery", function (e) {
    $('#gallery_info').removeClass('in active').addClass('hide');
    $('#amenity_info').removeClass('hide').addClass('in active');
});

$('.post-property-iamges').on("click", ".remove-pic", function (e) {
    var parentLi = $(this).parent();
    var filePath = $(this).data('url');
    if (filePath != "" && filePath != undefined) {
        $.grep(propertyPost.GalleryDetails, function (element, index) {
            if (element.FilePath == filePath) {
                $.ajax({
                    url: "/property/RemoveGalleryFile",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ imagePath: filePath }),
                    success: function (data, status) {
                        if (data.Success == true) {
                            parentLi.remove();
                            propertyPost.GalleryDetails.splice(index, 1);
                        }
                        else if (data.Success == false) {
                            showError(data.ErrorMessage);
                        }
                    }
                });
            }
        });
    }
});

$('#btn_edit_amenity').click(function () {
    var amenities = [];
    $('input[name="chkAmenity"]').each(function () {
        if ($(this).is(":checked")) {
            var amenity = $(this).data("key");
            amenities.push({ AmenityId: amenity });
        }
    });

    $.ajax({
        url: "/property/UpdateAmenity",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), amenities: amenities }),
        success: function (data, status) {
            if (data.Success == true)
                showError("Amenities updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        }
    });

});

$('#area,#property_age,#floor,#total_floor,#rent,#deposit').on('keypress paste', function (e) {
    var txt = String.fromCharCode(e.which);
    if (!validateNumeric(txt))
        return false;
});

$('.propertyStatus').on("click", "#btnPropertyStatus", function (e) {
   // if ($('#IsAreaMapped').val() == "true") {
        var currentStaus = $('#CurrentStatus').val();
        var selectedStatus = $('#ddStatus option:selected').val();
        var propertyId = $('#PropertyId').val();
   
        if (currentStaus != selectedStatus) {
            var postData = {
                status: selectedStatus,
                propertyId: propertyId       
            };

            var btnId = $(this).attr('id');
            $('#' + btnId).html('Loading <i class="fa fa-refresh fa-spin"></i>').attr("disabled", true);
            $.ajax({
                url: "/property/UpdateStatus",
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(postData),
                success: function (data, status) {
                    if (data.Success == true) {
                        showError("Status Updated Sucessfully.");
                    }
                    else
                        showError(data.Error);
                },
                complete: function () {
                    $('#' + btnId).html('Update Status').removeAttr("disabled");
                    loader.hide();
                }
            });
        }
   // }
   // else
     //   return showError('Area is not mapped to property.');
});

$('.propertyStatus').on("click", "#btnMapCategory", function (e) {            
    var propertyId = $('#PropertyId').val();
    var lat = $('#hdnLat').val();
    var lng = $('#hdnLng').val();
    
        var postData = {            
            propertyId: propertyId,
            lat: lat,
            lng: lng
        };

        var btnId = $(this).attr('id');
        $('#' + btnId).html('Loading <i class="fa fa-refresh fa-spin"></i>').attr("disabled", true);
        $.ajax({
            url: "/property/GenerateCategory",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            success: function (data, status) {
                if (data.Success == true) {
                    showError("Category Map Sucessfully.");
                }
                else
                    showError(data.Error);
            },
            complete: function () {
                $('#' + btnId).html('Map Category').removeAttr("disabled");
                loader.hide();
            }
        });
});

$('#btnMapArea').click(function () {
    if ($('.area option:selected').val() == "")
        return showError("Please select area.");

    $.post("/property/updatearea", { propertyId: $('#PropertyId').val(), areaId: $('.area option:selected').val() }, function (data) {
        if (data.Success)
        { $('#IsAreaMapped').val(true); showSuccessMessage('Area mapped successfully.'); $('#mapToArea').modal('hide'); }
        else
            showError(data.ErrorMessage);
    });
});

function removeGalleryPic(imgId) {
    $.ajax({
        url: "/property/DeleteGallery",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#PropertyId').val(), image: imgId }),
        success: function (data, status) {
            if (data.Success == true)
                $('#img' + imgId).remove();
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        }
    });
}

$('.post-property-iamges').on("click", ".remove-picture", function (e) {
    var parentLi = $(this).parent();
    var filePath = $(this).data('file');
    if (filePath !== "" && filePath !== undefined) {
        $.grep(propertyPost.GalleryDetails, function (element, index) {
            if (element.FileName === filePath) {
                $.ajax({
                    url: "/property/RemoveGalleryFile",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ imagePath: filePath }),
                    success: function (data, status) {
                        if (data.Success == true) {
                            parentLi.remove();
                            propertyPost.GalleryDetails.splice(index, 1);
                        }
                        else if (data.Success == false) {
                            showError(data.ErrorMessage);
                        }
                    }
                });
            }
        });
    }
});


