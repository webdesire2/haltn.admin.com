﻿using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Foolproof;

namespace FH.Admin_Web.ViewModel
{
    public class AddPropertySEOModel
    {
        [Range(1,int.MaxValue)]
        public int PropertyId { get; set; }
        
        [Required,MaxLength(100)]
        public string MetaTitle { get; set; }

        [Required,MaxLength(200)]
        public string MetaKeyword { get; set; }

        [Required,MaxLength(500)]
        public string MetaDesc { get; set; }

        [AllowHtml]
        public string JsonDesc { get; set; }        
    }

    public class AddSEOCategory
    {             
        public string Locality { get; set; }
        public string Lat { get; set; }        
        public string Lng { get; set; }        
        public string PropertyType { get; set; }
        public string Prefix { get; set; }
        public string City { get; set; }       
        public int Radius { get; set; }
    }

    public class EditSEOCategory
    {
        [Range(1,int.MaxValue)]
        public int Id { get; set; }

        [Required]
        public string Category { get; set; }
        public string Url { get; set; }

        [Required]
        public string MetaTitle { get; set; }

        [Required]
        public string MetaKeyword { get; set; }

        [Required]
        public string MetaDesc { get; set; }

        [AllowHtml]
        public string MetaJson { get; set; }        

        public string Heading { get; set; }
        public string HeadingContent { get; set; }

        public string Result { get; set; }
        public bool IsPostBack { get; set; }
    }

    public class ReplaceCategoryText
    {
        public int Id { get; set; }
        public string OldText { get; set; }
        public string NewText { get; set; }
    }
}
