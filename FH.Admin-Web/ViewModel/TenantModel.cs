﻿using FH.Admin.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FH.Admin_Web.ViewModel
{
    public class AddTenantModel:IValidatableObject
    {  
        [Range(1,int.MaxValue,ErrorMessage ="Property id required.")]
        public int PropertyId { get; set; }
        
        [Required(ErrorMessage ="Name required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Room No. required.")]
        public int RoomId { get; set; }

        [Required(ErrorMessage = "Occupancy required.")]
        public float Occupancy { get; set; }

        [Required(ErrorMessage ="Mobile no required.")]
        [MinLength(10,ErrorMessage ="10 digit mobile no required.")]
        [MaxLength(10, ErrorMessage = "10 digit mobile no required.")]
        public string Mobile { get; set; }

        [EmailAddress(ErrorMessage ="Invalid email id.")]
        public string Email { get; set; }
        
        [Range(1,double.MaxValue,ErrorMessage ="Security amount required.")]
        public double SecurityAmount { get; set; }

        [Range(1, double.MaxValue, ErrorMessage = "Montly rent amount required.")]
        public double MonthlyRent { get; set; }

        public int RoomNo { get; set; }

        public double SecurityDeposit { get; set; }

        [Display(Name ="Check-In date")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public DateTime CheckInDate { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (SecurityDeposit > SecurityAmount)
                yield return new ValidationResult("Security deposit cann't be more than security amount.");
        }

        public IEnumerable<PGRoomData> PGRoomData { get; set; }
    } 
    
    public class EditTenantModel
    {
        [Range(1, int.MaxValue, ErrorMessage = "Property id required.")]
        public int TenantId { get; set; }

        [Required(ErrorMessage = "Name required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Room No. required.")]
        public int RoomId { get; set; }

        [Required(ErrorMessage = "Occupancy required.")]
        public float Occupancy { get; set; }

        [Required(ErrorMessage = "Mobile no required.")]
        [MinLength(10, ErrorMessage = "10 digit mobile no required.")]
        [MaxLength(10, ErrorMessage = "10 digit mobile no required.")]
        public string Mobile { get; set; }

      
        [EmailAddress(ErrorMessage = "Invalid email id.")]
        public string Email { get; set; }

        //[Range(1, double.MaxValue, ErrorMessage = "Security amount required.")]
        public double SecurityAmount { get; set; } 

        [Range(1, double.MaxValue, ErrorMessage = "Montly rent amount required.")]
        public double MonthlyRent { get; set; }

        public double SecurityDeposit { get; set; }

        public int RoomNo { get; set; }

        [Display(Name = "Check-In date")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public DateTime CheckInDate { get; set; }

        [Display(Name = "Check-Out date")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public DateTime? CheckOutDate { get; set; }

        public bool IsActive { get; set; }

        public IEnumerable<PGRoomData> PGRoomData { get; set; }
        public PGRoomData TenantRoom { get; set; }
    }
     
    public class CollectCashData
    {
        public int TenantId { get; set; }
        public double rentAmount { get; set; }
        public DateTime? rentForMonthStartDate { get; set; }
        public DateTime? rentForMonthEndDate { get; set; }
        public string Remark { get; set; }
    }


    public class GetAllTenantVM
    {
        public int Pid { get; set; }
        public int Tid { get; set; }
        public int RoomNo { get; set; }
        public string Mobile { get; set; }

        public int IsActive1 { get; set; }
        public IEnumerable<TenantList> TenantList { get; set; }

        public Pager Pager { get; set; }

        public int Page { get; set; } = 1;       
    }


    public class GetNOTTenantVM
    {
        public int Pid { get; set; }
        public int Tid { get; set; }
        public int RoomNo { get; set; }
        public string Mobile { get; set; }

        public bool IsActive { get; set; }
        public IEnumerable<NotPaidT> TenantList { get; set; }

        public Pager Pager { get; set; }

        public int Page { get; set; } = 1;
    }
    public class PropertyMapVM
    {
        public int Pid { get; set; }
        public int Tid { get; set; }
        public int RoomNo { get; set; }
        public string Mobile { get; set; }
        public DateTime CheckInDate { get; set; }

        public IEnumerable<TenantList> TenantList { get; set; }
        public IEnumerable<PGRoomData> PGRoomData { get; set; }

        public Pager Pager { get; set; }

        public int Page { get; set; } = 1;
    }
}