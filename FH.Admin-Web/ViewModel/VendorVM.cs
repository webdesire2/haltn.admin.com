﻿using System.ComponentModel.DataAnnotations;

namespace FH.Admin_Web.ViewModel
{
    public class AddOwnerModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage ="Username required.")]
        [MaxLength(50,ErrorMessage = "Username can't be more than 50 charcaters.")]
        public string Name { get; set; }

        
        [Required(ErrorMessage ="Email id required.")]
        [EmailAddress(ErrorMessage ="Invalid email id.")]        
        public string Email { get; set; }

        [Required(ErrorMessage ="Mobile no required.")]   
        [MinLength(10,ErrorMessage ="Invalid mobile no")]
        [MaxLength(10,ErrorMessage ="Invalid mobile no")]
        public string Mobile { get; set; }

        public bool IsActive { get; set; }
    }
}