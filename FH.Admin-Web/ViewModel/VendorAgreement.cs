﻿
namespace FH.Admin_Web.ViewModel
{
    public class EmailSentStatus
    {
        public string Email { get; set; }
        public string Status { get; set; }
    }
}