﻿using FH.Admin.Entities;
using System.Collections.Generic;

namespace FH.Admin_Web
{
    public class PropertyAssignVM
    {
        public string CityName { get; set; }
        public CityZoneAreaMapping ZoneArea { get; set; }
             
    }
}