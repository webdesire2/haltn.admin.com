﻿using FH.Admin.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FH.Admin_Web
{
    public class VMPropertyListing
    {
        public IEnumerable<PropertyMaster> propertyMaster { get; set; }
        public Pager pager { get; set; }
    }

    public class VMPaymentDetails 
    {
        public TanentStayDetails TanentStayDetails { get; set; }
        public IEnumerable<TenantPaymentHistory> TenantPaymentHistory { get; set; }
    }



    public class AddOffer
    {
        [Range(1,int.MaxValue)]
        public int PropertyId { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="Title required.")]
        [MaxLength(50,ErrorMessage ="Max length 50 allowed.")]
        public string Title { get; set; }

        [Range(1,100,ErrorMessage ="Offer % required.")]
        public int OfferPercent { get; set; }
    }    
}