﻿using FH.Admin.Entities;
using System.Collections.Generic;
namespace FH.Admin_Web
{
    public class PropertyLeadVM
    {
        public IEnumerable<PropertyLeadList> Lead { get; set; }
        public IEnumerable<CityMaster> City { get; set; }        
        public IEnumerable<AreaMaster> Area { get; set; }
    }
}