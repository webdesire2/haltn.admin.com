﻿using FH.Admin.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FH.Admin_Web.ViewModel
{

    public class GetAllPlantVM
    {
        public IEnumerable<PlanMaster> Plans { get; set; }
    }

    public class GetOwnerDetailVM
    {
        public IEnumerable<PropertyMaster> PropertyList { get; set; }
        public IEnumerable<PlanMaster> Plans { get; set; }
    }


    public class GetAllPlanSellVM
    {
        public int PropertyId { get; set; }
        public int CreatedBy { get; set; }
        public IEnumerable<PlanSellMaster> MyPlans { get; set; }
        public IEnumerable<PlanSellMaster> MyOrders { get; set; }
    }

    public class GenerateOrder
    {
        public int PlanId { get; set; }
        public float Price { get; set; }
        public int OwnerId { get; set; }
        public int VarifiedTag { get; set; }
        public Array PropertyId { get; set; }
        public int PlanOrderID { get; set; }
    }

    public class EditPlanModel
    {
        public int PlanId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public int Validity { get; set; }
        public int NumberOfLeads { get; set; }
        public int Status { get; set; }
        public int VarifiedTag { get; set; }
        public string Color { get; set; }
        public string SEOData { get; set; }
        public int CreatedBy { get; set; }
        public string Category { get; set; }
    }

    public class GetownerDetails
    {
        public int PlanId { get; set; }
        public string Mobile { get; set; }
    }


} 