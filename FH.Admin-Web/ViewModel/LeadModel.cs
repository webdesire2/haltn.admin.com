﻿using FH.Admin.Entities;
using System.Collections.Generic;

namespace FH.Admin_Web.ViewModel
{
    public class PropertySearchForLead
    { 
        public string PId { get; set; }
        public string PType { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }        
        public string Gender { get; set; }
        public string RoomType { get; set; }
        public string Tenant { get; set; }
        public string Bhk { get; set; }
        public string Apartment { get; set; }
    }

    public class AddOfflineLead
    {
        public string ClientName { get; set; }
        public string ClientMobile { get; set; }
        public int[] PId { get; set; }
    }

    public class GetAllLeadModel
    {
        public int LeadId { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string gender { get; set; }
        public string PropertyType { get; set; }
        public string PropertyId { get; set; }
        public string LeadSource { get; set; }
    }

    public class WebsiteLeadVM
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public IEnumerable<WebsiteLead> Leads { get; set; }
    }
}