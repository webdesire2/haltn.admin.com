﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FH.Web.Security
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            string redirectUrl = filterContext.HttpContext.Request.Path;
            string nonAuthorizeAction ="login";
            var rd = filterContext.RequestContext.RouteData;
            if (rd.GetRequiredString("action").ToLower() == nonAuthorizeAction)
                return;

            Roles = Convert.ToInt32(UserRoles.Agent).ToString();

            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                if (!String.IsNullOrEmpty(Roles))
                {
                    if (!CurrentUser.IsInRole(Roles))
                    {
                        filterContext.Result = new ViewResult()
                        {
                            ViewName = "AccessDenied"
                        };
                    }
                }

                if (!String.IsNullOrEmpty(Users))
                {
                    if (!Users.Contains(CurrentUser.UserId.ToString()))
                    {                        
                        filterContext.Result = new RedirectToRouteResult(new
                        RouteValueDictionary(new { controller = "user", action = "Login",redirectUrl=redirectUrl }));

                        // base.OnAuthorization(filterContext); //returns to login url
                    }
                }
            }
            else
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                  filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new
                          RouteValueDictionary(new { controller = "user", action = "Login",redirectUrl=redirectUrl}));
                }
            }

        }
    }
}