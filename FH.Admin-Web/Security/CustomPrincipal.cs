﻿using System.Linq;
using System.Security.Principal;

namespace FH.Web.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string[] Roles { get; set; }

        public IIdentity Identity { get; private set; }

        public CustomPrincipal(string userName)
        {
            this.Identity = new GenericIdentity(userName);
        }

        public bool IsInRole(string role)
        {
            if (Roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum UserRoles
    {
        Agent = 1
    }

}