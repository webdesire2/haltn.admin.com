﻿using System.Configuration;

namespace FH.Admin.Data
{
    internal class ConnectionFactory
    {
        public readonly static string getConString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
    }
}
