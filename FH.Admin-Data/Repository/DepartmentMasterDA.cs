﻿using Dapper;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;
using System;

namespace FH.Admin.Data
{
    public class DepartmentMasterDA :IDepartmentMasterDA
    {
        IDbTransaction _transaction;
        public DepartmentMasterDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<DepartmentMaster> GetAllDepartment()
        {
            return SqlMapper.Query<DepartmentMaster>(_transaction.Connection, "SELECT * FROM Department_Master", null, _transaction,
                   commandType: CommandType.Text);
        }      
    }

    public interface IDepartmentMasterDA
    {
        IEnumerable<DepartmentMaster> GetAllDepartment();
    }
}
