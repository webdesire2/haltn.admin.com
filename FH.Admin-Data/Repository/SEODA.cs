﻿using FH.Admin.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System;

namespace FH.Admin.Data
{
    public class SEODA
    {
        string _connectionString = string.Empty;

        public SEODA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public SEOCategoryVM GetAllCategory(SEODE de)
        {
            SEOCategoryVM vm = new SEOCategoryVM();

            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    using (var multi = con.QueryMultiple("udsp_seo", SEOParam(de, SEOCallValue.GetAllCategoryWithPaging), null, commandType: CommandType.StoredProcedure))
                    {
                        vm.SEOCategories = multi.Read<SEOCategory>();
                        var record = multi.ReadFirstOrDefault();
                        vm.PageNo = record.PageNo;
                        vm.PageSize = record.PageSize;
                        vm.TotalRecord = record.TotalRecord;
                    }                    
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }

            return vm;
        }

        public IEnumerable<T> Execute<T>(SEODE de, SEOCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_seo",SEOParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }
        
        public IEnumerable<T> ExecuteQuery<T>(string query)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>(query,commandType: CommandType.Text);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }

        }


        private DynamicParameters SEOParam(SEODE de, SEOCallValue callValue)
        {
            if (de == null)
                de = new SEODE();

            var param = new DynamicParameters();
            param.Add("@CallValue", (int)callValue);
            param.Add("@Id", de.Id);
            param.Add("@Category", de.Category==null?"": de.Category);
            param.Add("@Url", de.Url == null ? "" : de.Url);
            param.Add("@MetaTitle", de.MetaTitle == null ? "" : de.MetaTitle);
            param.Add("@MetaDesc", de.MetaDesc == null ? "" : de.MetaDesc);
            param.Add("@MetaKeyword", de.MetaKeyword == null ? "" : de.MetaKeyword);
            param.Add("@MetaJson", de.MetaJson == null ? "" : de.MetaJson);
            param.Add("@CreatedBy", de.UserId);
            param.Add("@PageNo", de.PageNo);
            param.Add("@PageSize", de.PageSize);
            param.Add("@PropertyId", de.PropertyId);
            param.Add("@CategoryId", de.CategoryId);
            param.Add("@City", de.City==null?"":de.City);
            param.Add("@Lat", de.Lat==null?"":de.Lat);
            param.Add("@Lng", de.Lng==null?"":de.Lng);
            param.Add("@Heading", de.Heading==null?"":de.Heading);
            param.Add("@HeadingContent", de.HeadingContent==null?"":de.HeadingContent);
            param.Add("@PropertyType", de.PropertyType==null?"":de.PropertyType);
            param.Add("@Prefix", de.Prefix==null?"":de.Prefix);
            param.Add("@SearchKey", de.SearchKey);
            return param;
        }

        public void MapCategoryToProperty(int propertyId)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@PropertyId", propertyId);

                    var result = con.Execute("udsp_map_category_to_property",param, null, commandType: CommandType.StoredProcedure);                   
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public SEOCategory GetCategory(int id)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@Id", id);
                    string query = "" +
                        "SELECT * FROM SEO_Category WHERE Id=@Id";
                    return con.QueryFirstOrDefault<SEOCategory>(query, param, null, commandType: CommandType.Text);
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public SEOLocalityVM GetAllLocality(SEODE de)
        {
            SEOLocalityVM vm = new SEOLocalityVM();

            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    using (var multi = con.QueryMultiple("udsp_seo", SEOParam(de, SEOCallValue.GetAllLocalityBySearch), null, commandType: CommandType.StoredProcedure))
                    {
                        vm.ListLocality = multi.Read<SEOLocalityMaster>();
                        var record = multi.ReadFirstOrDefault();
                        vm.PageNo = record.PageNo;
                        vm.PageSize = record.PageSize;
                        vm.TotalRecord = record.TotalRecord;
                    }
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }

            return vm;
        }
        
        public IEnumerable<SEOLocalityMaster> GetLocalityByLatLng(string lat,string lng,int radius)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var param = new DynamicParameters();
                    param.Add("@Lat", lat);
                    param.Add("@Lng", lng);
                    param.Add("@Radius", radius);

                    return con.Query<SEOLocalityMaster>("udsp_Search_Locality_By_LatLng",param, null, commandType: CommandType.StoredProcedure);                    
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }
    }

    public enum SEOCallValue
    {
        AddCategory = 1,        
        GetAllCategoryWithPaging=2,
        AddPropertyCategory=3,
        RemocePropertyCategory=4,
        GetPropertyCategory=5,
        GetCategryByAutoSearch=6,
        AddPropertySEO=7,
        GetCategoryById=8,
        UpdateCategory=9,
        DeleteCategory=10,
        AddLocality=12,
        GetAllLocalityBySearch=13
       
    }
}
