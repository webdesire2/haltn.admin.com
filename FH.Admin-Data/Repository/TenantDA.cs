﻿using System.Data;
using FH.Admin.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;

namespace FH.Admin.Data
{
    public class TenantDA
    {
        string _connectionString = string.Empty;
        public TenantDA()
        {
            _connectionString =ConnectionFactory.getConString;
        }

        public TenantListVM GetAllTenant(TenantDE de,TenantCallValue callValue)
        {
            TenantListVM vm = new TenantListVM();

            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    using (var multi = con.QueryMultiple("udsp_tenent_mgmt", TenantParam(de, callValue), null, commandType: CommandType.StoredProcedure))
                    {
                        vm.TenantList = multi.Read<TenantList>();

                        var record = multi.ReadFirstOrDefault();
                        vm.PageNo=record.PageNo;
                        vm.PageSize = record.PageSize;
                        vm.TotalRecord = record.TotalRecord;
                    }                        
                }
                catch(Exception ex)
                {
                    throw new ApplicationException(ex.Message);
                }
                finally
                {
                    con.Dispose();
                }
            }

            return vm;
        }

        public IEnumerable<T> Execute<T>(TenantDE de, TenantCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_tenent_mgmt", TenantParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                { 
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public T ExecuteWithTransaction<T>(TenantDE de, TenantCallValue param)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                IDbTransaction tran = con.BeginTransaction();
                try
                {
                    var result = con.QueryFirstOrDefault<T>("udsp_tenent_mgmt", TenantParam(de, param), tran, commandType: CommandType.StoredProcedure);
                    tran.Commit();
                    return result;
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    throw new ApplicationException();
                }
                finally
                {
                    tran.Dispose();
                    con.Dispose();
                }
            }
        }

        private DynamicParameters TenantParam(TenantDE de, TenantCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@TenantId", de.TenantId);
            param.Add("@PropertyId", de.PropertyId);
            param.Add("@Name", de.Name);
            param.Add("@Mobile", de.Mobile);
            param.Add("@Email", de.Email);
            param.Add("@SecurityAmount", de.SecurityAmount);
            param.Add("@StatusMessage", de.StatusMessage);
            param.Add("@MonthlyRent", de.MonthlyRent);
            param.Add("@SecurityDeposit", de.SecurityDeposit);
            param.Add("@CheckInDate", de.CheckInDate);
            param.Add("@CheckOutDate", de.CheckOutDate);
            param.Add("@CreatedBy", de.UserId);
            param.Add("@AdminId", de.AdminId);
            param.Add("@IsActive", de.IsActive);
            param.Add("@IsActive1", de.IsActive1);
            param.Add("@PageNo", de.PageNo);
            param.Add("@RoomNo", de.RoomNo);
            param.Add("@RoomId", de.RoomId);
            param.Add("@Occupancy", de.Occupancy);
            param.Add("@QueryType", (int)callValue);
            param.Add("@Payment", de.Payment);
            param.Add("@RentForMonthStartDate", de.RentForMonthStartDate);
            param.Add("@RentForMonthEndDate", de.RentForMonthEndDate);
            param.Add("@PaymentType", de.PaymentType);
            return param;
        }
    }

    public enum TenantCallValue
    {
        AddTenantByAdmin = 10,
        gtnotpaid = 40,
        GetAllTenantByAdmin =11,        
        GetTenantByAdmin=12,
        UpdateTenantByAdmin=13,
        GetTenantPaymentByAdmin=14,
        GetTanentStayDetails = 15,
        CollectPayment = 16,
        CollectSecurity = 18,
        RefundSecurity = 22,
        getAvailableRooms = 28, 
        getTenantRooms = 29,

    }
}
 