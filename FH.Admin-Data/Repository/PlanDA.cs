﻿using System.Data;
using FH.Admin.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;

namespace FH.Admin.Data
{
    public class PlanDA
    {
        string _connectionString = string.Empty;
        public PlanDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }




        public IEnumerable<T> Execute<T>(PlanMaster de, PlanCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_plan_mgmt", TenantParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch(Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public T ExecuteWithTransaction<T>(PlanMaster de, PlanCallValue param)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                IDbTransaction tran = con.BeginTransaction();
                try
                {
                    var result = con.QueryFirstOrDefault<T>("udsp_plan_mgmt", TenantParam(de, param), tran, commandType: CommandType.StoredProcedure);
                    tran.Commit();
                    return result;
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    throw new ApplicationException();
                }
                finally
                {
                    tran.Dispose();
                    con.Dispose();
                }
            }
        }

        private DynamicParameters TenantParam(PlanMaster de, PlanCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@PlanId", de.PlanId);
            param.Add("@Name", de.Name);
            param.Add("@Description", de.Description);
            param.Add("@Price", de.Price);
            param.Add("@Discount", de.Discount);
            param.Add("@Validity", de.Validity);
            param.Add("@NumberOfLeads", de.NumberOfLeads);
            param.Add("@Status", de.Status);
            param.Add("@VarifiedTag", de.VarifiedTag);
            param.Add("@Color", de.Color);
            param.Add("@SEOData", de.SEOData);
            param.Add("@PropertyId", de.PropertyId);
            param.Add("@CreatedBy", de.CreatedBy);
            param.Add("@Mobile", de.Mobile); 
            param.Add("@Payment", de.Payment);
            param.Add("@Category", de.Category);
            param.Add("@PlanOrderID", de.PlanOrderID);
            param.Add("@OrderGenratedByAdmin", de.OrderGenratedByAdmin);
            param.Add("@QueryType", (int)callValue);
            return param;
        }
    }

    public enum PlanCallValue
    {
        AddPlanByAdmin = 1,
        GetAllPlanByAdmin = 2,
        GetPlanByAdmin= 3,
        UpdatePlanByAdmin=4,
        GetMyOder = 11,
        GetMyPlans = 6,
        GetOwnerPropertyDetails = 15,
        AddPlanOrder = 10,
        AddPlanToVendor = 9,
        DeleteOderByAdmin = 16
    }
}
