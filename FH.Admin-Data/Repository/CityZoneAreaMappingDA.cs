﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using FH.Admin.Entities;
using System;

namespace FH.Admin.Data
{
    public class CityZoneAreaMappingDA:ICityZoneAreaMappingDA
    {
        IDbTransaction _transaction;
        public CityZoneAreaMappingDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public CityZoneAreaMapping GetDetails()
        {
            CityZoneAreaMapping objMapping = new CityZoneAreaMapping();
           
            try
            {
                var param = new DynamicParameters();
                param.Add("@QueryType", 1);

                using (var multiRecord = SqlMapper.QueryMultiple(_transaction.Connection, "usp_City_Zone_Area_Mapping", param, _transaction, commandType: CommandType.StoredProcedure))
                {
                    objMapping.CityMaster= multiRecord.Read<CityMaster>();
                    objMapping.ZoneMaster = multiRecord.Read<ZoneMaster>();
                }

                return objMapping;
            }
            catch
            {
               return objMapping;
            }
        }

        public string AddNewZone(ZoneMaster entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@CityId", entity.CityId);
                param.Add("@ZoneName", entity.ZoneName);
                param.Add("@QueryType", 2);
                return SqlMapper.QuerySingleOrDefault<string>(_transaction.Connection, "usp_City_Zone_Area_Mapping",param,_transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                return "";
            }
        }

        public string AddNewArea(AreaMaster entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@CityId", entity.CityId);
                param.Add("@ZoneId", entity.ZoneId);
                param.Add("@AreaName", entity.AreaName);
                param.Add("@QueryType",3);
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_City_Zone_Area_Mapping", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                return "";
            }
        }

        public IEnumerable<ZoneMaster> GetZone(int cityId)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@CityId",cityId);                
                param.Add("@QueryType",4);
                return SqlMapper.Query<ZoneMaster>(_transaction.Connection, "usp_City_Zone_Area_Mapping", param, _transaction, 
                    commandType: CommandType.StoredProcedure);
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AreaMaster> GetArea()
        {   
            try
            {
                var param = new DynamicParameters();             
                param.Add("@QueryType",5);
                return SqlMapper.Query<AreaMaster, ZoneMaster,CityMaster, AreaMaster>(_transaction.Connection,"usp_City_Zone_Area_Mapping",
                    (areaMaster, zoneMaster,cityMaster) =>
                {
                    areaMaster.Zone = zoneMaster;
                    areaMaster.City = cityMaster;
                    return areaMaster;
                }, param, _transaction, splitOn: "ZoneId,CityId", commandType: CommandType.StoredProcedure);
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public IEnumerable<AreaMaster> GetArea(int cityId)
        {
            var param = new DynamicParameters();
            param.Add("@CityId", cityId);            
            return SqlMapper.Query<AreaMaster>(_transaction.Connection, "SELECT * FROM AREA_MASTER WITH(NOLOCK) WHERE CityId=@CityId", param, _transaction,
            commandType: CommandType.Text);
        }
        public CityZoneAreaMapping GetAllAreaZone(int cityId=0,string cityCode="")
        {
            CityZoneAreaMapping objMap = new CityZoneAreaMapping();
            try
            {
                var param = new DynamicParameters();                
                param.Add("@QueryType",6);
                if (cityId>0)                
                    param.Add("@CityId", cityId);
                if(!string.IsNullOrEmpty(cityCode))
                    param.Add("@CityCode", cityCode);

                using (var multi = SqlMapper.QueryMultiple(_transaction.Connection, "usp_City_Zone_Area_Mapping", param, _transaction, null, CommandType.StoredProcedure))
                {
                    objMap.ZoneMaster= multi.Read<ZoneMaster>();
                    objMap.AreaMaster=multi.Read<AreaMaster>();
                }                
            }
            catch
            {
                return objMap;
            }

            return objMap;
        }


    }

    public interface ICityZoneAreaMappingDA
    {
        CityZoneAreaMapping GetDetails();
        string AddNewZone(ZoneMaster entity);
        string AddNewArea(AreaMaster entity);      
        IEnumerable<ZoneMaster> GetZone(int cityId);
        IEnumerable<AreaMaster> GetArea();
        IEnumerable<AreaMaster> GetArea(int cityId);
        CityZoneAreaMapping GetAllAreaZone(int cityId=0,string cityCode="");
    }
}
