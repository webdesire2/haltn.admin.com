﻿using Dapper;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;

namespace FH.Admin.Data
{
    public class CityMasterDA:ICityMasterDA
    {
        IDbTransaction _transaction;
        public CityMasterDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<CityMaster> GetAllCity()
        {
            return SqlMapper.Query<CityMaster>(_transaction.Connection, "SELECT * FROM CITY_MASTER",null, _transaction,
                   commandType: CommandType.Text);
        }
    }

    public interface ICityMasterDA
    {
        IEnumerable<CityMaster> GetAllCity();
    }
}
