﻿using System.Collections.Generic;
using System.Data;
using Dapper;

namespace FH.Admin.Data
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        IDbTransaction _transaction;

        public GenericRepository(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public int Add(string spName, object parameter)
        {
           return SqlMapper.Execute(_transaction.Connection, spName, parameter, _transaction, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void Delete(string spName, object parameter)
        {
           SqlMapper.Execute(_transaction.Connection, spName, parameter, _transaction, commandType: System.Data.CommandType.StoredProcedure);
        }

        public TEntity Get(string spName, object parameter)
        {
            return SqlMapper.QueryFirstOrDefault<TEntity>(_transaction.Connection, spName, parameter, _transaction, commandType: System.Data.CommandType.StoredProcedure);
        }

        public IEnumerable<TEntity> GetAll(string spName, object parameter = null)
        {
            return SqlMapper.Query<TEntity>(_transaction.Connection, spName, parameter, commandType: System.Data.CommandType.StoredProcedure);
        }

        public void Update(string spName, object parameter)
        {
            SqlMapper.Execute(_transaction.Connection, spName, parameter, _transaction, commandType: System.Data.CommandType.StoredProcedure);
        }

        public IEnumerable<dynamic> QueryMultiple(string query, CommandType commandType, object param=null)
        {
            dynamic lst = null;
            try
            {
                using (var multi = SqlMapper.QueryMultiple(_transaction.Connection, query, param, _transaction, commandType: commandType))
                {
                    lst = new List<dynamic>();
                    while (!multi.IsConsumed)
                    {
                        lst.Add(multi.Read());
                    }
                }
            }
            catch
            {
                return lst;
            }

            return lst;
        }

    }

    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity Get(string spName, object parameter);
        IEnumerable<TEntity> GetAll(string spName, object parameter = null);
        int Add(string spName, object parameter);
        void Delete(string spName, object parameter);
        void Update(string spName, object parameter);
        IEnumerable<dynamic> QueryMultiple(string query, CommandType commandType, object param = null);
    }
}
