﻿using System.Data;
using FH.Admin.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;

namespace FH.Admin.Data.Repository
{
   public class LeadDA
    {
        string _connectionString = string.Empty;

        public LeadDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(LeadDE de, LeadCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_lead", Param(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch(Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        private DynamicParameters Param(LeadDE de, LeadCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@StartDate", de.StartDate);
            param.Add("@EndDate", de.EndDate);
            param.Add("@PropertyType", de.PropertyType);
            param.Add("@PropertyIds", de.PropertyIds);
            param.Add("@Latitude",string.IsNullOrEmpty(de.Latitude)?"":de.Latitude);
            param.Add("@Longitude",string.IsNullOrEmpty(de.Longitude)?"":de.Longitude);
            param.Add("@QueryType", (int)callValue);
            param.Add("@Name", de.Name);
            param.Add("@Mobile", de.Mobile);
            param.Add("@LeadId", de.LeadId);
            param.Add("@Gender", de.Gender);
            param.Add("@RoomType",de.RoomType);
            param.Add("@Tenant", de.Tenant);
            param.Add("@Bhk", de.Bhk);
            param.Add("@Apartment", de.Apartment);
            param.Add("@LeadSource", de.LeadSource);
            return param;
        }        
    }

    public enum LeadCallValue
    {
        GetSiteVisit = 1,
        GetActivePropertyToAddLead=2,
        AddOfflineLead=3,
        GetPGLeadByFilter=4,
        GetWebsiteLeads = 5
    }
}
