﻿using Dapper;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;

namespace FH.Admin.Data
{
    public class TeamMasterDA : ITeamMasterDA
    {
        IDbTransaction _transaction;
        public TeamMasterDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<TeamMaster> GetAllTeam()
        {
            return SqlMapper.Query<TeamMaster>(_transaction.Connection, "SELECT * FROM TEAM_MASTER", null, _transaction,
                   commandType: CommandType.Text);
        }
    }

    public interface ITeamMasterDA
    {
        IEnumerable<TeamMaster> GetAllTeam();
    }
}
