﻿using Dapper;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;

namespace FH.Admin.Data
{
    public class AmenityMasterDA : IAmenityMasterDA
    {
        IDbTransaction _transaction;
        public AmenityMasterDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<AmenityMaster> GetAll()
        {
            return SqlMapper.Query<AmenityMaster>(_transaction.Connection, "SELECT * FROM Amenity_MASTER", null, _transaction,
                   commandType: CommandType.Text);
        }
    }

    public interface IAmenityMasterDA
    {
        IEnumerable<AmenityMaster> GetAll();
    }
}
