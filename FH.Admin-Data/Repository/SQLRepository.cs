﻿using Dapper;
using FH.Admin.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FH.Admin.Data.Repository
{
    public class SQLRepository
    {
        string _connectionString = string.Empty;

        public SQLRepository()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public T ExecuteFirst<T>(string sql, object param = null, CmdType? cmdType = null)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    CommandType commandType = CmdType.SP == cmdType ? CommandType.StoredProcedure : CommandType.Text;
                    var result = con.Query<T>(sql, param, commandType: commandType).FirstOrDefault();
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public IEnumerable<T> Execute<T>(string sql, object param = null, CmdType? cmdType = null)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    CommandType commandType = CmdType.SP == cmdType ? CommandType.StoredProcedure : CommandType.Text;
                    var result = con.Query<T>(sql, param, commandType: commandType);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public List<List<dynamic>> ExecuteMulti(string query, object param = null, CmdType? cmdType = null)
        {
            List<List<dynamic>> objList = new List<List<dynamic>>();

            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    CommandType commandType = CmdType.SP == cmdType ? CommandType.StoredProcedure : CommandType.Text;
                    using (var multi = con.QueryMultiple(query, param, commandType: commandType))
                    {
                        while (!multi.IsConsumed)
                        {
                            var lst = multi.Read();
                            objList.Add(lst != null ? lst.AsList() : null);
                        }
                    }

                    return objList;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
                finally
                {
                    con.Dispose();
                }
            }
        }


    }

    public enum CmdType
    {
        SP,
        TEXT
    }

}
