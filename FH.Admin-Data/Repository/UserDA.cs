﻿using Dapper;
using FH.Admin.Entities;
using System;
using System.Collections.Generic;
using System.Data;

namespace FH.Admin.Data
{
    public class UserDA:GenericRepository<UserDetails>,IUserDA
    {
        IDbTransaction _transaction;
        public UserDA(IDbTransaction transaction) : base(transaction)
        {
            _transaction = transaction;
        }
        public UserDetails GetUserDetail(string email)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@QueryType", 1);
                param.Add("@Email", email);
                return Get("Usp_AdminDetails", param);
            }
            catch(Exception e)
            {
                return null;
            }
        }
        public int AddUser(UserRegistration entity)
        {            
            var param = new DynamicParameters();
            param.Add("@Name", entity.Name);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@Email", entity.Email);
            param.Add("@DepartmentId", entity.DepartmentId);
            param.Add("@CityId", entity.CityId);
            param.Add("@TeamId", entity.TeamId);
            param.Add("@DesignationId", entity.DesignationId);
            if(entity.ZoneId>0)
            param.Add("@ZoneId",entity.ZoneId);
            param.Add("@QueryType", 2);                               
            return SqlMapper.ExecuteScalar<int>(_transaction.Connection,"Usp_AdminDetails", param,_transaction,null,CommandType.StoredProcedure);            
        }
        public IEnumerable<UserDetails> GetAreaManager(int areaId)
        {
            IEnumerable<UserDetails> lstUser = new List<UserDetails>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@QueryType",4);
                param.Add("@AreaId", areaId);
                lstUser = SqlMapper.Query<UserDetails>(_transaction.Connection, "Usp_AdminDetails", param, _transaction, commandType: CommandType.StoredProcedure);
                return lstUser;
            }
            catch
            {
                return lstUser;
            }
        }
        public UserRegistration GetAllDepartment_Designation_City()
        {
            UserRegistration user = new UserRegistration();
                
            string query = "SELECT * FROM Department_Master;SELECT * FROM Designation_Master;SELECT * FROM City_Master";
            using (var resultSet = SqlMapper.QueryMultiple(_transaction.Connection, query, null, _transaction, null, CommandType.Text))
            {
               user.Department=resultSet.Read<DepartmentMaster>();
               user.Designation = resultSet.Read<DesignationMaster>();
                user.City = resultSet.Read<CityMaster>();
            }

            return user;            
        }
        public IEnumerable<UserList> GetAllEmployee()
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 3);            
           return SqlMapper.Query<UserList>(_transaction.Connection, "Usp_AdminDetails", param, _transaction,commandType: CommandType.StoredProcedure);
        }
        public UserRegistration GetTeamAndDesignation(int departmentId)
        {
            UserRegistration user = new UserRegistration();

            string query = "SELECT * FROM Team_Master Where DepartmentId=" + departmentId;
                query +=";SELECT * FROM Designation_Master Where DepartmentId="+departmentId;
            using (var resultSet = SqlMapper.QueryMultiple(_transaction.Connection, query, null, _transaction, null, CommandType.Text))
            {
                user.Team = resultSet.Read<TeamMaster>();
                user.Designation = resultSet.Read<DesignationMaster>();                
            }

            return user;
        }        
        public IEnumerable<CityMaster> GetAllCity()
        {            
            return SqlMapper.Query<CityMaster>(_transaction.Connection, "SELECT * FROM City_Master",null, _transaction, commandType: CommandType.Text);
        }
        public bool IsUserMobileNoExist(string mobileNo)
        {
            var param = new DynamicParameters();
            param.Add("@Mobile",mobileNo);
            if (!string.IsNullOrEmpty(SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "SELECT UserId FROM User_Details WITH(NOLOCK) WHERE Mobile=@Mobile ", param, _transaction, commandType: CommandType.Text)))
              return true;

            return false;
            
        }
        public UserDetails GetUserByIdAndMobile(int userId,string mobileNo)
        {            
            try
            {
                string query = "SELECT UserId,Name,Mobile,Email from User_Details WITH(NOLOCK) Where IsActive=1 AND UPPER(UserType)='OWNER'";
                var param = new DynamicParameters();
                if (userId > 0)
                {
                    param.Add("@userId", userId);
                    query += " AND UserId=@userId";
                }
                if(!string.IsNullOrEmpty(mobileNo))
                {
                    param.Add("@mobileNo", mobileNo);
                    query += " AND Mobile=@mobileNo";
                }
                
                return SqlMapper.QueryFirstOrDefault<UserDetails>(_transaction.Connection,query, param, _transaction, commandType: CommandType.Text);                
            }
            catch
            {
                return null;
            }
        }
    }

    public interface IUserDA
    {
        UserDetails GetUserDetail(string email);
        int AddUser(UserRegistration entity);
        IEnumerable<UserDetails> GetAreaManager(int areaId);
        UserRegistration GetAllDepartment_Designation_City();
        IEnumerable<UserList> GetAllEmployee();
        UserRegistration GetTeamAndDesignation(int departmentId);
        IEnumerable<CityMaster> GetAllCity();
        bool IsUserMobileNoExist(string mobileNo);
        UserDetails GetUserByIdAndMobile(int userId, string mobileNo);
    }
}
