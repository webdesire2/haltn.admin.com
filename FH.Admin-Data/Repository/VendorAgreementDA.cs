﻿using System.Data;
using System.Linq;
using Dapper;
using FH.Admin.Entities;

namespace FH.Admin.Data.Repository
{
    public class VendorAgreementDA
    {
        IDbTransaction _transaction;

        public VendorAgreementDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public VendorEmail GetVendorEmail(VendorAgreementParam de)
        {
            de.CallValue = 3;
            var data=SqlMapper.Query<VendorEmail>(_transaction.Connection, "udsp_vendor_agreement", SPVendorAgreementParam(de), _transaction, commandType: CommandType.StoredProcedure);
            return data.FirstOrDefault();
        }

        public int UpdateAgreementStatus(VendorAgreementParam de)
        {
            de.CallValue = 1;
           int userId=SqlMapper.ExecuteScalar<int>(_transaction.Connection, "udsp_vendor_agreement",SPVendorAgreementParam(de), _transaction,commandType: CommandType.StoredProcedure);
            return userId;
        }

        private DynamicParameters SPVendorAgreementParam(VendorAgreementParam de)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", de.CallValue);
            param.Add("@Status", de.Status);
            param.Add("@EmailId", de.EmailId);
            param.Add("@PropertyId", de.PropertyId);
            return param;
        }
    }
}
