﻿using FH.Admin.Entities;
using System.Data;
using System.Collections.Generic;
using Dapper;
using System;
using System.Linq;

namespace FH.Admin.Data
{
    public class PropertyDA:IPropertyDA
    {
        private IDbTransaction _transaction;
        public PropertyDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<PropertyMaster> GetAllProperty(int pageSize,int pageIndex,string propertyType,string propertyStatus,int propertyId,int ownerId,string ownerMobile,out int totalRecord)
        {
            totalRecord = 0;    
            try
            {
                var param = new DynamicParameters();
                param.Add("@PageIndex", pageIndex);
                param.Add("@PageSize", pageSize);
                param.Add("@PropertyType", propertyType);
                param.Add("@PropertyStatus", propertyStatus);
                param.Add("@PropertyId", propertyId);
                param.Add("@OwnerId", ownerId);
                param.Add("@OwnerMobile", ownerMobile);
                param.Add("@TotalRecord", dbType: DbType.Int32, direction: ParameterDirection.Output);

               IEnumerable<PropertyMaster> lstProperty = SqlMapper.Query<PropertyMaster>(_transaction.Connection, "usp_PropertyListing", param, _transaction, commandType: CommandType.StoredProcedure);
                totalRecord = param.Get<int>("@TotalRecord");

                return lstProperty;
            }
            catch(Exception e)
            {
                return null;
            }  
        }

        public IEnumerable<PGListing> GetAllPG(int pageSize,int pageIndex,string status,out int totalRecord)
        {
            totalRecord = 0;
            try
            {
                var param = new DynamicParameters();
                param.Add("@PageIndex", pageIndex);
                param.Add("@PageSize", pageSize);                
                param.Add("@Status",status);
                param.Add("@TotalRecord", dbType: DbType.Int32, direction: ParameterDirection.Output);
                IEnumerable<PGListing> lstPG = SqlMapper.Query<PGListing>(_transaction.Connection, "usp_PGList_Admin", param,_transaction,commandType: CommandType.StoredProcedure);
                totalRecord = param.Get<int>("@TotalRecord");
                return lstPG;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        
        public Property GetProperty(int propertyId)
        {
            Property property = new Property();

            var param = new DynamicParameters();
            param.Add("@PropertyId", propertyId);            

            using (var multiRecord = SqlMapper.QueryMultiple(_transaction.Connection, "usp_GetPropertyById_admin", param, _transaction, commandType: CommandType.StoredProcedure))
            {
                string propertyType = multiRecord.ReadSingleOrDefault<string>();

                if (propertyType == "FLAT" || propertyType == "ROOM" || propertyType == "FLATMATE")
                {
                    PropertyFlatRoom propertyFlatRoom = new PropertyFlatRoom();
                    propertyFlatRoom.PropertyMaster = multiRecord.ReadSingleOrDefault<PropertyMaster>();
                    propertyFlatRoom.PropertyDetail = multiRecord.ReadSingleOrDefault<PropertyDetailFlatRoom>();
                    propertyFlatRoom.PropertyDetail.FurnishingDetails = multiRecord.Read<PropertyFurnishingDetails>();
                    propertyFlatRoom.AmenityDetails = multiRecord.Read<PropertyAmenitiesDetails>();
                    propertyFlatRoom.GalleryDetails = multiRecord.Read<PropertyGalleryDetails>();
                    property.FurnishMaster = multiRecord.Read<FurnishingMaster>();
                    property.AmenityMaster = multiRecord.Read<AmenityMaster>();
                    property.PropertyPostedBy = multiRecord.ReadSingleOrDefault<UserDetails>();
                    property.PropertyFlatRoomFlatmate = propertyFlatRoom;
                }

                if (propertyType == "PG")
                {
                    PropertyPG propertyPG = new PropertyPG();
                    propertyPG.PropertyMaster = multiRecord.ReadSingleOrDefault<PropertyMaster>();
                    propertyPG.PgDetail = multiRecord.ReadSingleOrDefault<PGDetail>();
                    propertyPG.RoomRentalDetail = multiRecord.Read<PGRoomRentalDetail>().AsList();
                    propertyPG.RoomAmenities = multiRecord.Read<PGRoomAmenity>();
                    propertyPG.pgRules = multiRecord.Read<PGRule>();
                    propertyPG.AmenityDetails = multiRecord.Read<PropertyAmenitiesDetails>();
                    propertyPG.GalleryDetails = multiRecord.Read<PropertyGalleryDetails>();
                    property.AmenityMaster = multiRecord.Read<AmenityMaster>();
                    property.PropertyPostedBy = multiRecord.ReadSingleOrDefault<UserDetails>();
                    property.PGRoomData = multiRecord.Read<PGRoomData>();

                    property.PropertyPG = propertyPG;                    
                } 

                var seo = multiRecord.ReadSingleOrDefault<PropertySEO>();
                var propertycat = multiRecord.Read<PropertySEOCategory>();

                PropertySEO pseo = new PropertySEO();
                if (seo != null)
                {
                    pseo = seo;
                    pseo.PropertySEOCategories = propertycat;
                }
                else
                {
                    pseo.PropertySEOCategories = propertycat;
                }

                property.PropertySEO = pseo;
            }

            return property;
        }
        public int AddNewPG(PropertyPG entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyType", "PG");
                param.Add("@CreatedBy", entity.PropertyMaster.CreatedBy);
                param.Add("@PostedBy", entity.PropertyMaster.PostedBy);
                param.Add("@City", entity.PropertyMaster.City);
                param.Add("@Locality", entity.PropertyMaster.Locality);
                param.Add("@Street", entity.PropertyMaster.Street);
                param.Add("@Latitude", entity.PropertyMaster.Latitude);
                param.Add("@Longitude", entity.PropertyMaster.Longitude);
                param.Add("@PropertyName", entity.PropertyMaster.PropertyName);
                param.Add("@ContactPersonName", entity.PropertyMaster.ContactPersonName);
                param.Add("@ContactPersonMobile", entity.PropertyMaster.ContactPersonMobile);

                /********PG Room Detail **********/
                if (entity.RoomRentalDetail != null)
                {
                    DataTable dtRoomRental = Convert.ToDataTable<PGRoomRentalDetail>(entity.RoomRentalDetail);
                    param.Add("@RoomRentalDetail", dtRoomRental.AsTableValuedParameter("PGRoomDetails1"));
                }

                if (entity.RoomAmenities != null)
                {
                    DataTable dtRoomAmenity = Convert.ToDataTable<PGRoomAmenity>(entity.RoomAmenities);
                    param.Add("@RoomAmenity", dtRoomAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                /*******Rental detail ********/
                param.Add("@AvailableFrom", entity.PgDetail.AvailableFrom);
                param.Add("@TenantGender", entity.PgDetail.TenantGender);
                param.Add("@PreferredGuest", entity.PgDetail.PreferredGuest);
                param.Add("@Fooding", entity.PgDetail.Fooding);
                param.Add("@Laundry", entity.PgDetail.Laundry);
                param.Add("@RoomCleaning", entity.PgDetail.RoomCleaning);                

                if (entity.pgRules != null)
                {
                    DataTable dtRules = Convert.ToDataTable<PGRule>(entity.pgRules);
                    param.Add("@PGRules", dtRules.AsTableValuedParameter("PGRules"));
                }

                if (entity.GalleryDetails != null)
                {
                    DataTable dtGallery = Convert.ToDataTable<PropertyGalleryDetails>(entity.GalleryDetails);
                    dtGallery.Columns.Remove("PropertyGalleryId");
                    param.Add("@GalleryDetails", dtGallery.AsTableValuedParameter("GalleryDetails"));
                }

                if (entity.AmenityDetails != null)
                {
                    DataTable dtAmenity = Convert.ToDataTable<PropertyAmenitiesDetails>(entity.AmenityDetails);
                    param.Add("@PropertyAmenityDetails", dtAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.ExecuteScalar<int>(_transaction.Connection, "usp_InsertPropertyPG_Admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string UpdatePropertyDetail(int propertyId,int userId, PropertyDetailFlatRoom entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@UserId",userId);                
                param.Add("@ApartmentType", entity.ApartmentType);
                param.Add("@ApartmentName", entity.ApartmentName);
                param.Add("@BhkType", entity.BhkType);
                param.Add("@RoomType", entity.RoomType);
                param.Add("@TenantGender", entity.TenantGender);
                param.Add("@PropertySize", entity.PropertySize);
                param.Add("@Facing", entity.Facing);
                param.Add("@PropertyAge", entity.PropertyAge);
                param.Add("@FloorNo", entity.FloorNo);
                param.Add("@TotalFloor", entity.TotalFloor);
                param.Add("@BathRooms", entity.BathRooms);
                param.Add("@Balconies", entity.Balconies);
                param.Add("@WaterSupply", entity.WaterSupply);
                param.Add("@GateSecurity", entity.GateSecurity);

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_Property_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);

            }
            catch
            {
                throw;
            }
        }
        public string UpdateLocalityDetail(int propertyId, int userId, PropertyMaster entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@UserId", userId);
                param.Add("@ContactPersonName", entity.ContactPersonName);
                param.Add("@ContactPersonMobile", entity.ContactPersonMobile);
                param.Add("@City", entity.City);
                param.Add("@Locality", entity.Locality);
                param.Add("@Street", entity.Street);
                param.Add("@Latitude", entity.Latitude);
                param.Add("@Longitude", entity.Longitude);
                param.Add("@LocationMapLink", entity.LocationMapLink);
                //param.Add("@PropertyName", entity.PropertyName);

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_locality_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string UpdateRentalDetail(int propertyId,string propertyType, int userId, PropertyDetailFlatRoom entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@PropertyType", propertyType);
                param.Add("@UserId", userId);
                param.Add("@OwnerExpectedRent", entity.OwnerExpectedRent);
                param.Add("@OwnerExpectedDeposit", entity.OwnerExpectedDeposit);
                param.Add("@ExpectedRent", entity.ExpectedRent);
                param.Add("@ExpectedDeposit", entity.ExpectedDeposit);
                param.Add("@AvailableFrom", entity.AvailableFrom);
                param.Add("@RentNegotiable", entity.IsRentNegotiable);
                param.Add("@PreferredTenant", entity.PreferredTenant);
                param.Add("@Parking", entity.Parking);
                param.Add("@Furnish", entity.Furnishing);
                param.Add("@Description", entity.Description);
                if (entity.FurnishingDetails != null)
                {
                    DataTable dtFurnishingDetail = Convert.ToDataTable<PropertyFurnishingDetails>(entity.FurnishingDetails);
                    param.Add("@PropertyFurnishingDetail", dtFurnishingDetail.AsTableValuedParameter("PropertyFurnishingDetails"));
                }
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_rental_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string AddNewGallery(int propertyId, int userId, string file)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@UserId",userId);
                param.Add("@FilePath", file);
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_add_gallery_admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        
        public string DeleteGallery(int propertyId, int userId, int imageId)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@UserId",userId);
                param.Add("@ImageId", imageId);
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_delete_gallery_admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string UpdatePropertyAmenity(int propertyId, int userId, IEnumerable<PropertyAmenitiesDetails> entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@UserId", userId);
                if (entity != null)
                {
                    DataTable dtAmenity = Convert.ToDataTable<PropertyAmenitiesDetails>(entity);
                    param.Add("@PropertyAmenityDetails", dtAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_amenity_admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string UpdatePGRoomDetail(int propertyId, int userId, IEnumerable<PGRoomRentalDetail> roomRentalDetail, IEnumerable<PGRoomAmenity> roomAmenities)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@UserId", userId);

                if (roomRentalDetail != null)
                {
                    DataTable dtRoomRental = Convert.ToDataTable<PGRoomRentalDetail>(roomRentalDetail);
                    param.Add("@RoomRentalDetail", dtRoomRental.AsTableValuedParameter("PGRoomDetails1"));
                }

                if (roomAmenities != null)
                {
                    DataTable dtRoomAmenity = Convert.ToDataTable<PGRoomAmenity>(roomAmenities);
                    param.Add("@RoomAmenity", dtRoomAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_pg_room_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }

        }
        public string UpdatePGDetail(int propertyId, int userId, PGDetail pgDetail, IEnumerable<PGRule> pgRules)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@UserId", userId);
                param.Add("@AvailableFrom", pgDetail.AvailableFrom);
                param.Add("@TenantGender", pgDetail.TenantGender);
                param.Add("@PreferredGuest", pgDetail.PreferredGuest);
                param.Add("@Fooding", pgDetail.Fooding);
                param.Add("@Laundry", pgDetail.Laundry);
                param.Add("@RoomCleaning", pgDetail.RoomCleaning);
                if (pgRules != null)
                {
                    DataTable dtRules = Convert.ToDataTable<PGRule>(pgRules);
                    param.Add("@PGRules", dtRules.AsTableValuedParameter("PGRules"));
                }
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_pg_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);

            }
            catch
            {
                throw;
            }
        }
        public string UpdatePropertyStatus(int propertyId,string status)
        {
            try
            {
                string query = "IF EXISTS(SELECT * FROM PROPERTY_MASTER WHERE PropertyId=@PropertyId) ";                
                query += "UPDATE Property_Master set PropertyStatusByFH=@PropertyStatusByFH WHERE PropertyId=@PropertyId ";
                query += "ELSE SELECT 'Something went wrong.' ";
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@PropertyStatusByFH", status);
               return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
            }
            catch
            {
                throw;
            }
        }
        public void UpdatePropertyArea(int propertyId, int areaId)
        {
            try
            {
                string query = "UPDATE Property_Master SET Area=@AreaId WHERE PropertyId=@PropertyId";
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AreaId",areaId);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
            }
            catch
            {
                throw;
            }
        }
        public void UpdatePropertyFeedbackStatus(int id,bool isActive)
        {
            try
            {                
               string query = "UPDATE PropertyFeedBack set IsActive=@IsActive WHERE PropertyFeedBackId=@Id ";                
                var param = new DynamicParameters();
                param.Add("@IsActive",isActive);
                param.Add("@Id",id);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
            }
            catch
            {
                throw;
            }
        }
        public IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid)
        {
            string query = "SELECT * FROM PropertyFeedBack WITH(NOLOCK) WHERE PropertyId="+pid;
            return SqlMapper.Query<PropertyFeedBack>(_transaction.Connection, query,null, _transaction, commandType: CommandType.Text);
        }

        public IEnumerable<PropertyFeedBack> GetAllPGFeedBack()
        {
            string query = "SELECT * FROM PropertyFeedBack WITH(NOLOCK) WHERE IsActive=0";
            return SqlMapper.Query<PropertyFeedBack>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
        }

        public void AddPGFeedback(PropertyFeedBack entity)
        {
            try
            {
                string query = "INSERT INTO PropertyFeedBack(PropertyId,FoodQuality,Security,Wifi,Staff,Maintenance,Description,FeedBackByName,IsActive) ";
                query += "VALUES(@PropertyId,@FoodQuality,@Security,@Wifi,@Staff,@Maintenance,@Description,@FeedBackByName,@IsActive)";
                var param = new DynamicParameters();
                param.Add("@PropertyId", entity.PropertyId);
                param.Add("@FoodQuality",entity.FoodQuality);
                param.Add("@Security",entity.Security);
                param.Add("@Wifi",entity.Wifi);
                param.Add("@Staff",entity.Staff);
                param.Add("@Maintenance",entity.Maintenance);
                param.Add("@Description",entity.Description);
                param.Add("@FeedBackByName", entity.FeedBackByName);
                param.Add("@IsActive",entity.IsActive);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
            }
            catch
            {
                throw;
            }
        }


        public void deletePGRoomData(PGRoomData PGRoomData)
        {
            try
            {
                string query = "UPDATE Tenant_Master SET RoomId = null  WHERE RoomId = @PGRoomInventoryId ";
                query += " DELETE FROM PG_RoomData WHERE PGRoomInventoryId = @PGRoomInventoryId ";
                var param = new DynamicParameters();
                param.Add("@PGRoomInventoryId", PGRoomData.PGRoomInventoryId);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
            }
            catch
            {
                throw;
            }
        }

        public PGRoomData addPGRoomData(PGRoomData PGRoomData)
        { 
            try
            {
                string query = "INSERT INTO PG_RoomData (PropertyId , RoomNo , RoomType , FloorNo )  VALUES ( @PropertyId , @RoomNo , @RoomType , @FloorNo ) ";
                query += " SELECT * FROM PG_RoomData WHERE PGRoomInventoryId = (SELECT SCOPE_IDENTITY())";
                var param = new DynamicParameters(); 
                param.Add("@RoomNo", PGRoomData.RoomNo);  
                param.Add("@RoomType", PGRoomData.RoomType);
                param.Add("@PropertyId", PGRoomData.PropertyId); 
                param.Add("@FloorNo", PGRoomData.FloorNo);   
                return SqlMapper.Query<PGRoomData>(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text).FirstOrDefault();
            } 
            catch (Exception e)
            { 
                throw;
            }
        }


        public IEnumerable<FurnishingMaster> GetAllFurnishing()
        {
            return SqlMapper.Query<FurnishingMaster>(_transaction.Connection, "SELECT * FROM Furnishing_Master", null, _transaction);
        }

        public int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyType", entity.PropertyMaster.PropertyType);
                param.Add("@CreatedBy", entity.PropertyMaster.CreatedBy);
                param.Add("@City", entity.PropertyMaster.City);
                param.Add("@Locality", entity.PropertyMaster.Locality);
                param.Add("@Street", entity.PropertyMaster.Street);
                param.Add("@Latitude", entity.PropertyMaster.Latitude);
                param.Add("@Longitude", entity.PropertyMaster.Longitude);
                param.Add("@ContactPersonName", entity.PropertyMaster.ContactPersonName);
                param.Add("@ContactPersonMobile", entity.PropertyMaster.ContactPersonMobile);
                param.Add("@LocationMapLink", entity.PropertyMaster.LocationMapLink);

                param.Add("@ApartmentType", entity.PropertyDetail.ApartmentType);
                param.Add("@ApartmentName", entity.PropertyDetail.ApartmentName);
                param.Add("@BhkType", entity.PropertyDetail.BhkType);
                param.Add("@RoomType", entity.PropertyDetail.RoomType);
                param.Add("@TenantGender", entity.PropertyDetail.TenantGender);
                param.Add("@PropertySize", entity.PropertyDetail.PropertySize);
                param.Add("@Facing", entity.PropertyDetail.Facing);
                param.Add("@PropertyAge", entity.PropertyDetail.PropertyAge);
                param.Add("@FloorNo", entity.PropertyDetail.FloorNo);
                param.Add("@TotalFloor", entity.PropertyDetail.TotalFloor);
                param.Add("@BathRooms", entity.PropertyDetail.BathRooms);
                param.Add("@Balconies", entity.PropertyDetail.Balconies);
                param.Add("@WaterSupply", entity.PropertyDetail.WaterSupply);
                param.Add("@GateSecurity", entity.PropertyDetail.GateSecurity);

                /*******Rental detail ********/
                param.Add("@OwnerExpectedRent", entity.PropertyDetail.OwnerExpectedRent);
                param.Add("@OwnerExpectedDeposit", entity.PropertyDetail.OwnerExpectedDeposit);
                param.Add("@ExpectedRent", entity.PropertyDetail.ExpectedRent);
                param.Add("@ExpectedDeposit", entity.PropertyDetail.ExpectedDeposit);
                param.Add("@IsRentNegotiable", entity.PropertyDetail.IsRentNegotiable);
                param.Add("@AvailableFrom", entity.PropertyDetail.AvailableFrom);
                param.Add("@PreferredTenant", entity.PropertyDetail.PreferredTenant);
                param.Add("@Parking", entity.PropertyDetail.Parking);
                param.Add("@Furnishing", entity.PropertyDetail.Furnishing);
                param.Add("@Description", entity.PropertyDetail.Description);

                if (entity.PropertyDetail.FurnishingDetails != null)
                {
                    DataTable dtFurnishingDetail = Convert.ToDataTable<PropertyFurnishingDetails>(entity.PropertyDetail.FurnishingDetails);
                    param.Add("@PropertyFurnishingDetail", dtFurnishingDetail.AsTableValuedParameter("PropertyFurnishingDetails"));
                }

                if (entity.GalleryDetails != null)
                {
                    DataTable dtGallery = Convert.ToDataTable<PropertyGalleryDetails>(entity.GalleryDetails);
                    dtGallery.Columns.Remove("PropertyGalleryId");
                    param.Add("@GalleryDetails", dtGallery.AsTableValuedParameter("GalleryDetails"));
                }

                if (entity.AmenityDetails != null)
                {
                    DataTable dtAmenity = Convert.ToDataTable<PropertyAmenitiesDetails>(entity.AmenityDetails);
                    param.Add("@PropertyAmenityDetails", dtAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.ExecuteScalar<int>(_transaction.Connection, "usp_InsertPropertyFlatRoomFlatMate_Admin", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public int SavePropertySEO(PropertySEO entity)
        {
            var param = new DynamicParameters();
            param.Add("@PropertyId",entity.PropertyId);           
            param.Add("@MetaTitle", entity.MetaTitle);
            param.Add("@MetaDesc", entity.MetaDesc);
            param.Add("@JsonDesc", entity.JsonDesc);
            param.Add("@QueryType", 1);

          return SqlMapper.Execute(_transaction.Connection, "udsp_property_seo", param, _transaction, commandType: CommandType.StoredProcedure);
        }       
    }

    public interface IPropertyDA
    {
        IEnumerable<PropertyMaster> GetAllProperty(int pageSize, int pageIndex, string propertyType, string propertyStatus,int propertyId,int ownerId,string ownerName, out int totalRecord);
        Property GetProperty(int propertyId);
        IEnumerable<PGListing> GetAllPG(int pageSize, int pageIndex, string status, out int totalRecord);
        string UpdatePropertyDetail(int propertyId, int userId, PropertyDetailFlatRoom entity);
        string UpdateLocalityDetail(int propertyId, int userId, PropertyMaster entity);
        string UpdateRentalDetail(int propertyId,string propertyType, int userId, PropertyDetailFlatRoom entity);
        string AddNewGallery(int propertyId, int userId, string file);
        string DeleteGallery(int propertyId, int userId, int imageId);
        string UpdatePropertyAmenity(int propertyId, int userId, IEnumerable<PropertyAmenitiesDetails> entity);
        string UpdatePGRoomDetail(int propertyId, int userId, IEnumerable<PGRoomRentalDetail> roomRentalDetail, IEnumerable<PGRoomAmenity> roomAmenities);
        string UpdatePGDetail(int propertyId, int userId, PGDetail pgDetail, IEnumerable<PGRule> pgRules);
        string UpdatePropertyStatus(int propertyId, string status);
        void UpdatePropertyArea(int propertyId, int areaId);
        int AddNewPG(PropertyPG entity);
        void UpdatePropertyFeedbackStatus(int id,bool isActive);
        IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid);
        IEnumerable<PropertyFeedBack> GetAllPGFeedBack();
        void AddPGFeedback(PropertyFeedBack entity);
        IEnumerable<FurnishingMaster> GetAllFurnishing();
        int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity);
        int SavePropertySEO(PropertySEO entity);
        PGRoomData addPGRoomData(PGRoomData PGRoomData);
        void deletePGRoomData(PGRoomData PGRoomData);
    }
}
