﻿using Dapper;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;
namespace FH.Admin.Data
{
    public class DesignationMasterDA : IDesignationMasterDA
    {
        IDbTransaction _transaction;
        public DesignationMasterDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<DesignationMaster> GetAllDesignation()
        {
            return SqlMapper.Query<DesignationMaster>(_transaction.Connection, "SELECT * FROM Designation_Master", null, _transaction,
                   commandType: CommandType.Text);
        }
    }

    public interface IDesignationMasterDA
    {
        IEnumerable<DesignationMaster> GetAllDesignation();
    }
}
