﻿using Dapper;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FH.Admin.Data
{
    public class VendorDA : IVendorDA
    {
        IDbTransaction _transaction;
        public VendorDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public IEnumerable<AgentList> GetAllAgent()
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 1);            
            return SqlMapper.Query<AgentList>(_transaction.Connection, "usp_agent_detail_admin", param, _transaction,commandType: CommandType.StoredProcedure);
        }
        public IEnumerable<AgentList> GetAllOwner(UserDetails de)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",3);
            param.Add("@UserId", de.UserId);
            param.Add("@Mobile", de.Mobile);
            return SqlMapper.Query<AgentList>(_transaction.Connection, "usp_agent_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);
        }
        public IEnumerable<AgentList> GetAllBuyer()
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",4);
            return SqlMapper.Query<AgentList>(_transaction.Connection, "usp_agent_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<AccountList> AccountList(UserDetails de)
        {
            var param = new DynamicParameters();
            param.Add("@UserId", de.UserId);
            param.Add("@AccountId", de.AccountId);
            param.Add("@QueryType", 5);
            return SqlMapper.Query<AccountList>(_transaction.Connection, "usp_agent_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure);
        }
        public Agent GetAgent(int agentId)
        {
            Agent objAgent =new Agent();
            var param = new DynamicParameters();
            param.Add("@QueryType", 2);
            param.Add("@AgentId", agentId);
            using (var multiRecord = SqlMapper.QueryMultiple(_transaction.Connection, "usp_agent_detail_admin", param, _transaction, commandType: CommandType.StoredProcedure))
            {
                objAgent=multiRecord.ReadSingleOrDefault<Agent>();               
            }
            return objAgent;
        }
        public void UpdateAgent(Agent entity)
        {
            var param = new DynamicParameters();
            param.Add("@AgentId", entity.Id);
            param.Add("@Name", entity.Name);
            param.Add("@Email", entity.Email);
            param.Add("@AccountHolderName", entity.AccountHolderName);
            param.Add("@AccountNo", entity.AccountNo);
            param.Add("@IFSCCode", entity.IFSCCode);
            param.Add("@CompanyName", entity.CompanyName);
            param.Add("@OwnerName", entity.OwnerName);
            param.Add("@OfficialEmail", entity.OfficialEmail);
            param.Add("@OfficeAddress", entity.OfficeAddress);
            param.Add("@GSTNo", entity.GSTNo);
            param.Add("@Relationship", entity.Relationship);
            param.Add("@CityId", entity.CityId);
            param.Add("@AreaId", entity.AreaId);
            param.Add("@PanNo", entity.PANNo);
           SqlMapper.Execute(_transaction.Connection, "usp_update_agent_admin", param, _transaction, null, CommandType.StoredProcedure);
        }

        public void AddOwner(OwnerDE entity)
        {
            var param = new DynamicParameters();
            param.Add("@Name", entity.Name);
            param.Add("@Email", entity.Email);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@Password", entity.Password);
            param.Add("@QueryType", 11);
            SqlMapper.Execute(_transaction.Connection, "Usp_UserDetails", param, _transaction, null, CommandType.StoredProcedure);
        }

        public IEnumerable<OwnerDE> GetOwner(OwnerDE entity)
        {
            var param = new DynamicParameters();
            param.Add("@UserId", entity.UserId);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@Email", entity.Email);
            param.Add("@QueryType", 12);
            var owner= SqlMapper.Query<OwnerDE>(_transaction.Connection, "Usp_UserDetails", param, _transaction,commandType: CommandType.StoredProcedure);
            return owner;
        }

        public void UpdateOwner(OwnerDE entity)
        {
            var param = new DynamicParameters();
            param.Add("@UserId", entity.UserId);
            param.Add("@Name", entity.Name);
            param.Add("@Email", entity.Email);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@IsActive", entity.IsActive);
            param.Add("@QueryType", 13);
            SqlMapper.Execute(_transaction.Connection, "Usp_UserDetails", param, _transaction, null, CommandType.StoredProcedure);
        }

        public void EditAccount(AccountList entity)
        {
            var param = new DynamicParameters();
            param.Add("@PaytmMID", entity.PaytmMID);
            param.Add("@AccountId", entity.AccountId);
            param.Add("@IsActive", entity.IsActive);
            param.Add("@QueryType", 6);
            SqlMapper.Execute(_transaction.Connection, "usp_agent_detail_admin", param, _transaction, null, CommandType.StoredProcedure);
        }
    }

    public interface IVendorDA
    {
        IEnumerable<AgentList> GetAllAgent();
        void UpdateAgent(Agent entity);
        Agent GetAgent(int agentId);
        IEnumerable<AgentList> GetAllOwner(UserDetails de);
        IEnumerable<AgentList> GetAllBuyer();

        IEnumerable<AccountList> AccountList(UserDetails de);
        void AddOwner(OwnerDE entity);
        IEnumerable<OwnerDE> GetOwner(OwnerDE entity);
        void UpdateOwner(OwnerDE entity);
        void EditAccount(AccountList entity);
    }
}
