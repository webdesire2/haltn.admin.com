﻿using Dapper;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;

namespace FH.Admin.Data
{
   public class PropertyNearByPlaceMappingDA:IPropertyNearByPlaceMappingDA
    {
        IDbTransaction _transaction;
        public PropertyNearByPlaceMappingDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public void InsertPropertyNearByPlace(IEnumerable<PropertyNearbyPlace> lstNearByPlace,int propertyId)
        {         
            DataTable dt=Convert.ToDataTable<PropertyNearbyPlace>(lstNearByPlace);
            
            var param = new DynamicParameters();
            param.Add("@NearByPlaceTypes", dt.AsTableValuedParameter("NearByPlace"));            
            param.Add("@PropertyId", propertyId);
            param.Add("@QueryType", 1);

            SqlMapper.Execute(_transaction.Connection, "usp_Property_NearByPlace_Mapping", param, _transaction, commandType: CommandType.StoredProcedure);
        }
    }

    public interface IPropertyNearByPlaceMappingDA
    {
        void InsertPropertyNearByPlace(IEnumerable<PropertyNearbyPlace> lstNearByPlace,int propertyId);
    }
}
