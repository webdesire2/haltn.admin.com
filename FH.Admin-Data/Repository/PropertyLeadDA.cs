﻿using Dapper;
using FH.Admin.Entities;
using System;
using System.Collections.Generic;
using System.Data;

namespace FH.Admin.Data
{
   public class PropertyLeadDA:IPropertyLeadDA
    {
        private IDbTransaction _transaction;
        public PropertyLeadDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<PropertyLeadList> GetAllLead(int pageIndex,int pageSize,string status)
        {
            IEnumerable<PropertyLeadList> lstLead;
            try
            {                
                var param = new DynamicParameters();
                param.Add("@PageIndex", pageIndex);
                param.Add("@PageSize", pageSize);
                param.Add("@LeadStatus", status);
                if(status.ToUpper()=="DONE")
                    lstLead = SqlMapper.Query<PropertyLeadList>(_transaction.Connection, "usp_property_lead_payment_listing_admin", param, _transaction, commandType: CommandType.StoredProcedure);
                 else
                 lstLead = SqlMapper.Query<PropertyLeadList>(_transaction.Connection, "usp_property_lead_listing_admin", param, _transaction, commandType: CommandType.StoredProcedure);                
                return lstLead;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<UserLeadFollowUp> GetLeadFollowUpTeam(int areaId)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 1);
            param.Add("@AreaId", areaId);
            return SqlMapper.Query<UserLeadFollowUp>(_transaction.Connection, "usp_lead_followup", param, _transaction, commandType: CommandType.StoredProcedure);
        }

        public void UpdateLeadStatusAndAssignTo(int leadId,int assignTo,int assignBy,string status)
        {
            var param = new DynamicParameters();
            param.Add("@LeadId",leadId);            
            param.Add("@AssignToUserId",assignTo);
            param.Add("@AssignBy", assignBy);
            param.Add("@LeadStatus", status);
            param.Add("@QueryType",2);
            SqlMapper.Execute(_transaction.Connection, "usp_lead_followup", param, _transaction, commandType: CommandType.StoredProcedure);
        }

        public void UpdateLeadStatus(int leadId,string status,string comment,int updatedBy)
        {
            var param = new DynamicParameters();
            param.Add("@LeadId", leadId);            
            param.Add("@LeadStatus", status);
            param.Add("@Comment",comment);
            param.Add("@AssignBy", updatedBy);
            param.Add("@QueryType",4);
            SqlMapper.Execute(_transaction.Connection, "usp_lead_followup", param, _transaction, commandType: CommandType.StoredProcedure);
        }

        public int UpdateLeadPaymentDetail(LeadPaymentDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@LeadId",entity.LeadId);
            param.Add("@LeadStatus","DOCUMENTATION");
            param.Add("@TotalBrokerage", entity.TotalBrokerage);
            param.Add("@ReceivedAmount", entity.ReceivedAmount);
            param.Add("@OurCharges", entity.OurCharges);
            param.Add("@IsPaymentReceived", entity.IsPaymentReceived);
            param.Add("@PaymentExpectedDate", entity.PaymentExpectedDate);
            param.Add("@PaymentReceivedDate", entity.PaymentReceivedDate);
            param.Add("@Comment", entity.Comment);            
            param.Add("@AssignBy", entity.CreatedBy);
            param.Add("@QueryType", 6);            
            return SqlMapper.ExecuteScalar<int>(_transaction.Connection, "usp_lead_followup", param, _transaction, commandType: CommandType.StoredProcedure);
        }

        public void AddClientRequirementNote(int leadId,string remark)
        {
            var param = new DynamicParameters();
            param.Add("@LeadId", leadId);
            param.Add("@Remark",remark);
            SqlMapper.Execute(_transaction.Connection, "Update Property_Leads SET Remarks=@Remark WHERE LeadId=@LeadId", param, _transaction, commandType: CommandType.Text);
        }
        public IEnumerable<PropertyLeadLog> GetLeadLog(int leadId)
        {
            var param = new DynamicParameters();
            param.Add("@LeadId", leadId);            
            param.Add("@QueryType",5);
            return SqlMapper.Query<PropertyLeadLog>(_transaction.Connection, "usp_lead_followup", param, _transaction, commandType: CommandType.StoredProcedure);
        }
        
        public Tuple<int,string> AddNewLead(NewLead entity)
        {
            Tuple<int, string> tpl=null;
            var param = new DynamicParameters();
            param.Add("@Mobile",entity.Mobile);
            param.Add("@Name",entity.Name);
            param.Add("@Email", entity.Email);
            param.Add("@CreatedBy", entity.CreatedBy);
            param.Add("@QueryType", 1);
            using (var dr = SqlMapper.ExecuteReader(_transaction.Connection, "usp_add_new_lead", param, _transaction, commandType: CommandType.StoredProcedure))
            {
                while (dr.Read())
                {                 
                    tpl = new Tuple<int, string>((int)dr[0],(string)dr[1]);
                }
                dr.Close();
            }
            return tpl;           
           
        }
        public Tuple<int, string> AssignMoreProperty(NewLead entity)
        {
            Tuple<int, string> tpl = null;
            var param = new DynamicParameters();
            param.Add("@PropertyId", entity.PropertyId);
            param.Add("@LeadId", entity.LeadId);
            param.Add("@VisitDate", entity.VisitDate);
            param.Add("@VisitTime", entity.VisitTime);            
            param.Add("@AssignBy", entity.CreatedBy);
            param.Add("@QueryType",7);
            using (var dr = SqlMapper.ExecuteReader(_transaction.Connection, "usp_lead_followup", param, _transaction, commandType: CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    tpl = new Tuple<int, string>((int)dr[0], (string)dr[1]);
                }
                dr.Close();
            }
            return tpl;

        }


    }

    public interface IPropertyLeadDA
    {
        IEnumerable<PropertyLeadList> GetAllLead(int pageIndex, int pageSize, string status);
        IEnumerable<UserLeadFollowUp> GetLeadFollowUpTeam(int areaId);
        void UpdateLeadStatusAndAssignTo(int leadId, int assignTo, int assignBy, string status);
        void UpdateLeadStatus(int leadId, string status, string comment,int updatedBy);
        IEnumerable<PropertyLeadLog> GetLeadLog(int leadId);
        int UpdateLeadPaymentDetail(LeadPaymentDetail entity);
        void AddClientRequirementNote(int leadId, string remark);
        Tuple<int, string> AddNewLead(NewLead entity);
        Tuple<int, string> AssignMoreProperty(NewLead entity);
    }
            
}
