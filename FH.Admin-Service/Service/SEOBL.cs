﻿using FH.Admin.Data;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FH.Admin.Service
{
    public class SEOBL
    {
        private readonly SEODA _seoDA;
        public SEOBL()
        {
            _seoDA = new SEODA();
        }

        public string AddCategory(SEODE de)
        {
           return _seoDA.Execute<string>(de, SEOCallValue.AddCategory).FirstOrDefault();
        }

        public string UdpateCategory(SEODE de)
        {
            return _seoDA.Execute<string>(de, SEOCallValue.UpdateCategory).FirstOrDefault();
        }

        public SEOCategoryVM GetAllCategory(SEODE de)
        {
            return _seoDA.GetAllCategory(de);
        }

        public IEnumerable<SEOCategory> GetCategoryByAutoSearch(SEODE de)
        {
            return _seoDA.Execute<SEOCategory>(de, SEOCallValue.GetCategryByAutoSearch);
        }

        public string AddPropertyCategory(SEODE de)
        {
            return _seoDA.Execute<string>(de, SEOCallValue.AddPropertyCategory).FirstOrDefault();
        }

        public void RemovePropertyCategory(SEODE de)
        {
            _seoDA.Execute<string>(de, SEOCallValue.RemocePropertyCategory);
        }

        public IEnumerable<PropertySEOCategory> GetPropertyCategory(SEODE de)
        {
            return _seoDA.Execute<PropertySEOCategory>(de, SEOCallValue.GetPropertyCategory);
        }

        public void AddPropertySEO(SEODE de)
        {
            _seoDA.Execute<int>(de, SEOCallValue.AddPropertySEO);
        }

        public SEOCategory GetCategory(SEODE de)
        {
            return _seoDA.GetCategory(de.Id);
        }

        public void DeleteCategory(SEODE de)
        {
            _seoDA.Execute<int>(de, SEOCallValue.DeleteCategory);
        }

        public SEOCategory GenerateCategory(string locality,string propertyType,string prefix,string lat,string lng)
        {
            SEOCategory seo = new SEOCategory();
            var localityWithinOneKilometers = GetLocalitiesForCategoryWithinOneKilometer(lat, lng);

            if (propertyType.ToLower() == "pg")
            {
                if (prefix.ToLower() == "boys")
                {
                    seo.Category = $"Boys pg on rent {locality}";
                    seo.Url = seo.Category.Replace(" ", "-");
                    seo.MetaTitle = $"Boys pg {locality}";
                    seo.MetaKeyword = $"Boys pg on rent {locality}, boys paying guest accommodation {locality}, gents pg on rent {locality}, pg with food facility {locality}, boys pg {locality}, men’s paying guest accommodation {locality}, male pg {locality}, pg for couples {locality}";
                    seo.MetaDesc = $"Boys pg {locality} with food, tv, internet, bed, personal washroom, washing machine. Boys Pg {localityWithinOneKilometers}.";
                    seo.Heading = $"Boys pg {locality}";
                    seo.HeadingContent = $"Find boys pg {locality} with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, Personal Washroom, All Meals inclusive. Boys Pg {localityWithinOneKilometers}. Contact us at info@haltn.com";

                }
                else if (prefix.ToLower() == "girls")
                {
                    seo.Category = $"Girl pg on rent {locality}";
                    seo.Url = seo.Category.Replace(" ", "-");
                    seo.MetaTitle = $"Girl pg {locality}";
                    seo.MetaKeyword = $"Safe and secure girl pg on rent {locality}, ladies paying guest accommodation {locality}, female pg on rent {locality}, pg with food facility {locality}, women pg {locality}, girl paying guest accommodation {locality}, coliving pg {locality}, pg for couples {locality}";
                    seo.MetaDesc = $"Girls Pg {localityWithinOneKilometers} with food, tv, internet, bed, personal washroom, washing machine";
                    seo.Heading = $"Girl pg {locality}";
                    seo.HeadingContent = $"Find female pg {locality}, Girl Pg {localityWithinOneKilometers} with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, Personal Washroom, All Meals inclusive. Contact us at info@haltn.com";

                }
                else if (prefix.ToLower() == "co-living")
                {
                    seo.Category = $"Coliving pg on rent {locality}";
                    seo.Url = seo.Category.Replace(" ", "-");
                    seo.MetaTitle = $"Coliving pg for boys and girls {locality}";
                    seo.MetaKeyword = $"Coliving pg on rent {locality}, boys & girls  paying guest accommodation {locality}, male & female pg on rent {locality}, pg with food facility {locality}, pg for men & women {locality}, co-living paying guest accommodation {locality}, unisex pg {locality}, pg for couples {locality}";
                    seo.MetaDesc = $"Coliving pg for boys and girls {localityWithinOneKilometers} with food, tv, internet, bed, personal washroom, washing machine";
                    seo.Heading = $"Coliving pg for boys and girls {locality}";
                    seo.HeadingContent = $"Coliving pg {locality},Fully furnished Coliving pg for boys and girls {localityWithinOneKilometers} with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, Personal Washroom, All Meals inclusive. Contact us at info@haltn.com";

                }

            }
            else if (propertyType.ToLower() == "room")
            {
                seo.Category = $"Room or 1RK {locality}";
                seo.Url = seo.Category.Replace(" ", "-");
                seo.MetaTitle = $"Room/1RK  for boys and girls {locality}";
                seo.MetaKeyword = $"Room /1RK on rent {locality}, Room/1RK  on rent for boys and girls {locality}, male & female Room/1Rk on rent {locality}";
                seo.MetaDesc = $"Room/1RK for boys and girls {localityWithinOneKilometers} with tv, internet, bed, personal washroom, washing machine";
                seo.Heading = $"Room/1RK for boys and girls {locality}";
                seo.HeadingContent = $"Room/1RK {locality},  Fully furnished Coliving Room/1Rk for boys and  girls {localityWithinOneKilometers} with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, and Washroom. Contact us at info@haltn.com";
            }
            else if (propertyType.ToLower() == "flat")
            {
                seo.Category = $"{prefix} flat for rent {locality}";
                seo.Url = seo.Category.Replace(" ", "-");
                seo.MetaTitle = $"Furnished {prefix} Flat on rent starts at Rs.12000 {locality}";
                seo.MetaKeyword = $"{prefix} flat on rent {locality}, {prefix} flat {locality}. {prefix} flat on rent {locality}, {prefix} flat with all facility {locality}, {prefix} flat for boys {locality}, {prefix} flat for girls {locality}, {prefix} flat for couples {locality}, {prefix} flat for family {locality}";
                seo.MetaDesc = $"fully furnished {prefix} flat on rent {locality}, starts at 12k furnished with tv, internet, bed, washroom, washing machine";
                seo.Heading = $"Fully Furnished {prefix} flat {locality} for bachelors & families.";
                seo.HeadingContent = $"Find {prefix} flats {locality}, rent starts at Rs.12000/- Fully furnished {prefix} Flats with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, Personal Washroom, Kitchen inclusive. Contact us at info@haltn.com";
            }
            else if (propertyType.ToLower() == "flatmate")
            {
                if (prefix.ToLower() == "male")
                {
                    seo.Category = $"Shared Flat for male flatmates near {locality}";
                    seo.Url = seo.Category.Replace(" ", "-");
                    seo.MetaTitle = $"Shared Flat for male flatmates on rent {locality}";
                    seo.MetaKeyword = $"Flat for rent on sharing basis {locality}, flat for flatments on sharing basis {locality}, shared flat for boys flatmates on rent {locality}, male flatmate required for fully furnished flat {locality}, fully furnished flat for male flatmate {locality}, shared flat available for gents flatmate {locality}, fully furnished flat for boy flatmate {locality}, male flatmate required to share a flat {locality}, men’s flatmate required to share a flat {locality}";
                    seo.MetaDesc = $"fully furnished Shared Flat for male flatmates on rent {locality}, starts at 9k furnished with tv, internet, bed, washroom, washing machine";
                    seo.Heading = $"Furnished Shared Flat for male flatmates {locality}.";
                    seo.HeadingContent = $"Find fully furnished shared independent flats for flatmates {locality}, rent starts at Rs.9000/- Fully furnished Flats have shared rooms for male flatmates with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, Personal Washroom, Kitchen inclusive. Contact us at info@haltn.com";
                }
                else if(prefix.ToLower()=="female")
                {
                    seo.Category = $"Shared Flat for female flatmates near {locality}";
                    seo.Url = seo.Category.Replace(" ", "-");
                    seo.MetaTitle = $"Shared Flat for female flatmates on rent {locality}";
                    seo.MetaKeyword = $"Flat for rent on sharing basis {locality}, flat for flatments on sharing basis {locality}, shared flat for female flatmates on rent {locality}, female flatmate required for fully furnished flat {locality}, fully furnished flat for male flatmate {locality}, shared flat available for girls flatmate {locality}, fully furnished flat for ladies flatmate {locality}, male flatmate required to share a flat {locality}, women flatmate required to share a flat {locality}";
                    seo.MetaDesc = $"fully furnished Shared Flat for female flatmates on rent {locality}, starts at 9k furnished with tv, internet, bed, washroom, washing machine";
                    seo.Heading = $"Furnished Shared Flat for female flatmates {locality}.";
                    seo.HeadingContent = $"Find fully furnished shared independent flats for flatmates {locality}, rent starts at Rs.9000/- Fully furnished Flats have shared rooms for female flatmates with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, Personal Washroom, Kitchen inclusive. Contact us at info@haltn.com";
                }
                else if(prefix.ToLower()=="co-living")
                {
                    seo.Category = $"Shared Flat for coliving flatmates near {locality}";
                    seo.Url = seo.Category.Replace(" ", "-");
                    seo.MetaTitle = $"Shared Flat for coliving flatmates on rent {locality}";
                    seo.MetaKeyword = $"Flat for rent on sharing basis {locality}, flat for flatments on sharing basis {locality}, shared flat for female flatmates on rent {locality}, coliving flatmate required for fully furnished flat {locality}, fully furnished flat for coliving flatmate {locality}, shared flat available for coliving flatmate {locality}, fully furnished flat for coliving flatmate {locality}, coliving flatmate required to share a flat {locality}, coliving flatmate required to share a flat {locality}";
                    seo.MetaDesc = $"fully furnished Shared Flat for coliving flatmates on rent {locality}, starts at 9k furnished with tv, internet, bed, washroom, washing machine";
                    seo.Heading = $"Furnished Shared Flat for coliving flatmates {locality}.";
                    seo.HeadingContent = $"Find fully furnished shared independent flats for flatmates {locality}, rent starts at Rs.9000/- Fully furnished Flats have shared rooms for female flatmates with all modern amenities such as TV, Fridge, AC, Washing Machine, Bed, Personal Washroom, Kitchen inclusive. Contact us at info@haltn.com";
                }
            }

            seo.Lat = lat;
            seo.Lng = lng;
            seo.PropertyType = propertyType;
            seo.Prefix = prefix;
            return seo;
        }

        public void MapCategoryToProperty(int propertyId)
        {
            _seoDA.MapCategoryToProperty(propertyId);
        }

        public void AddLocality(List<SEOLocalityMaster> lst)
        {
            foreach(var i in lst)
            {
                SEODE de = new SEODE
                {
                    Category=i.Locality,
                    Lat=i.Lat,
                    Lng=i.Lng,
                    City=i.City
                };
                _seoDA.Execute<int>(de, SEOCallValue.AddLocality);
            }
        }

        public SEOLocalityVM GetAllLocality(string searchKey,int pageNo)
        {
            SEODE de = new SEODE
            {
                PageNo=pageNo,
                SearchKey=searchKey
            };

            return _seoDA.GetAllLocality(de);
        }

        public void DeleteLocality(int id)
        {
            string query = $"DELETE FROM SEO_Locality_Master WHERE Id={id}";
            _seoDA.ExecuteQuery<string>(query).FirstOrDefault();
        }

        public SEOLocalityMaster GetLocalityById(int id)
        {
            string query = $"SELECT * FROM SEO_Locality_Master WHERE Id={id}";
            return _seoDA.ExecuteQuery<SEOLocalityMaster>(query).FirstOrDefault();
        }

        public string UpdateLocality(SEOLocalityMaster model)
        {
            string query = $@"IF NOT EXISTS(SELECT 1 FROM SEO_Locality_Master WHERE Id !={model.Id} AND Locality='{model.Locality}')
                          BEGIN
                            UPDATE SEO_Locality_Master SET Locality ='{model.Locality}' WHERE Id ={model.Id}
                          END
                          ELSE
                               BEGIN
                                 SELECT 'Locality already exist.'
                              END";

            return _seoDA.ExecuteQuery<string>(query).FirstOrDefault();
        }

        public IEnumerable<SEOLocalityMaster> GetLocalityByLatLng(string lat,string lng,int radius)
        {
            return _seoDA.GetLocalityByLatLng(lat, lng,radius);
        }

        private string GetLocalitiesForCategoryWithinOneKilometer(string lat, string lng)
        {
            StringBuilder categorylocalities = new StringBuilder("");
            var localitiesList = GetLocalityByLatLng(lat, lng, 1); // within 1 km radius
            foreach (var locality in localitiesList)
            {
                categorylocalities.Append(locality.Locality);
                categorylocalities.Append(", ");
            }
            var categoryLocalities = categorylocalities.ToString();
            return categoryLocalities == "" ? categoryLocalities : categoryLocalities.Substring(0, categoryLocalities.Length - 2);
    }
    }
}
