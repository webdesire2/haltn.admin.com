﻿using FH.Admin.Entities;
using FH.Admin.Data;
using System.Data;
using System.Collections.Generic;

namespace FH.Admin.Service
{
    public class VendorBL:IVendorBL
    {
        private readonly IVendorDA vendorDA;
        public VendorBL(IDbTransaction transaction)
        {
            vendorDA = new VendorDA(transaction);
        }
        public IEnumerable<AgentList> GetAllAgent()
        {
            return vendorDA.GetAllAgent();
        }
        public IEnumerable<AgentList> GetAllOwner(UserDetails de)
        {
            return vendorDA.GetAllOwner(de);
        }
        public IEnumerable<AgentList> GetAllBuyer()
        {
            return vendorDA.GetAllBuyer();
        }

        public IEnumerable<AccountList> AccountList(UserDetails de)
        {
            return vendorDA.AccountList(de);
        }
        public void UpdateAgent(Agent entity)
        {
            vendorDA.UpdateAgent(entity);
        }

        public Agent GetAgent(int agentId)
        {
            return vendorDA.GetAgent(agentId);
        }

        public IEnumerable<OwnerDE> GetOwner(OwnerDE entity)
        {
            return vendorDA.GetOwner(entity);
        }

        public void AddOwner(OwnerDE entity)
        {
            vendorDA.AddOwner(entity);
        }

        public void UpdateOwner(OwnerDE entity)
        {
            vendorDA.UpdateOwner(entity);
        }

        public void EditAccount(AccountList entity)
        {
            vendorDA.EditAccount(entity);
        }


    }

    public interface IVendorBL
    {
        IEnumerable<AgentList> GetAllAgent();
        IEnumerable<AgentList> GetAllOwner(UserDetails de);
        IEnumerable<AgentList> GetAllBuyer();
        IEnumerable<AccountList> AccountList(UserDetails de);
        void UpdateAgent(Agent entity);
        Agent GetAgent(int agentId);
        IEnumerable<OwnerDE> GetOwner(OwnerDE entity);
        void AddOwner(OwnerDE entity);
        void UpdateOwner(OwnerDE entity);

        void EditAccount(AccountList entity);
    }
}
