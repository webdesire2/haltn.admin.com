﻿using FH.Admin.Entities;
using System.Collections.Generic;
using FH.Admin.Data;
using System.Data;

namespace FH.Admin.Service
{
    public class DesignationMasterBL : IDesignationMasterBL
    {
        private readonly IDesignationMasterDA designationMasterDA;
        public DesignationMasterBL(IDbTransaction transaction)
        {
            designationMasterDA = new DesignationMasterDA(transaction);
        }
        public IEnumerable<DesignationMaster> GetAllDesignation()
        {
            return designationMasterDA.GetAllDesignation();
        }
    }

    public interface IDesignationMasterBL
    {
        IEnumerable<DesignationMaster> GetAllDesignation();
    }
}
