﻿using FH.Admin.Data;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Data;

namespace FH.Admin.Service
{
    public class PropertyNearByPlaceMappingBL:IPropertyNearByPlaceMappingBL
    {
        private readonly IPropertyNearByPlaceMappingDA _placeMapping;
        public PropertyNearByPlaceMappingBL(IDbTransaction transaction)
        {           
            _placeMapping = new PropertyNearByPlaceMappingDA(transaction);
        }

        public void InsertNearByPlace(IEnumerable<PropertyNearbyPlace> lstNearByPlace,int propertyId)
        {
            _placeMapping.InsertPropertyNearByPlace(lstNearByPlace,propertyId);
        }
    }

    public interface IPropertyNearByPlaceMappingBL
    {
        void InsertNearByPlace(IEnumerable<PropertyNearbyPlace> lstNearByPlace,int propertyId);
    }
}
