﻿using System.Collections.Generic;
using System.Data;
using FH.Admin.Entities;
using FH.Admin.Data;
using System;

namespace FH.Admin.Service
{
    public class PropertyLeadBL:IPropertyLeadBL
    {
        private readonly IPropertyLeadDA _propertyLeadDA;
        public PropertyLeadBL(IDbTransaction transaction)
        {
            _propertyLeadDA = new PropertyLeadDA(transaction);
        }

        public IEnumerable<PropertyLeadList> GetAllLead(int pageIndex, int pageSize,string status)
        {
            return _propertyLeadDA.GetAllLead(pageIndex,pageSize,status);
        }
        public IEnumerable<UserLeadFollowUp> GetLeadFollowUpTeam(int areaId)
        {
            return _propertyLeadDA.GetLeadFollowUpTeam(areaId);
        }

        public void UpdateLeadStatusAndAssignTo(int leadId, int assignTo, int assignBy, string status)
        {
            _propertyLeadDA.UpdateLeadStatusAndAssignTo(leadId, assignTo, assignBy, status);
        }

        public void UpdateLeadStatus(int leadId, string status, string comment,int updatedBy)
        {
            _propertyLeadDA.UpdateLeadStatus(leadId, status, comment,updatedBy);
        }

        public IEnumerable<PropertyLeadLog> GetLeadLog(int leadId)
        {
           return _propertyLeadDA.GetLeadLog(leadId);
        }
        public int UpdateLeadPaymentDetail(LeadPaymentDetail entity)
        {
            return _propertyLeadDA.UpdateLeadPaymentDetail(entity);
        }

        public void AddClientRequirementNote(int leadId, string remark)
        {
            _propertyLeadDA.AddClientRequirementNote(leadId, remark);
        }
        public Tuple<int, string> AddNewLead(NewLead entity)
        {
           return  _propertyLeadDA.AddNewLead(entity);
        }

        public Tuple<int, string> AssignMoreProperty(NewLead entity)
        {
            return _propertyLeadDA.AssignMoreProperty(entity);
        }
    }

    public interface IPropertyLeadBL
    {
        IEnumerable<PropertyLeadList> GetAllLead(int pageIndex, int pageSize, string status);
        IEnumerable<UserLeadFollowUp> GetLeadFollowUpTeam(int areaId);
        void UpdateLeadStatusAndAssignTo(int leadId, int assignTo, int assignBy, string status);
        void UpdateLeadStatus(int leadId, string status, string comment,int updatedBy);
        IEnumerable<PropertyLeadLog> GetLeadLog(int leadId);
        int UpdateLeadPaymentDetail(LeadPaymentDetail entity);
        void AddClientRequirementNote(int leadId, string remark);
        Tuple<int, string> AddNewLead(NewLead entity);
        Tuple<int, string> AssignMoreProperty(NewLead entity);
    }
}
