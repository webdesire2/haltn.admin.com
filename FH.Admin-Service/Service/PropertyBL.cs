﻿using System.Collections.Generic;
using System.Data;
using FH.Admin.Entities;
using FH.Admin.Data;
using FH.Admin.Data.Repository;
using System;

namespace FH.Admin.Service
{
    public class PropertyBL:IPropertyBL
    {
        private readonly IPropertyDA _propertyDA;
        private readonly SQLRepository _sqlDb;

        public PropertyBL()
        {
            _sqlDb = new SQLRepository();
        }
        public PropertyBL(IDbTransaction transaction)
        {
            _propertyDA = new PropertyDA(transaction);
        }



        //addPGRoomData
        public PGRoomData addPGRoomData(PGRoomData PGRoomData)
        {
            return _propertyDA.addPGRoomData(PGRoomData);
        }

        //addPGRoomData
        public void deletePGRoomData(PGRoomData PGRoomData)
        {
            _propertyDA.deletePGRoomData(PGRoomData);
        }
         
        public IEnumerable<PropertyMaster> GetAllProperty(int pageSize, int pageIndex, string propertyType, string propertyStatus,int propertyId,int ownerId,string ownerMobile ,out int totalRecord)
        {            
            return _propertyDA.GetAllProperty(pageSize, pageIndex,propertyType,propertyStatus,propertyId,ownerId,ownerMobile,out totalRecord);
        }
        public IEnumerable<PGListing> GetAllPG(int pageSize, int pageIndex, string status, out int totalRecord)
        {
            return _propertyDA.GetAllPG(pageSize,pageIndex,status,out totalRecord);
        }

        public Property GetProperty(int propertyId)
        {
            return _propertyDA.GetProperty(propertyId);
        }
        public string UpdatePropertyDetail(int propertyId, int userId, PropertyDetailFlatRoom entity)
        {
            return _propertyDA.UpdatePropertyDetail(propertyId, userId, entity);
        }
        public string UpdateLocalityDetail(int propertyId, int userId, PropertyMaster entity)
        {
            return _propertyDA.UpdateLocalityDetail(propertyId, userId, entity);
        }
        public string UpdateRentalDetail(int propertyId,string propertyType, int userId, PropertyDetailFlatRoom entity)
        {
            return _propertyDA.UpdateRentalDetail(propertyId,propertyType,userId, entity);
        }
        public string AddNewGallery(int propertyId, int userId, string file)
        {
            return _propertyDA.AddNewGallery(propertyId, userId, file);
        }
        public string DeleteGallery(int propertyId, int userId, int imageId)
        {
            return _propertyDA.DeleteGallery(propertyId, userId, imageId);
        }
        public string UpdatePropertyAmenity(int propertyId, int userId, IEnumerable<PropertyAmenitiesDetails> entity)
        {
            return _propertyDA.UpdatePropertyAmenity(propertyId, userId, entity);
        }
        public string UpdatePGRoomDetail(int propertyId, int userId, IEnumerable<PGRoomRentalDetail> roomRentalDetail, IEnumerable<PGRoomAmenity> roomAmenities)
        {
            return _propertyDA.UpdatePGRoomDetail(propertyId, userId, roomRentalDetail, roomAmenities);
        }
        public string UpdatePGDetail(int propertyId, int userId, PGDetail pgDetail, IEnumerable<PGRule> pgRules)
        {
            return _propertyDA.UpdatePGDetail(propertyId, userId, pgDetail, pgRules);
        }
        public string UpdatePropertyStatus(int propertyId, string status)
        {
           return _propertyDA.UpdatePropertyStatus(propertyId, status);
        }
        public void UpdatePropertyArea(int propertyId, int areaId)
        {
            _propertyDA.UpdatePropertyArea(propertyId, areaId);
        }

        public int AddNewPG(PropertyPG input)
        {
           return _propertyDA.AddNewPG(input);
        }
  
        public void UpdatePropertyFeedbackStatus(int id, bool isActive)
        {
             _propertyDA.UpdatePropertyFeedbackStatus(id, isActive);
        }

        public IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid)
        {
           return _propertyDA.GetPGFeedBack(pid);
        }

        public IEnumerable<PropertyFeedBack> GetAllPGFeedBack()
        {
            return _propertyDA.GetAllPGFeedBack();
        }

        public void AddPGFeedback(PropertyFeedBack entity)
        {
            _propertyDA.AddPGFeedback(entity);
        }

        public IEnumerable<FurnishingMaster> GetAllFurnishing()
        {
            return _propertyDA.GetAllFurnishing();
        }

        public int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity)
        {
            return _propertyDA.AddPropertyFlatRoomFlatMate(entity);
        }

        public int SavePropertySEO(PropertySEO entity)
        {
            return _propertyDA.SavePropertySEO(entity);
        }

        public void AddOffer(string title,int offerPercent,int propertyid)
        {
            string query = @"update property_master set OfferTitle=@Title,OfferPercent=@OfferPercent
                           where propertyid=@PropertyId";

            _sqlDb.ExecuteFirst<int>(query,new {@Title=title,@OfferPercent=offerPercent,@PropertyId=propertyid });
        }

        public dynamic GetOffer(int propertyId)
        {
            string query = @"select PropertyId,OfferTitle,OfferPercent from property_master where PropertyId="+propertyId;
            return _sqlDb.ExecuteFirst<dynamic>(query);
        }

        public dynamic GetPropertyDetailForSEO(int propertyId)
        {
            string query = @"select pm.PropertyType,pm.City,pd.BhkType,pd.RoomType,pg.TenantGender,pm.Locality,pm.City from property_master pm
              left join property_details pd on pm.PropertyId = pd.PropertyId
              left join property_details_pg pg on pm.PropertyId = pg.PropertyId
              where pm.PropertyId =" + propertyId;
            return _sqlDb.ExecuteFirst<dynamic>(query);
        }
    }
    public interface IPropertyBL
    {
        IEnumerable<PropertyMaster> GetAllProperty(int pageSize, int pageIndex, string propertyType, string propertyStatus,int propertyId,int ownerId,string ownerMobile, out int totalRecord);
        Property GetProperty(int propertyId);
        PGRoomData addPGRoomData(PGRoomData PGRoomData);
        void deletePGRoomData(PGRoomData PGRoomData);
        IEnumerable<PGListing> GetAllPG(int pageSize, int pageIndex, string status, out int totalRecord);
        string UpdatePropertyDetail(int propertyId, int userId, PropertyDetailFlatRoom entity);
        string UpdateLocalityDetail(int propertyId, int userId, PropertyMaster entity);
        string UpdateRentalDetail(int propertyId, string propertyType, int userId, PropertyDetailFlatRoom entity);
        string AddNewGallery(int propertyId, int userId, string file);
        string DeleteGallery(int propertyId, int userId, int imageId);
        string UpdatePropertyAmenity(int propertyId, int userId, IEnumerable<PropertyAmenitiesDetails> entity);
        string UpdatePGRoomDetail(int propertyId, int userId, IEnumerable<PGRoomRentalDetail> roomRentalDetail, IEnumerable<PGRoomAmenity> roomAmenities);
        string UpdatePGDetail(int propertyId, int userId, PGDetail pgDetail, IEnumerable<PGRule> pgRules);
        string UpdatePropertyStatus(int propertyId, string status);
        void UpdatePropertyArea(int propertyId, int areaId);
        int AddNewPG(PropertyPG input);
        void UpdatePropertyFeedbackStatus(int id, bool isActive);
        IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid);
        IEnumerable<PropertyFeedBack> GetAllPGFeedBack();
        void AddPGFeedback(PropertyFeedBack entity);
        IEnumerable<FurnishingMaster> GetAllFurnishing();
        int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity);
        int SavePropertySEO(PropertySEO entity);
        void AddOffer(string title, int offerPercenta, int propertyid);
        dynamic GetOffer(int propertyId);
        dynamic GetPropertyDetailForSEO(int propertyId);
    }
}
