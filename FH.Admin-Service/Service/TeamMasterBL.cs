﻿using FH.Admin.Entities;
using System.Collections.Generic;
using FH.Admin.Data;
using System.Data;

namespace FH.Admin.Service
{
    public class TeamMasterBL : ITeamMasterBL
    {
        private readonly ITeamMasterDA teamMasterDA;
        public TeamMasterBL(IDbTransaction transaction)
        {
            teamMasterDA = new TeamMasterDA(transaction);
        }
        public IEnumerable<TeamMaster> GetAllTeam()
        {
            return teamMasterDA.GetAllTeam();
        }
    }

    public interface ITeamMasterBL
    {
        IEnumerable<TeamMaster> GetAllTeam();
    }
}
