﻿using FH.Admin.Entities;
using System.Collections.Generic;
using FH.Admin.Data;
using System.Data;

namespace FH.Admin.Service
{
    public class DepartmentBL : IDepartmentMasterBL
    {
        private readonly IDepartmentMasterDA departmentMasterDA;
        public DepartmentBL(IDbTransaction transaction)
        {
            departmentMasterDA = new DepartmentMasterDA(transaction);
        }
        public IEnumerable<DepartmentMaster> GetAllDepartment()
        {
            return departmentMasterDA.GetAllDepartment();
        }
    }

    public interface IDepartmentMasterBL
    {
        IEnumerable<DepartmentMaster> GetAllDepartment();
    }
}
