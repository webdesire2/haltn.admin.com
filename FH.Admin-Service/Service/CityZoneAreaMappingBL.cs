﻿
using System.Data;
using FH.Admin.Data;
using FH.Admin.Entities;
using System.Collections.Generic;

namespace FH.Admin.Service
{
    public class CityZoneAreaMappingBL:ICityZoneAreaMappingBL
    {
        private readonly ICityZoneAreaMappingDA czaMapping;
        public CityZoneAreaMappingBL(IDbTransaction transaction)
        {
            czaMapping = new CityZoneAreaMappingDA(transaction);
        }

        public CityZoneAreaMapping GetDetails()
        {
            return czaMapping.GetDetails();
        }

        public string AddNewZone(ZoneMaster entity)
        {
           return czaMapping.AddNewZone(entity);
        }

        public string AddNewArea(AreaMaster entity)
        {
            return czaMapping.AddNewArea(entity);
        }
        public IEnumerable<ZoneMaster> GetZone(int cityId)
        {
            return czaMapping.GetZone(cityId);
        }
        public IEnumerable<AreaMaster> GetArea()
        {
            return czaMapping.GetArea();
        }
        public IEnumerable<AreaMaster> GetArea(int cityId)
        {
            return czaMapping.GetArea(cityId);
        }
        public CityZoneAreaMapping GetAllAreaZone(int cityId=0, string cityCode="")
        {
            return czaMapping.GetAllAreaZone(cityId,cityCode);
        }
    }

    public interface ICityZoneAreaMappingBL
    {
        CityZoneAreaMapping GetDetails();
        string AddNewZone(ZoneMaster entity);
        string AddNewArea(AreaMaster entity);
        IEnumerable<ZoneMaster> GetZone(int cityId);   
        IEnumerable<AreaMaster> GetArea();
        IEnumerable<AreaMaster> GetArea(int cityId);
        CityZoneAreaMapping GetAllAreaZone(int cityId=0, string cityCode="");
    }
}
