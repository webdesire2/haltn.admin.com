﻿using System.Data;
using FH.Admin.Entities;
using FH.Admin.Data;
using System.Collections.Generic;

namespace FH.Admin.Service
{
   public class UserBL:IUserBL
    {
        private IUserDA _userDA;
        
        public UserBL(IDbTransaction transaction)
        {
            _userDA = new UserDA(transaction);
        }    

        public UserDetails GetUserDetails(string email)
        {
            return _userDA.GetUserDetail(email);
        }
        public IEnumerable<UserList> GetAllEmployee()
        {
           return _userDA.GetAllEmployee();
        }

        public int AddUser(UserRegistration entity)
        {
            return _userDA.AddUser(entity);
        }

        public UserRegistration GetAllDepartment_Designation_City()
        {
            return _userDA.GetAllDepartment_Designation_City();
        }

        public IEnumerable<UserDetails> GetAreaManager(int areaId)
        {
            return _userDA.GetAreaManager(areaId);
        }

        public UserRegistration GetTeamAndDesignation(int departmentId)
        {
            return _userDA.GetTeamAndDesignation(departmentId);
        }

        public IEnumerable<CityMaster> GetAllCity()
        {
            return _userDA.GetAllCity();
        }

        public bool IsUserMobileNoExist(string mobileNo)
        {
            return _userDA.IsUserMobileNoExist(mobileNo);
        }
        public UserDetails GetUserByIdAndMobile(int userId, string mobileNo)
        {
            return _userDA.GetUserByIdAndMobile(userId, mobileNo);
        }
    }

    public interface IUserBL
    {
        UserDetails GetUserDetails(string email);
        IEnumerable<UserList> GetAllEmployee();
        int AddUser(UserRegistration entity);
        UserRegistration GetAllDepartment_Designation_City();
        IEnumerable<UserDetails> GetAreaManager(int areaId);
        UserRegistration GetTeamAndDesignation(int departmentId);
        IEnumerable<CityMaster> GetAllCity();
        bool IsUserMobileNoExist(string mobileNo);
        UserDetails GetUserByIdAndMobile(int userId, string mobileNo);
    }
}
