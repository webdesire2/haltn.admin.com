﻿using FH.Admin.Entities;
using System.Collections.Generic;
using FH.Admin.Data;
using System.Data;

namespace FH.Admin.Service
{
    public class AmenityMasterBL:IAmenityMasterBL
    {
        private readonly IAmenityMasterDA amenityMasterDA;
        public AmenityMasterBL(IDbTransaction transaction)
        {
            amenityMasterDA = new AmenityMasterDA(transaction);
        }
        public IEnumerable<AmenityMaster> GetAll()
        {
            return amenityMasterDA.GetAll();
        }
    }

   public interface IAmenityMasterBL
    {
        IEnumerable<AmenityMaster> GetAll();
    }
}
