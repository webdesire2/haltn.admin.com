﻿using FH.Admin.Entities;
using FH.Admin.Data;
using System.Collections.Generic;
using FH.Admin.Entities.ViewModel;
using System.Linq;

namespace FH.Admin.Service
{
    public class PlanBL
    {
        private readonly PlanDA _planDA;

        public PlanBL()
        {
            _planDA = new PlanDA();
        }

        public QueryResponse AddPlan(PlanMaster de)
        {
            return _planDA.Execute<QueryResponse>(de, PlanCallValue.AddPlanByAdmin).FirstOrDefault();
        }

        public QueryResponse DeleteOderByAdmin(PlanMaster de)
        {
            return _planDA.Execute<QueryResponse>(de, PlanCallValue.DeleteOderByAdmin).FirstOrDefault();
        }
         

        public IEnumerable<PlanMaster> PlanList(PlanMaster de)
        { 
            return _planDA.Execute<PlanMaster>(de, PlanCallValue.GetAllPlanByAdmin);
        }

        public IEnumerable<PlanSellMaster> GetMyOder(PlanMaster de)
        {
            return _planDA.Execute<PlanSellMaster>(de, PlanCallValue.GetMyOder);
        }

        public int AddPlanToVendor(PlanMaster de)
        {
            return _planDA.ExecuteWithTransaction<int>(de, PlanCallValue.AddPlanToVendor);
        }

        public IEnumerable<PlanMaster> AddPlanOrder(PlanMaster de)
        {
            return _planDA.Execute<PlanMaster>(de, PlanCallValue.AddPlanOrder);
        }

        public IEnumerable<PlanSellMaster> GetMyPlans(PlanMaster de)
        {
            return _planDA.Execute<PlanSellMaster>(de, PlanCallValue.GetMyPlans);
        }

        public IEnumerable<PropertyMaster> GetOwnerPropertyDetails(PlanMaster de)
        {
            return _planDA.Execute<PropertyMaster>(de, PlanCallValue.GetOwnerPropertyDetails);
        }


        public PlanMaster GetPlan(PlanMaster de)
        {
            return _planDA.Execute<PlanMaster>(de, PlanCallValue.GetPlanByAdmin).FirstOrDefault();
        }

        public int Update(PlanMaster de)
        {
            return _planDA.Execute<int>(de, PlanCallValue.UpdatePlanByAdmin).FirstOrDefault();
        }
        /*
         *         public QueryResponse CollectPayment(TenantDE de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.CollectPayment).FirstOrDefault();
        }


        public IEnumerable<TenantPaymentHistory> GetTenantPaymentLog(TenantDE de)
        {
            return _tenantDA.Execute<TenantPaymentHistory>(de, TenantCallValue.GetTenantPaymentByAdmin);
        }

        public TanentStayDetails GetTanentStayDetails(TenantDE de)
        {
            return _tenantDA.Execute<TanentStayDetails>(de, TenantCallValue.GetTanentStayDetails).FirstOrDefault(); ;
        }

        public GetTenant GetTenant(TenantDE de)
        {
            return _tenantDA.Execute<GetTenant>(de, TenantCallValue.GetTenantByAdmin).FirstOrDefault();
        }

        public int Update(TenantDE de)
        {
            return _tenantDA.Execute<int>(de, TenantCallValue.UpdateTenantByAdmin).FirstOrDefault();
        }
         */

    }
}
