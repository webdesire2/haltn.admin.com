﻿using FH.Admin.Entities;
using System.Collections.Generic;
using FH.Admin.Data;
using System.Data;

namespace FH.Admin.Service
{
   public class CityMasterBL:ICityMasterBL
    {
        private readonly ICityMasterDA cityMasterDA;
        public CityMasterBL(IDbTransaction transaction)
        {
            cityMasterDA = new CityMasterDA(transaction);
        }
        public IEnumerable<CityMaster> GetAllCity()
        {
            return cityMasterDA.GetAllCity();
        }
    }

    public interface ICityMasterBL
    {
        IEnumerable<CityMaster> GetAllCity();
    }
}
