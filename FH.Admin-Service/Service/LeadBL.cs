﻿using FH.Admin.Entities;
using FH.Admin.Data.Repository;
using System.Collections.Generic;

namespace FH.Admin.Service
{
    public class LeadBL
    {
        private readonly LeadDA _leadDA;

        public LeadBL()
        {
            _leadDA = new LeadDA();
        }

        public IEnumerable<SiteVisitModel> GetSiteVisit(LeadDE de)
        {
            return _leadDA.Execute<SiteVisitModel>(de, LeadCallValue.GetSiteVisit);
        }

        public IEnumerable<WebsiteLead> GetWebsiteLeads(LeadDE de)
        {
            return _leadDA.Execute<WebsiteLead>(de, LeadCallValue.GetWebsiteLeads);
        }

        //

        public IEnumerable<PropertyForLead> GetProperty(LeadDE de)
        {
            return _leadDA.Execute<PropertyForLead>(de,LeadCallValue.GetActivePropertyToAddLead);
        }
        
        public IEnumerable<dynamic> AddOfflineLead(LeadDE de)
        {
           return _leadDA.Execute<dynamic>(de, LeadCallValue.AddOfflineLead);
        }

        public IEnumerable<AllLeadList> GetAllPGLead(LeadDE de)
        {
            return _leadDA.Execute<AllLeadList>(de,LeadCallValue.GetPGLeadByFilter);
        }
    }
}
