﻿using FH.Admin.Data;
using FH.Admin.Entities;
using System.Collections.Generic;
using System.Linq;

namespace FH.Admin.Service
{
    public class UseCodeMasterBL
    {
        private readonly UseCodeMasterDA _useCodeDA;
        public UseCodeMasterBL()
        {
            _useCodeDA = new UseCodeMasterDA();
        }

        public int AddUseCode(UseCodeMaster de)
        {
           return _useCodeDA.ExecuteWithTransaction<int>(de, UseCodeCallValue.AddNew);            
        }

        public IEnumerable<UseCodeMaster> GetAll()
        {
            return _useCodeDA.Execute<UseCodeMaster>(null, UseCodeCallValue.GetAll);
        }

        public UseCodeMaster GetById(UseCodeMaster de)
        {
            var data= _useCodeDA.Execute<UseCodeMaster>(de, UseCodeCallValue.GetById);
            if (data != null)
                return data.FirstOrDefault();

            return null;            
        }

        public void UpdateUseCode(UseCodeMaster de)
        {
            _useCodeDA.ExecuteWithTransaction<int>(de, UseCodeCallValue.Update);
        }
    }
}
