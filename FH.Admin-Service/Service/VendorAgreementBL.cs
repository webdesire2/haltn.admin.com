﻿using System.IO;
using System.Net.Mail;
using System.Collections.ObjectModel;
using FH.Admin.Entities;
using FH.Admin.Service.Helper;
using System.Collections.Generic;

namespace FH.Admin.Service
{
    public class VendorAgreementBL
    {
        public VendorEmail GetVendorEmail(VendorAgreementParam de)
        {
            using (var uow = new UnitOfWork())
            {
                return uow.VendorAgreementDA.GetVendorEmail(de);                
            }
        }

        public bool SendAgreementMail(int propertyId,string mailTo,string teplateUrl,Stream attachmentFile,string fileName)
        {
            var acceptenceUrl=System.Configuration.ConfigurationManager.AppSettings["SellerAgreementAceeptanceUrl"];

            Collection<Attachment> attachments = new Collection<Attachment>();
            string subject ="Agreement Document With Haltn-"+propertyId;
            string mailBody = string.Empty;
            using (StreamReader reader = new StreamReader(teplateUrl))
            {
                mailBody = reader.ReadToEnd();
            }
                attachments.Add(new Attachment(attachmentFile,fileName));           
            try
            {   
                VendorAgreementParam de = new VendorAgreementParam()
                {
                    PropertyId=propertyId,
                    Status = Enum.SellerAgreementStatus.SENT.ToString()
                };

                using (var uow = new UnitOfWork())
                {
                  uow.VendorAgreementDA.UpdateAgreementStatus(de);
                    uow.Commit();
                    mailBody = mailBody.Replace("{acceptanceUrl}", string.Concat(acceptenceUrl, EncreptDecrpt.Encrypt(propertyId.ToString())));
                    MailHelper.SendMailBySales(mailTo, subject, mailBody, attachments);
                    return true;
                }
            }
           catch
            {
                return false;
            }
        }
    }
}
