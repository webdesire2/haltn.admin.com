﻿using FH.Admin.Entities;
using FH.Admin.Data;
using System.Collections.Generic;
using FH.Admin.Entities.ViewModel;
using System.Linq;

namespace FH.Admin.Service
{
    public class TenantBL
    {
        private readonly TenantDA _tenantDA;

        public TenantBL()
        {
            _tenantDA = new TenantDA();
        }

        public QueryResponse AddTenant(TenantDE de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.AddTenantByAdmin).FirstOrDefault();
        }

        public QueryResponse CollectPayment(TenantDE de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.CollectPayment).FirstOrDefault();
        }

        public QueryResponse CollectSecurity(TenantDE de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.CollectSecurity).FirstOrDefault();
        }

        public QueryResponse RefundSecurity(TenantDE de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.RefundSecurity).FirstOrDefault();
        }
        
        public TenantListVM GetAllTenant(TenantDE de)
        {
            return _tenantDA.GetAllTenant(de, TenantCallValue.GetAllTenantByAdmin);
        }


        public IEnumerable<TenantList> LRGetAllTenant(TenantDE de)
        {
            return _tenantDA.Execute<TenantList>(de, TenantCallValue.gtnotpaid);
        }

        public IEnumerable<NotPaidT> gtnotpaid(TenantDE de)
        {
            return _tenantDA.Execute<NotPaidT>(de, TenantCallValue.gtnotpaid);
        } 
        public IEnumerable<PGRoomData> getAvailableRooms(TenantDE de)
        {
            return _tenantDA.Execute<PGRoomData>(de, TenantCallValue.getAvailableRooms);
        }

        public PGRoomData getTenantRooms(TenantDE de)
        {
            return _tenantDA.Execute<PGRoomData>(de, TenantCallValue.getTenantRooms).FirstOrDefault();
        }

        public IEnumerable<TenantPaymentHistory> GetTenantPaymentLog(TenantDE de)
        {
            return _tenantDA.Execute<TenantPaymentHistory>(de, TenantCallValue.GetTenantPaymentByAdmin);
        }

        public TanentStayDetails GetTanentStayDetails(TenantDE de)
        {
            return _tenantDA.Execute<TanentStayDetails>(de, TenantCallValue.GetTanentStayDetails).FirstOrDefault(); ;
        }

        public GetTenant GetTenant(TenantDE de)
        {
            return _tenantDA.Execute<GetTenant>(de, TenantCallValue.GetTenantByAdmin).FirstOrDefault();
        }

        public int Update(TenantDE de)
        {
            return _tenantDA.Execute<int>(de, TenantCallValue.UpdateTenantByAdmin).FirstOrDefault();
        }
    }
}
