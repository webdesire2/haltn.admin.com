﻿using System;
using System.Data;
using FH.Admin.Data.Repository;

namespace FH.Admin.Service
{
    public class UnitOfWork : IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        private IUserBL _user;
        private IPropertyBL _property;        
        private ICityZoneAreaMappingBL _zoneAreaMapping;
        private IPropertyLeadBL _propertyLead;
        private ICityMasterBL _cityMaster;
        private IDepartmentMasterBL _departmentMaster;
        private ITeamMasterBL _teamMaster;
        private IDesignationMasterBL _designationMaster;
        private IVendorBL _vendor;
        private IAmenityMasterBL _amenityMaster;

        private VendorAgreementDA _vendorAgreementDA;

        public UnitOfWork()
        {
            _connection = ConnectionFactory.GetConnection;
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IUserBL UserBL
        {
            get
            {
                return _user ?? (_user = new UserBL(_transaction));
            }
        }

        public IPropertyBL PropertyBL
        {
            get
            {
                return _property ?? (_property = new PropertyBL(_transaction));
            }
        }
       
        public ICityZoneAreaMappingBL CityZoneAreaMappingBL
        {
            get
            {
                return _zoneAreaMapping ?? (_zoneAreaMapping = new CityZoneAreaMappingBL(_transaction));
            }
        }

        public IPropertyLeadBL PropertyLeadBL
        {
            get
            {
                return _propertyLead ?? (_propertyLead = new PropertyLeadBL(_transaction));
            }
        }

        public ICityMasterBL CityMasterBL
        {
            get
            {
                return _cityMaster ?? (_cityMaster = new CityMasterBL(_transaction));
            }
        }

        public IDepartmentMasterBL DepartmentMasterBL
        {
            get
            {
                return _departmentMaster ?? (_departmentMaster = new DepartmentBL(_transaction));
            }
        }
        public ITeamMasterBL TeamMasterBL
        {
            get
            {
                return _teamMaster ?? (_teamMaster = new TeamMasterBL(_transaction));
            }
        }
        public IDesignationMasterBL DesignationMasterBL
        {
            get
            {
                return _designationMaster ?? (_designationMaster = new DesignationMasterBL(_transaction));
            }
        }
        public IVendorBL VendorBL
        {
            get
            {
                return _vendor ?? (_vendor = new VendorBL(_transaction));
            }
        }
        public IAmenityMasterBL AmenityMasterBL
        {
            get
            {
                return _amenityMaster ?? (_amenityMaster = new AmenityMasterBL(_transaction));
            }
        }

        public VendorAgreementDA VendorAgreementDA
        {
            get
            {
                return _vendorAgreementDA ?? (_vendorAgreementDA = new VendorAgreementDA(_transaction));
            }
        }

        private void Reset()
        {
            _user = null;
            _property = null;            
            _zoneAreaMapping = null;
            _propertyLead = null;
            _cityMaster = null;
            _departmentMaster = null;
            _cityMaster = null;
            _designationMaster = null;
            _vendor = null;
            _amenityMaster = null;
            _vendorAgreementDA = null;
        }
        public void Commit()
        {
            try
            {
                _transaction.Commit();               
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }

                Reset();
            }
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }

            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }
        }

    }
}
