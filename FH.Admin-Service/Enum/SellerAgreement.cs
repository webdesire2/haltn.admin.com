﻿namespace FH.Admin.Service.Enum
{
    public enum SellerAgreementStatus
    {  
       SENT,
       NOT_YET_ACCEPTED,
       ACCEPTED
    }
}
