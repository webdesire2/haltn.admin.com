﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Mail;
using System.Configuration;
namespace FH.Admin.Service
{
    public class MailHelper
    {        
        private static string smtpHost = "smtp.yandex.ru";
        private static int smtpPort =25;

        private static bool SendMail(string userName,string password,string sendToEmail,string mailSubject,string mailCC,string mailBody,Collection<Attachment> attachments)
        {
            try
            {
                MailMessage mailmesage = new MailMessage();
                SmtpClient smtpServer = new SmtpClient(smtpHost);
                mailmesage.From = new MailAddress(userName,"Haltn");
                if(!string.IsNullOrWhiteSpace(sendToEmail))
                {
                    mailmesage.To.Add(sendToEmail);
                }

                if(!string.IsNullOrEmpty(mailCC))
                {   
                    mailmesage.CC.Add(new MailAddress(mailCC));
                }
               
                mailmesage.Subject = mailSubject;
                mailmesage.Body = mailBody;
                mailmesage.IsBodyHtml = true;

                if (attachments != null)
                {
                    foreach (var item in attachments)
                    {
                        mailmesage.Attachments.Add(item);
                    }
                }

                smtpServer.UseDefaultCredentials = false;
                smtpServer.Credentials = new NetworkCredential(userName, password);
                smtpServer.Host = smtpHost;
                smtpServer.Port = smtpPort;                                
                smtpServer.EnableSsl = true;                
                smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpServer.Send(mailmesage);
                mailmesage.Attachments.Dispose();
                return true;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        public static bool SendMailBySales(string mailTo,string mailSubject,string mailBody,Collection<Attachment> attachments=null)
        {
            string userName = ConfigurationManager.AppSettings["SellerAgreementEmail"];            
            string password = ConfigurationManager.AppSettings["SellerAgreementEmailPassword"]; ;            
            return SendMail(userName, password, mailTo, mailSubject,userName,mailBody, attachments);                        
        }
    }    
}
